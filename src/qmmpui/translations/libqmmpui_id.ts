<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../forms/aboutdialog.ui" line="14"/>
        <source>About Qmmp</source>
        <translation>Tentang Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="49"/>
        <source>About</source>
        <translation>Tentang</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="63"/>
        <source>Authors</source>
        <translation>Penulis</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="77"/>
        <source>Translators</source>
        <translation>Penerjemah</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="91"/>
        <source>Thanks To</source>
        <translation>Terimakasih Kepada</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="105"/>
        <source>License Agreement</source>
        <translation>Perjanjian Lisensi</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="69"/>
        <source>Qt-based Multimedia Player (Qmmp)</source>
        <translation>Qt-based Multimedia Player (Qmmp)</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="72"/>
        <source>Version: %1</source>
        <translation>Versi: %1</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="79"/>
        <source>(c) %1-%2 Qmmp Development Team</source>
        <translation>(c) %1-%2 Regu Pengembangan Qmmp</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="84"/>
        <source>Transports:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="90"/>
        <source>Decoders:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="98"/>
        <source>Engines:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="105"/>
        <source>Effects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="134"/>
        <source>File dialogs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="143"/>
        <source>User interfaces:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="126"/>
        <source>Output plugins:</source>
        <translation>Plugin keluaran:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="73"/>
        <source>Qt version: %1 (compiled with %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="74"/>
        <source>Qt platform: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="75"/>
        <source>System: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="76"/>
        <source>Build ABI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="113"/>
        <source>Visual plugins:</source>
        <translation>Plugin visual:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="120"/>
        <source>General plugins:</source>
        <translation>Plugin umum:</translation>
    </message>
</context>
<context>
    <name>AddUrlDialog</name>
    <message>
        <location filename="../forms/addurldialog.ui" line="14"/>
        <source>Enter URL to add</source>
        <translation>Masukkan URL untuk menambahkan</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="55"/>
        <source>&amp;Add</source>
        <translation>&amp;Tambahkan</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="62"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Batal</translation>
    </message>
    <message>
        <location filename="../addurldialog.cpp" line="90"/>
        <source>Error</source>
        <translation>Galat</translation>
    </message>
</context>
<context>
    <name>ColorWidget</name>
    <message>
        <location filename="../colorwidget.cpp" line="46"/>
        <source>Select Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColumnEditor</name>
    <message>
        <location filename="../forms/columneditor.ui" line="14"/>
        <source>Edit Column</source>
        <translation>Kolom Edit</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="36"/>
        <source>Name:</source>
        <translation>Nama:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="76"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="64"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="29"/>
        <source>Type:</source>
        <translation>Tipe:</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="86"/>
        <source>Artist</source>
        <translation>Artist</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="87"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="91"/>
        <source>Title</source>
        <translation>Judul</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="94"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="95"/>
        <source>Comment</source>
        <translation>Komentar</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="96"/>
        <source>Composer</source>
        <translation>Pengarang</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="97"/>
        <source>Duration</source>
        <translation>Durasi</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="102"/>
        <source>Year</source>
        <translation>Tahun</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="101"/>
        <source>Track Index</source>
        <translation>Indeks Track</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="88"/>
        <source>Artist - Album</source>
        <translation>Artist - Album</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="89"/>
        <source>Artist - Title</source>
        <translation>Artist - Judul</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="90"/>
        <source>Album Artist</source>
        <translation>Album Artist</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="92"/>
        <source>Track Number</source>
        <translation>Nomor track</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="93"/>
        <source>Two-digit Track Number</source>
        <translation>Nomor Track Dua-digit</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="98"/>
        <source>Disc Number</source>
        <translation>Nomor Cakram</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="99"/>
        <source>File Name</source>
        <translation>Nama File</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="100"/>
        <source>File Path</source>
        <translation>Alur File</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="103"/>
        <source>Parent Directory Name</source>
        <translation>Nama Direktori Induk</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="104"/>
        <source>Parent Directory Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="105"/>
        <source>Custom</source>
        <translation>Kostum</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../forms/configdialog.ui" line="14"/>
        <source>Qmmp Settings</source>
        <translation>Setelan Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="58"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="67"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="76"/>
        <source>Advanced</source>
        <translation>Lanjutan</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="85"/>
        <source>Connectivity</source>
        <translation>Persambungan</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="94"/>
        <location filename="../forms/configdialog.ui" line="996"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="153"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="183"/>
        <source>Convert %20 to blanks</source>
        <translation>Konversikan %20 ke blank</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="159"/>
        <source>Load metadata from files</source>
        <translation>Muat metadata dari file</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="176"/>
        <source>Convert underscores to blanks</source>
        <translation>Konversikan garis bawah ke blank</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="226"/>
        <source>Group format:</source>
        <translation>Format grup:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="242"/>
        <location filename="../forms/configdialog.ui" line="267"/>
        <location filename="../forms/configdialog.ui" line="710"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="169"/>
        <source>Read tags while loading a playlist</source>
        <translation>Baca tag ketika memuat suatu daftar putar</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="193"/>
        <source>Group Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="202"/>
        <source>Group size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="219"/>
        <source>Show dividing line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="251"/>
        <source>Extra row format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="276"/>
        <source>Show extra row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="283"/>
        <source>Show cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="293"/>
        <source>Directory Scanning Options</source>
        <translation>Pilihan Pemindaian Direktori</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="299"/>
        <source>Restrict files to:</source>
        <translation>Membatasi file-file ke:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="313"/>
        <location filename="../forms/configdialog.ui" line="588"/>
        <source>Exclude files:</source>
        <translation>Kecuali file:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="330"/>
        <source>Miscellaneous</source>
        <translation>Lain-lain</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="336"/>
        <source>Auto-save playlist when modified</source>
        <translation>Simpan otomatis playlist ketika dimodifikasi</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="343"/>
        <source>Clear previous playlist when opening new one</source>
        <translation>Bersihkan playlist sebelumnya ketika membuka yang baru</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="396"/>
        <location filename="../configdialog.cpp" line="341"/>
        <source>Preferences</source>
        <translation>Suka-suka</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="413"/>
        <location filename="../configdialog.cpp" line="344"/>
        <source>Information</source>
        <translation>Informasi</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="459"/>
        <source>Description</source>
        <translation>Deskripsi</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="464"/>
        <source>Filename</source>
        <translation>Filename</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="476"/>
        <source>Look and Feel</source>
        <translation>Lihat dan Rasakan</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="482"/>
        <source>Language:</source>
        <translation>Bahasa:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="515"/>
        <source>Display average bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="525"/>
        <source>Playback</source>
        <translation>Playback</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="531"/>
        <source>Continue playback on startup</source>
        <translation>Lanjutkan playback saat startup</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="538"/>
        <source>Determine file type by content</source>
        <translation>Menetapkan tipe file oleh isi</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="545"/>
        <source>Add files from command line to this playlist:</source>
        <translation>Tambahkan file dari baris perintah ke playlist ini:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="562"/>
        <source>Cover Image Retrieve</source>
        <translation>Tarik Citra Sampul</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="568"/>
        <source>Use separate image files</source>
        <translation>Gunakan file-file citra tersendiri</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="578"/>
        <source>Include files:</source>
        <translation>Cakup file:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="600"/>
        <source>Recursive search depth:</source>
        <translation>Kedalaman penelusuran rekursif:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="632"/>
        <source>URL Dialog</source>
        <translation>Dialog URL</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="638"/>
        <source>Auto-paste URL from clipboard</source>
        <translation>Tempel otomatis URL dari clipboard</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="648"/>
        <source>CUE Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="654"/>
        <source>Use system font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="675"/>
        <source>Font:</source>
        <translation>Fonta:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="700"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="739"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="751"/>
        <source>Enable proxy usage</source>
        <translation>Aktifkan penggunaan proxy</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="758"/>
        <source>Proxy type:</source>
        <translation>Tipe proksi:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="768"/>
        <source>Proxy host name:</source>
        <translation>Nama host proxy:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="785"/>
        <source>Proxy port:</source>
        <translation>Port proxy:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="802"/>
        <source>Use authentication with proxy</source>
        <translation>Gunakan pengesahan dengan proxy</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="809"/>
        <source>Proxy user name:</source>
        <translation>Nama pengguna proxy:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="826"/>
        <source>Proxy password:</source>
        <translation>Sandi proxy:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="866"/>
        <source>Replay Gain</source>
        <translation>Gain Replay</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="872"/>
        <source>Replay Gain mode:</source>
        <translation>Mode Gain Replay:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="889"/>
        <source>Preamp:</source>
        <translation>Preamp:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="921"/>
        <location filename="../forms/configdialog.ui" line="966"/>
        <source>dB</source>
        <translation>dB</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="934"/>
        <source>Default gain:</source>
        <translation>Gain default:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="986"/>
        <source>Use  peak info to prevent clipping</source>
        <translation>Gunakan puncak info untuk mencegah clipping</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1002"/>
        <source>Buffer size:</source>
        <translation>Ukuran buffer:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1028"/>
        <source>ms</source>
        <translation>md</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1061"/>
        <source>Use software volume control</source>
        <translation>Gunakan software kendali volume</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1101"/>
        <source>Use two passes for equalizer</source>
        <translation>Gunakan dua kali proses untuk ekualizer</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1044"/>
        <source>Volume adjustment step:</source>
        <translation>Langkah utak-atik volume:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="350"/>
        <source>Skip already existing tracks when adding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="357"/>
        <source>Stop playback after removing of current track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1068"/>
        <source>Output bit depth:</source>
        <translation>Kedalaman bit keluaran:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1009"/>
        <source>Use dithering</source>
        <translation>Gunakan dithering</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="63"/>
        <source>1 row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="64"/>
        <source>3 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="65"/>
        <source>4 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="66"/>
        <source>5 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="67"/>
        <source>Track</source>
        <translation>Track</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="68"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="69"/>
        <source>Disabled</source>
        <translation>Dinonaktifkan</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="76"/>
        <source>HTTP</source>
        <translation>HTTP</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="77"/>
        <source>SOCKS5</source>
        <translation>SOCKS5</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="204"/>
        <source>Transports</source>
        <translation>Transports</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="215"/>
        <source>Decoders</source>
        <translation>Decoders</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="226"/>
        <source>Engines</source>
        <translation>Mesin</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="238"/>
        <source>Effects</source>
        <translation>Efek</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="249"/>
        <source>Visualization</source>
        <translation>Visualisasi</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>General</source>
        <translation>Umum</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="272"/>
        <source>Output</source>
        <translation>Keluaran</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="283"/>
        <source>File Dialogs</source>
        <translation>Dialog File</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="295"/>
        <source>User Interfaces</source>
        <translation>Antarmuka Pengguna</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="354"/>
        <source>&lt;Autodetect&gt;</source>
        <translation>&lt;DeteksiOtomatis&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="355"/>
        <source>Brazilian Portuguese</source>
        <translation>Brazilian Portuguese</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="356"/>
        <source>Chinese Simplified</source>
        <translation>Chinese Simplified</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="357"/>
        <source>Chinese Traditional</source>
        <translation>Chinese Traditional</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="358"/>
        <source>Czech</source>
        <translation>Czech</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="359"/>
        <source>Dutch</source>
        <translation>Dutch</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="360"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="361"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="362"/>
        <source>Galician</source>
        <translation>Galician</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="363"/>
        <source>German</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="364"/>
        <source>Greek</source>
        <translation>Yunani:</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="365"/>
        <source>Hebrew</source>
        <translation>Hebrew</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="366"/>
        <source>Hungarian</source>
        <translation>Hungarian</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="367"/>
        <source>Indonesian</source>
        <translation>Indonesian</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="368"/>
        <source>Italian</source>
        <translation>Italian</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="369"/>
        <source>Japanese</source>
        <translation>Japanese</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="370"/>
        <source>Kazakh</source>
        <translation>Kazakh</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="371"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="372"/>
        <source>Lithuanian</source>
        <translation>Lithuanian</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="373"/>
        <source>Polish</source>
        <translation>Polish</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="374"/>
        <source>Portuguese</source>
        <translation>Portuguese</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="375"/>
        <source>Russian</source>
        <translation>Russian</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="376"/>
        <source>Serbian</source>
        <translation>Serbia:</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="377"/>
        <source>Slovak</source>
        <translation>Slovak</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="378"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="379"/>
        <source>Spanish</source>
        <translation>Spanish</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <source>Turkish</source>
        <translation>Turkish</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="381"/>
        <source>Ukrainian</source>
        <translation>Ukrainian</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="382"/>
        <source>Serbian (Ijekavian)</source>
        <translation>Serbian (Ijekavian)</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="383"/>
        <source>Serbian (Ekavian)</source>
        <translation>Serbian (Ekavian)</translation>
    </message>
</context>
<context>
    <name>CoverEditor</name>
    <message>
        <location filename="../forms/covereditor.ui" line="22"/>
        <source>Image source:</source>
        <translation>Sumber citra:</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="76"/>
        <source>Load</source>
        <translation>Muat</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="83"/>
        <source>Delete</source>
        <translation>Hapus</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="90"/>
        <source>Save as...</source>
        <translation>Simpan sebagai...</translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="34"/>
        <source>External file</source>
        <translation>File eksternal</translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="35"/>
        <source>Tag</source>
        <translation>Tag</translation>
    </message>
</context>
<context>
    <name>CoverViewer</name>
    <message>
        <location filename="../coverviewer.cpp" line="35"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Simpan Sebagai...</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="68"/>
        <source>Save Cover As</source>
        <translation>Simpan Sampul Sebagai</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="70"/>
        <location filename="../coverviewer.cpp" line="83"/>
        <source>Images</source>
        <translation>Citra</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="81"/>
        <source>Open Image</source>
        <translation>Buka citra</translation>
    </message>
</context>
<context>
    <name>CueEditor</name>
    <message>
        <location filename="../forms/cueeditor.ui" line="40"/>
        <source>Load</source>
        <translation>Muat</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="47"/>
        <source>Delete</source>
        <translation>Hapus</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="54"/>
        <source>Save as...</source>
        <translation>Simpan sebagai...</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <source>Open CUE File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <location filename="../cueeditor.cpp" line="150"/>
        <source>CUE Files</source>
        <translation>File CUE</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="148"/>
        <source>Save CUE File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../forms/detailsdialog.ui" line="14"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="44"/>
        <source>Open the directory containing this file</source>
        <translation> Buka direktori yang berisi file ini</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="47"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="63"/>
        <source>Summary</source>
        <translation>Ringkasan</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="207"/>
        <source>%1/%2</source>
        <translation>%1/%2</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="257"/>
        <source>Cover</source>
        <translation>Sampul</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="283"/>
        <source>Lyrics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="303"/>
        <source>Title</source>
        <translation>Judul</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="304"/>
        <source>Artist</source>
        <translation>Artist</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="305"/>
        <source>Album artist</source>
        <translation>Album artist</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="306"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="307"/>
        <source>Comment</source>
        <translation>Komentar</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="308"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="309"/>
        <source>Composer</source>
        <translation>Pengarang</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="310"/>
        <source>Year</source>
        <translation>Tahun</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="311"/>
        <source>Track</source>
        <translation>Track</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="312"/>
        <source>Disc number</source>
        <translation>Nomor cakram</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="331"/>
        <source>Duration</source>
        <translation>Durasi</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>kbps</source>
        <translation>kbps</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Sample rate</source>
        <translation>Sample rate</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="336"/>
        <source>Channels</source>
        <translation>Saluran</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>Sample size</source>
        <translation>Ukuran sample</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>bits</source>
        <translation>bits</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="338"/>
        <source>Format name</source>
        <translation>Nama format</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>File size</source>
        <translation>Ukuran file</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>Yes</source>
        <translation>Ya</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>No</source>
        <translation>Tidak</translation>
    </message>
</context>
<context>
    <name>JumpToTrackDialog</name>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="14"/>
        <source>Jump To Track</source>
        <translation>Lompat Ke Track</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="46"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="85"/>
        <location filename="../jumptotrackdialog.cpp" line="120"/>
        <location filename="../jumptotrackdialog.cpp" line="151"/>
        <source>Queue</source>
        <translation>Antrian</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="105"/>
        <source>Jump To</source>
        <translation>Lompat Ke</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="95"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="96"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="118"/>
        <location filename="../jumptotrackdialog.cpp" line="149"/>
        <source>Unqueue</source>
        <translation>Tak Antri</translation>
    </message>
</context>
<context>
    <name>MetaDataFormatterMenu</name>
    <message>
        <location filename="../metadataformattermenu.cpp" line="27"/>
        <source>Artist</source>
        <translation>Artist</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="28"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="29"/>
        <source>Album Artist</source>
        <translation>Album Artist</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="32"/>
        <source>Title</source>
        <translation>Judul</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="33"/>
        <source>Track Number</source>
        <translation>Nomor track</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="34"/>
        <source>Two-digit Track Number</source>
        <translation>Nomor Track Dua-digit</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="38"/>
        <source>Track Index</source>
        <translation>Indeks Track</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="40"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="41"/>
        <source>Comment</source>
        <translation>Komentar</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="42"/>
        <source>Composer</source>
        <translation>Pengarang</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="47"/>
        <location filename="../metadataformattermenu.cpp" line="61"/>
        <source>Duration</source>
        <translation>Durasi</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="55"/>
        <source>Artist - Album</source>
        <translation type="unfinished">Artist - Album</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>Duration | Format | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>Duration | Format | Bitrate | Sample rate </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>Year | Duration | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="68"/>
        <source>Parent Directory Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="70"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="71"/>
        <source>Sample Rate</source>
        <translation>Laju Sample</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="72"/>
        <source>Number of Channels</source>
        <translation>Jumlah Channel</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="73"/>
        <source>Sample Size</source>
        <translation>Ukuran Sample</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="74"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="75"/>
        <source>Decoder</source>
        <translation>Decoder</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="78"/>
        <source>File Size</source>
        <translation>Ukursn File</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="43"/>
        <source>Disc Number</source>
        <translation>Nomor Cakram</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="48"/>
        <source>File Name</source>
        <translation>Nama File</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="49"/>
        <source>File Path</source>
        <translation>Alur File</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="44"/>
        <source>Year</source>
        <translation>Tahun</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="51"/>
        <location filename="../metadataformattermenu.cpp" line="57"/>
        <location filename="../metadataformattermenu.cpp" line="65"/>
        <source>Condition</source>
        <translation>Kondisi</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="50"/>
        <source>Artist - Title</source>
        <translation>Artist - Judul</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="56"/>
        <source>Artist - [Year] Album</source>
        <translation>Artis - [Tahun] Album</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>%y | %if(%l,%l | ,)%{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="67"/>
        <source>Parent Directory Name</source>
        <translation>Nama Direktori Induk</translation>
    </message>
</context>
<context>
    <name>PlayListDownloader</name>
    <message>
        <location filename="../playlistdownloader.cpp" line="123"/>
        <source>Unsupported playlist format</source>
        <translation>Format playlist tak didukung</translation>
    </message>
</context>
<context>
    <name>PlayListHeaderModel</name>
    <message>
        <location filename="../playlistheadermodel.cpp" line="35"/>
        <source>Artist - Title</source>
        <translation>Artist - Judul</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="185"/>
        <source>Title</source>
        <translation>Judul</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="186"/>
        <source>Add Column</source>
        <translation>Tambahkan Kolom</translation>
    </message>
</context>
<context>
    <name>PlayListManager</name>
    <message>
        <location filename="../playlistmanager.cpp" line="177"/>
        <location filename="../playlistmanager.cpp" line="319"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
</context>
<context>
    <name>PlayListTrack</name>
    <message>
        <location filename="../playlisttrack.cpp" line="245"/>
        <source>Streams</source>
        <translation>Streams</translation>
    </message>
    <message>
        <location filename="../playlisttrack.cpp" line="250"/>
        <source>Empty group</source>
        <translation>Kosongkan grup</translation>
    </message>
</context>
<context>
    <name>QmmpUiSettings</name>
    <message>
        <location filename="../qmmpuisettings.cpp" line="39"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpuisettings.cpp" line="64"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
</context>
<context>
    <name>QtFileDialogFactory</name>
    <message>
        <location filename="../qtfiledialog.cpp" line="35"/>
        <source>Qt File Dialog</source>
        <translation>Dialog File Qt</translation>
    </message>
</context>
<context>
    <name>ShortcutDialog</name>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="14"/>
        <source>Change Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="29"/>
        <source>Press the key combination you want to assign</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="52"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagEditor</name>
    <message>
        <location filename="../forms/tageditor.ui" line="14"/>
        <source>Tag Editor</source>
        <translation>Pengedit Tag</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="38"/>
        <source>Title:</source>
        <translation>Judul:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="64"/>
        <source>Artist:</source>
        <translation>Artist:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="90"/>
        <source>Album:</source>
        <translation>Album:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="113"/>
        <source>Album artist:</source>
        <translation>Album artist:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="123"/>
        <source>Composer:</source>
        <translation>Pengarang:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="143"/>
        <source>Genre:</source>
        <translation>Genre:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="169"/>
        <source>Track:</source>
        <translation>Track:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="190"/>
        <location filename="../forms/tageditor.ui" line="228"/>
        <location filename="../forms/tageditor.ui" line="260"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="203"/>
        <source>Year:</source>
        <translation>Tahun:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="250"/>
        <source>Disc number:</source>
        <translation>Nomor cakram:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="275"/>
        <source>Comment:</source>
        <translation>Komentar:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="311"/>
        <source>Include selected tag in file</source>
        <translation>Cakup tag yang terpilih di file</translation>
    </message>
</context>
<context>
    <name>TemplateEditor</name>
    <message>
        <location filename="../forms/templateeditor.ui" line="14"/>
        <source>Template Editor</source>
        <translation>Pengedit Template</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="39"/>
        <source>Reset</source>
        <translation>Setel ulang</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="46"/>
        <source>Insert</source>
        <translation>Sisispkan</translation>
    </message>
</context>
<context>
    <name>UiHelper</name>
    <message>
        <location filename="../uihelper.cpp" line="136"/>
        <location filename="../uihelper.cpp" line="148"/>
        <source>All Supported Bitstreams</source>
        <translation>Semua Bitstreams Yang Didukung</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="142"/>
        <source>Select one or more files to open</source>
        <translation>Pilih satu atau lebih banyak file untuk buka</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="154"/>
        <source>Select one or more files to play</source>
        <translation>Pilih satu atau lebih banyak file untuk play</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="162"/>
        <source>Choose a directory</source>
        <translation>Memilih sebuah direktori</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="178"/>
        <location filename="../uihelper.cpp" line="202"/>
        <source>Playlist Files</source>
        <translation>File Playlist</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="180"/>
        <source>Open Playlist</source>
        <translation>Buka Playlist</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="205"/>
        <location filename="../uihelper.cpp" line="224"/>
        <source>Save Playlist</source>
        <translation>Simpan Playlist</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="224"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1 sudah ada.
Apakah kamu ingin menggantinya?</translation>
    </message>
</context>
<context>
    <name>VisualMenu</name>
    <message>
        <location filename="../visualmenu.cpp" line="26"/>
        <source>Visualization</source>
        <translation type="unfinished">Visualisasi</translation>
    </message>
</context>
<context>
    <name>WinFileAssocPage</name>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="43"/>
        <source>Media files handled by Qmmp:</source>
        <translation>File media yang ditangani oleh Qmmp:</translation>
    </message>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="17"/>
        <source>Select All</source>
        <translation>Pilih Semua</translation>
    </message>
</context>
</TS>
