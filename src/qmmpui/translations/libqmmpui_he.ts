<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../forms/aboutdialog.ui" line="14"/>
        <source>About Qmmp</source>
        <translation>אודות Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="49"/>
        <source>About</source>
        <translation>אודות</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="63"/>
        <source>Authors</source>
        <translation>מחברים</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="77"/>
        <source>Translators</source>
        <translation>מתרגמים</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="91"/>
        <source>Thanks To</source>
        <translation>תודות לכבוד</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="105"/>
        <source>License Agreement</source>
        <translation>הסכם רישוי</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="69"/>
        <source>Qt-based Multimedia Player (Qmmp)</source>
        <translation>נגן מולטימדיה מבוסס ‏Qt ‏(Qmmp)</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="72"/>
        <source>Version: %1</source>
        <translation>גירסה: %1</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="79"/>
        <source>(c) %1-%2 Qmmp Development Team</source>
        <translation>‏(c) ‏%1-%2 נבחרת הפיתוח של Qmmp</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="84"/>
        <source>Transports:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="90"/>
        <source>Decoders:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="98"/>
        <source>Engines:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="105"/>
        <source>Effects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="134"/>
        <source>File dialogs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="143"/>
        <source>User interfaces:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="126"/>
        <source>Output plugins:</source>
        <translation>תוספי פלט:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="73"/>
        <source>Qt version: %1 (compiled with %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="74"/>
        <source>Qt platform: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="75"/>
        <source>System: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="76"/>
        <source>Build ABI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="113"/>
        <source>Visual plugins:</source>
        <translation>תוספי חזות:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="120"/>
        <source>General plugins:</source>
        <translation>תוספי כלליים:</translation>
    </message>
</context>
<context>
    <name>AddUrlDialog</name>
    <message>
        <location filename="../forms/addurldialog.ui" line="14"/>
        <source>Enter URL to add</source>
        <translation>הזנת URL להוספה</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="55"/>
        <source>&amp;Add</source>
        <translation>&amp;הוסף</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="62"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ביטול</translation>
    </message>
    <message>
        <location filename="../addurldialog.cpp" line="90"/>
        <source>Error</source>
        <translation>שגיאה</translation>
    </message>
</context>
<context>
    <name>ColorWidget</name>
    <message>
        <location filename="../colorwidget.cpp" line="46"/>
        <source>Select Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColumnEditor</name>
    <message>
        <location filename="../forms/columneditor.ui" line="14"/>
        <source>Edit Column</source>
        <translation>ערוך טור</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="36"/>
        <source>Name:</source>
        <translation>שם:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="76"/>
        <source>Format:</source>
        <translation>פורמט:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="64"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="29"/>
        <source>Type:</source>
        <translation>טיפוס:</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="86"/>
        <source>Artist</source>
        <translation>אמן</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="87"/>
        <source>Album</source>
        <translation>אלבום</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="91"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="94"/>
        <source>Genre</source>
        <translation>ז׳אנר</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="95"/>
        <source>Comment</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="96"/>
        <source>Composer</source>
        <translation>מלחין</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="97"/>
        <source>Duration</source>
        <translation>משך</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="102"/>
        <source>Year</source>
        <translation>שנה</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="101"/>
        <source>Track Index</source>
        <translation>אינדקס רצועה</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="88"/>
        <source>Artist - Album</source>
        <translation>אמן - אלבום</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="89"/>
        <source>Artist - Title</source>
        <translation>אמן - כותרת</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="90"/>
        <source>Album Artist</source>
        <translation>אמן אלבום</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="92"/>
        <source>Track Number</source>
        <translation>מספר רצועה</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="93"/>
        <source>Two-digit Track Number</source>
        <translation>מספר רצועה דו ספרתי</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="98"/>
        <source>Disc Number</source>
        <translation>מספר תקליטור</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="99"/>
        <source>File Name</source>
        <translation>שם קובץ</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="100"/>
        <source>File Path</source>
        <translation>נתיב קובץ</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="103"/>
        <source>Parent Directory Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="104"/>
        <source>Parent Directory Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="105"/>
        <source>Custom</source>
        <translation>מותאם</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../forms/configdialog.ui" line="14"/>
        <source>Qmmp Settings</source>
        <translation>הגדרות Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="58"/>
        <source>Playlist</source>
        <translation>רשימת נגינה</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="67"/>
        <source>Plugins</source>
        <translation>תוספים</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="76"/>
        <source>Advanced</source>
        <translation>מתקדמות</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="85"/>
        <source>Connectivity</source>
        <translation>קישוריות</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="94"/>
        <location filename="../forms/configdialog.ui" line="996"/>
        <source>Audio</source>
        <translation>שמע</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="153"/>
        <source>Metadata</source>
        <translation>מידע-מוצמד</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="183"/>
        <source>Convert %20 to blanks</source>
        <translation>המר ‎%20 אל תווי רווח</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="159"/>
        <source>Load metadata from files</source>
        <translation>טען מידע-מוצמד מתוך קבצים</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="176"/>
        <source>Convert underscores to blanks</source>
        <translation>המר הדגשות אל תווי רווח</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="226"/>
        <source>Group format:</source>
        <translation>פורמט קבוצה:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="242"/>
        <location filename="../forms/configdialog.ui" line="267"/>
        <location filename="../forms/configdialog.ui" line="710"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="169"/>
        <source>Read tags while loading a playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="193"/>
        <source>Group Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="202"/>
        <source>Group size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="219"/>
        <source>Show dividing line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="251"/>
        <source>Extra row format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="276"/>
        <source>Show extra row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="283"/>
        <source>Show cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="293"/>
        <source>Directory Scanning Options</source>
        <translation>אפשרויות סריקת מדור</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="299"/>
        <source>Restrict files to:</source>
        <translation>הגבל קבצים אל:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="313"/>
        <location filename="../forms/configdialog.ui" line="588"/>
        <source>Exclude files:</source>
        <translation>הוצא קבצים:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="330"/>
        <source>Miscellaneous</source>
        <translation>שונות</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="336"/>
        <source>Auto-save playlist when modified</source>
        <translation>שמור אוטומטית רשימת נגינה כאשר משתנה</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="343"/>
        <source>Clear previous playlist when opening new one</source>
        <translation>טהר רשימות נגינה קודמות בעת פתיחת אחת חדשה</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="396"/>
        <location filename="../configdialog.cpp" line="341"/>
        <source>Preferences</source>
        <translation>העדפות</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="413"/>
        <location filename="../configdialog.cpp" line="344"/>
        <source>Information</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="459"/>
        <source>Description</source>
        <translation>תיאור</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="464"/>
        <source>Filename</source>
        <translation>שם קובץ</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="476"/>
        <source>Look and Feel</source>
        <translation>מראה ותחושה</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="482"/>
        <source>Language:</source>
        <translation>שפה:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="515"/>
        <source>Display average bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="525"/>
        <source>Playback</source>
        <translation>פס קול</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="531"/>
        <source>Continue playback on startup</source>
        <translation>המשך ניגון פס קול בעת הפעלה</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="538"/>
        <source>Determine file type by content</source>
        <translation>קבע טיפוס קובץ על פי תוכן</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="545"/>
        <source>Add files from command line to this playlist:</source>
        <translation>הוסף קבצים מתוך שורת פקודה אל רשימת נגינה זו:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="562"/>
        <source>Cover Image Retrieve</source>
        <translation>אחזור תמונת כיסוי</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="568"/>
        <source>Use separate image files</source>
        <translation>השתמש בקבצי תמונה פרודים</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="578"/>
        <source>Include files:</source>
        <translation>הכלל קבצים:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="600"/>
        <source>Recursive search depth:</source>
        <translation>חיפוש עומק רקורסיבי:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="632"/>
        <source>URL Dialog</source>
        <translation>דו שיח URL</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="638"/>
        <source>Auto-paste URL from clipboard</source>
        <translation>הדבק אוטומטית URL מתוך לוח גזירה</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="648"/>
        <source>CUE Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="654"/>
        <source>Use system font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="675"/>
        <source>Font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="700"/>
        <source>???</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="739"/>
        <source>Proxy</source>
        <translation>פרוקסי</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="751"/>
        <source>Enable proxy usage</source>
        <translation>אפשר שימוש בפרוקסי</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="758"/>
        <source>Proxy type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="768"/>
        <source>Proxy host name:</source>
        <translation>שם מארח פרוקסי:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="785"/>
        <source>Proxy port:</source>
        <translation>פורט פרוקסי:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="802"/>
        <source>Use authentication with proxy</source>
        <translation>השתמש באימות עם פרוקסי</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="809"/>
        <source>Proxy user name:</source>
        <translation>שם משתמש פרוקסי:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="826"/>
        <source>Proxy password:</source>
        <translation>סיסמת פרוקסי:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="866"/>
        <source>Replay Gain</source>
        <translation>הגברת שמע</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="872"/>
        <source>Replay Gain mode:</source>
        <translation>מצב הגברת שמע:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="889"/>
        <source>Preamp:</source>
        <translation>מגבר קדמי:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="921"/>
        <location filename="../forms/configdialog.ui" line="966"/>
        <source>dB</source>
        <translation>דציבל</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="934"/>
        <source>Default gain:</source>
        <translation>מגבר משתמט:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="986"/>
        <source>Use  peak info to prevent clipping</source>
        <translation>השתמש במידע שיא למניעת קיצץ</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1002"/>
        <source>Buffer size:</source>
        <translation>שיעור אגירה:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1028"/>
        <source>ms</source>
        <translation>מ״ש</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1061"/>
        <source>Use software volume control</source>
        <translation>נצל בקרת שמע של תוכנה</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1101"/>
        <source>Use two passes for equalizer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1044"/>
        <source>Volume adjustment step:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="350"/>
        <source>Skip already existing tracks when adding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="357"/>
        <source>Stop playback after removing of current track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1068"/>
        <source>Output bit depth:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1009"/>
        <source>Use dithering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="63"/>
        <source>1 row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="64"/>
        <source>3 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="65"/>
        <source>4 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="66"/>
        <source>5 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="67"/>
        <source>Track</source>
        <translation>רצועה</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="68"/>
        <source>Album</source>
        <translation>אלבום</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="69"/>
        <source>Disabled</source>
        <translation>מנוטרלת</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="76"/>
        <source>HTTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="77"/>
        <source>SOCKS5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="204"/>
        <source>Transports</source>
        <translation>מובילים</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="215"/>
        <source>Decoders</source>
        <translation>מפענחים</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="226"/>
        <source>Engines</source>
        <translation>מנועים</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="238"/>
        <source>Effects</source>
        <translation>אפקטים</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="249"/>
        <source>Visualization</source>
        <translation>חיזוי</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>General</source>
        <translation>כללי</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="272"/>
        <source>Output</source>
        <translation>פלט</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="283"/>
        <source>File Dialogs</source>
        <translation>תיבות דו שיח קובץ</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="295"/>
        <source>User Interfaces</source>
        <translation>ממשקי משתמש</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="354"/>
        <source>&lt;Autodetect&gt;</source>
        <translation>&lt;איתור אוטומטי&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="355"/>
        <source>Brazilian Portuguese</source>
        <translation>ברזילאית פורטוגזית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="356"/>
        <source>Chinese Simplified</source>
        <translation>סינית מפושטת</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="357"/>
        <source>Chinese Traditional</source>
        <translation>סינית מסורתית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="358"/>
        <source>Czech</source>
        <translation>צ׳כית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="359"/>
        <source>Dutch</source>
        <translation>הולנדית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="360"/>
        <source>English</source>
        <translation>אנגלית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="361"/>
        <source>French</source>
        <translation>צרפתית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="362"/>
        <source>Galician</source>
        <translation>גליצאית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="363"/>
        <source>German</source>
        <translation>גרמנית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="364"/>
        <source>Greek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="365"/>
        <source>Hebrew</source>
        <translation>עברית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="366"/>
        <source>Hungarian</source>
        <translation>הונגרית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="367"/>
        <source>Indonesian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="368"/>
        <source>Italian</source>
        <translation>איטלקית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="369"/>
        <source>Japanese</source>
        <translation>יפנית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="370"/>
        <source>Kazakh</source>
        <translation>קזחית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="371"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="372"/>
        <source>Lithuanian</source>
        <translation>ליטאית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="373"/>
        <source>Polish</source>
        <translation>פולנית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="374"/>
        <source>Portuguese</source>
        <translation>פורטוגזית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="375"/>
        <source>Russian</source>
        <translation>רוסית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="376"/>
        <source>Serbian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="377"/>
        <source>Slovak</source>
        <translation>סלובקית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="378"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="379"/>
        <source>Spanish</source>
        <translation>ספרדית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <source>Turkish</source>
        <translation>טורקית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="381"/>
        <source>Ukrainian</source>
        <translation>אוקראינית</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="382"/>
        <source>Serbian (Ijekavian)</source>
        <translation>סרבית (Ijekavian)</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="383"/>
        <source>Serbian (Ekavian)</source>
        <translation>סרבית (Ekavian)</translation>
    </message>
</context>
<context>
    <name>CoverEditor</name>
    <message>
        <location filename="../forms/covereditor.ui" line="22"/>
        <source>Image source:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="76"/>
        <source>Load</source>
        <translation>טען</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="83"/>
        <source>Delete</source>
        <translation>מחק</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="90"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="34"/>
        <source>External file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="35"/>
        <source>Tag</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverViewer</name>
    <message>
        <location filename="../coverviewer.cpp" line="35"/>
        <source>&amp;Save As...</source>
        <translation>&amp;שמירה בשם...</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="68"/>
        <source>Save Cover As</source>
        <translation>שמירת כיסוי בתור</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="70"/>
        <location filename="../coverviewer.cpp" line="83"/>
        <source>Images</source>
        <translation>תמונות</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="81"/>
        <source>Open Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CueEditor</name>
    <message>
        <location filename="../forms/cueeditor.ui" line="40"/>
        <source>Load</source>
        <translation type="unfinished">טען</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="47"/>
        <source>Delete</source>
        <translation type="unfinished">מחק</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="54"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <source>Open CUE File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <location filename="../cueeditor.cpp" line="150"/>
        <source>CUE Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="148"/>
        <source>Save CUE File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../forms/detailsdialog.ui" line="14"/>
        <source>Details</source>
        <translation>פרטים</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="44"/>
        <source>Open the directory containing this file</source>
        <translation>פתח את המדור שמכיל את קובץ זה</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="47"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="63"/>
        <source>Summary</source>
        <translation>סיכום</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="207"/>
        <source>%1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="257"/>
        <source>Cover</source>
        <translation>כיסוי</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="283"/>
        <source>Lyrics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="303"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="304"/>
        <source>Artist</source>
        <translation>אמן</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="305"/>
        <source>Album artist</source>
        <translation>אמן אלבום</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="306"/>
        <source>Album</source>
        <translation>אלבום</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="307"/>
        <source>Comment</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="308"/>
        <source>Genre</source>
        <translation>ז׳אנר</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="309"/>
        <source>Composer</source>
        <translation>מלחין</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="310"/>
        <source>Year</source>
        <translation>שנה</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="311"/>
        <source>Track</source>
        <translation>רצועה</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="312"/>
        <source>Disc number</source>
        <translation>מספר דיסק</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="331"/>
        <source>Duration</source>
        <translation>משך</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>Bitrate</source>
        <translation>שיעור סיביות</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Sample rate</source>
        <translation>שיעור דגימה</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Hz</source>
        <translation>הרץ</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="336"/>
        <source>Channels</source>
        <translation>ערוצים</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>Sample size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="338"/>
        <source>Format name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>File size</source>
        <translation>גודל קובץ</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>Yes</source>
        <translation>כן</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>No</source>
        <translation>לא</translation>
    </message>
</context>
<context>
    <name>JumpToTrackDialog</name>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="14"/>
        <source>Jump To Track</source>
        <translation>קפוץ אל רצועה</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="46"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="85"/>
        <location filename="../jumptotrackdialog.cpp" line="120"/>
        <location filename="../jumptotrackdialog.cpp" line="151"/>
        <source>Queue</source>
        <translation>תור</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="105"/>
        <source>Jump To</source>
        <translation>קפוץ אל</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="95"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="96"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="118"/>
        <location filename="../jumptotrackdialog.cpp" line="149"/>
        <source>Unqueue</source>
        <translation>בטל הזנה לתור</translation>
    </message>
</context>
<context>
    <name>MetaDataFormatterMenu</name>
    <message>
        <location filename="../metadataformattermenu.cpp" line="27"/>
        <source>Artist</source>
        <translation>אמן</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="28"/>
        <source>Album</source>
        <translation>אלבום</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="29"/>
        <source>Album Artist</source>
        <translation>אמן אלבום</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="32"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="33"/>
        <source>Track Number</source>
        <translation>מספר רצועה</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="34"/>
        <source>Two-digit Track Number</source>
        <translation>מספר רצועה דו ספרתי</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="38"/>
        <source>Track Index</source>
        <translation>אינדקס רצועה</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="40"/>
        <source>Genre</source>
        <translation>ז׳אנר</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="41"/>
        <source>Comment</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="42"/>
        <source>Composer</source>
        <translation>מלחין</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="47"/>
        <location filename="../metadataformattermenu.cpp" line="61"/>
        <source>Duration</source>
        <translation>משך</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="55"/>
        <source>Artist - Album</source>
        <translation type="unfinished">אמן - אלבום</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>Duration | Format | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>Duration | Format | Bitrate | Sample rate </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>Year | Duration | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="68"/>
        <source>Parent Directory Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="70"/>
        <source>Bitrate</source>
        <translation>שיעור סיביות</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="71"/>
        <source>Sample Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="72"/>
        <source>Number of Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="73"/>
        <source>Sample Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="74"/>
        <source>Format</source>
        <translation>פורמט</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="75"/>
        <source>Decoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="78"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="43"/>
        <source>Disc Number</source>
        <translation>מספר תקליטור</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="48"/>
        <source>File Name</source>
        <translation>שם קובץ</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="49"/>
        <source>File Path</source>
        <translation>נתיב קובץ</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="44"/>
        <source>Year</source>
        <translation>שנה</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="51"/>
        <location filename="../metadataformattermenu.cpp" line="57"/>
        <location filename="../metadataformattermenu.cpp" line="65"/>
        <source>Condition</source>
        <translation>תנאי</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="50"/>
        <source>Artist - Title</source>
        <translation>אמן - כותרת</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="56"/>
        <source>Artist - [Year] Album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>%y | %if(%l,%l | ,)%{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="67"/>
        <source>Parent Directory Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayListDownloader</name>
    <message>
        <location filename="../playlistdownloader.cpp" line="123"/>
        <source>Unsupported playlist format</source>
        <translation>פורמט רשימת נגינה לא נתמך</translation>
    </message>
</context>
<context>
    <name>PlayListHeaderModel</name>
    <message>
        <location filename="../playlistheadermodel.cpp" line="35"/>
        <source>Artist - Title</source>
        <translation>אמן - כותרת</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="185"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="186"/>
        <source>Add Column</source>
        <translation>הוסף טור</translation>
    </message>
</context>
<context>
    <name>PlayListManager</name>
    <message>
        <location filename="../playlistmanager.cpp" line="177"/>
        <location filename="../playlistmanager.cpp" line="319"/>
        <source>Playlist</source>
        <translation>רשימת נגינה</translation>
    </message>
</context>
<context>
    <name>PlayListTrack</name>
    <message>
        <location filename="../playlisttrack.cpp" line="245"/>
        <source>Streams</source>
        <translation>זרמים</translation>
    </message>
    <message>
        <location filename="../playlisttrack.cpp" line="250"/>
        <source>Empty group</source>
        <translation>קבוצה ריקה</translation>
    </message>
</context>
<context>
    <name>QmmpUiSettings</name>
    <message>
        <location filename="../qmmpuisettings.cpp" line="39"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpuisettings.cpp" line="64"/>
        <source>Playlist</source>
        <translation>רשימת נגינה</translation>
    </message>
</context>
<context>
    <name>QtFileDialogFactory</name>
    <message>
        <location filename="../qtfiledialog.cpp" line="35"/>
        <source>Qt File Dialog</source>
        <translation>דו שיח קובץ Qt</translation>
    </message>
</context>
<context>
    <name>ShortcutDialog</name>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="14"/>
        <source>Change Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="29"/>
        <source>Press the key combination you want to assign</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="52"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagEditor</name>
    <message>
        <location filename="../forms/tageditor.ui" line="14"/>
        <source>Tag Editor</source>
        <translation>עורך תגית</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="38"/>
        <source>Title:</source>
        <translation>כותרת:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="64"/>
        <source>Artist:</source>
        <translation>אמן:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="90"/>
        <source>Album:</source>
        <translation>אלבום:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="113"/>
        <source>Album artist:</source>
        <translation>אמן אלבום:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="123"/>
        <source>Composer:</source>
        <translation>מלחין:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="143"/>
        <source>Genre:</source>
        <translation>ז׳אנר:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="169"/>
        <source>Track:</source>
        <translation>רצועה:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="190"/>
        <location filename="../forms/tageditor.ui" line="228"/>
        <location filename="../forms/tageditor.ui" line="260"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="203"/>
        <source>Year:</source>
        <translation>שנה:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="250"/>
        <source>Disc number:</source>
        <translation>מספר דיסק:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="275"/>
        <source>Comment:</source>
        <translation>הערה:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="311"/>
        <source>Include selected tag in file</source>
        <translation>כלול תגית נבחרת בתוך קובץ</translation>
    </message>
</context>
<context>
    <name>TemplateEditor</name>
    <message>
        <location filename="../forms/templateeditor.ui" line="14"/>
        <source>Template Editor</source>
        <translation>עורך תבנית</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="39"/>
        <source>Reset</source>
        <translation>אפס</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="46"/>
        <source>Insert</source>
        <translation>שבץ</translation>
    </message>
</context>
<context>
    <name>UiHelper</name>
    <message>
        <location filename="../uihelper.cpp" line="136"/>
        <location filename="../uihelper.cpp" line="148"/>
        <source>All Supported Bitstreams</source>
        <translation>כל זרמי הסביות הנתמכים</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="142"/>
        <source>Select one or more files to open</source>
        <translation>בחר קובץ אחד או יותר לפתיחה</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="154"/>
        <source>Select one or more files to play</source>
        <translation>בחר קובץ אחד או יותר לניגון</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="162"/>
        <source>Choose a directory</source>
        <translation>בחר מדור</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="178"/>
        <location filename="../uihelper.cpp" line="202"/>
        <source>Playlist Files</source>
        <translation>קבצי רשימת נגינה</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="180"/>
        <source>Open Playlist</source>
        <translation>פתח רשימת נגינה</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="205"/>
        <location filename="../uihelper.cpp" line="224"/>
        <source>Save Playlist</source>
        <translation>שמור רשימת נגינה</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="224"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualMenu</name>
    <message>
        <location filename="../visualmenu.cpp" line="26"/>
        <source>Visualization</source>
        <translation type="unfinished">חיזוי</translation>
    </message>
</context>
<context>
    <name>WinFileAssocPage</name>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="43"/>
        <source>Media files handled by Qmmp:</source>
        <translation>קבצי מדיה אשר מטופלים על ידי Qmmp:</translation>
    </message>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="17"/>
        <source>Select All</source>
        <translation>בחר הכל</translation>
    </message>
</context>
</TS>
