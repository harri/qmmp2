<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../forms/aboutdialog.ui" line="14"/>
        <source>About Qmmp</source>
        <translation>Acerca de Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="49"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="63"/>
        <source>Authors</source>
        <translation>Autores</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="77"/>
        <source>Translators</source>
        <translation>Traductores</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="91"/>
        <source>Thanks To</source>
        <translation>Gracias a</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="105"/>
        <source>License Agreement</source>
        <translation>Contrato de licencia</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="69"/>
        <source>Qt-based Multimedia Player (Qmmp)</source>
        <translation>Qt-based Multimedia Player (Qmmp)</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="72"/>
        <source>Version: %1</source>
        <translation>Versión %1</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="79"/>
        <source>(c) %1-%2 Qmmp Development Team</source>
        <translation>(c) %1-%2 Equipo de desarrollo de Qmmp</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="84"/>
        <source>Transports:</source>
        <translation>Transportes:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="90"/>
        <source>Decoders:</source>
        <translation>Decodificadores:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="98"/>
        <source>Engines:</source>
        <translation>Motores:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="105"/>
        <source>Effects:</source>
        <translation>Efectos:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="134"/>
        <source>File dialogs:</source>
        <translation>Diálogos de archivo:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="143"/>
        <source>User interfaces:</source>
        <translation>Interfaces de usuario:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="126"/>
        <source>Output plugins:</source>
        <translation>Módulos de salida: </translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="73"/>
        <source>Qt version: %1 (compiled with %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="74"/>
        <source>Qt platform: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="75"/>
        <source>System: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="76"/>
        <source>Build ABI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="113"/>
        <source>Visual plugins:</source>
        <translation>Módulos visuales: </translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="120"/>
        <source>General plugins:</source>
        <translation>Módulos generales: </translation>
    </message>
</context>
<context>
    <name>AddUrlDialog</name>
    <message>
        <location filename="../forms/addurldialog.ui" line="14"/>
        <source>Enter URL to add</source>
        <translation>Ingresar URL a añadir</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="55"/>
        <source>&amp;Add</source>
        <translation>&amp;Agregar</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="62"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../addurldialog.cpp" line="90"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
</context>
<context>
    <name>ColorWidget</name>
    <message>
        <location filename="../colorwidget.cpp" line="46"/>
        <source>Select Color</source>
        <translation>Selector de color</translation>
    </message>
</context>
<context>
    <name>ColumnEditor</name>
    <message>
        <location filename="../forms/columneditor.ui" line="14"/>
        <source>Edit Column</source>
        <translation>Editar Columna</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="36"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="76"/>
        <source>Format:</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="64"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="29"/>
        <source>Type:</source>
        <translation>Tipo:</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="86"/>
        <source>Artist</source>
        <translation>Intérprete</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="87"/>
        <source>Album</source>
        <translation>Álbum</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="91"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="94"/>
        <source>Genre</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="95"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="96"/>
        <source>Composer</source>
        <translation>Compositor</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="97"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="102"/>
        <source>Year</source>
        <translation>Año</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="101"/>
        <source>Track Index</source>
        <translation>Índice de pistas</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="88"/>
        <source>Artist - Album</source>
        <translation>Artista - Álbum</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="89"/>
        <source>Artist - Title</source>
        <translation>Artista - Título</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="90"/>
        <source>Album Artist</source>
        <translation>Artista del álbum</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="92"/>
        <source>Track Number</source>
        <translation>Número de pista</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="93"/>
        <source>Two-digit Track Number</source>
        <translation>Número de pista de dos dígitos</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="98"/>
        <source>Disc Number</source>
        <translation>Número de disco</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="99"/>
        <source>File Name</source>
        <translation>Nombre de archivo</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="100"/>
        <source>File Path</source>
        <translation>Ruta del archivo</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="103"/>
        <source>Parent Directory Name</source>
        <translation>Nombre de Directorio Padre</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="104"/>
        <source>Parent Directory Path</source>
        <translation>Ruta del Directorio Principal</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="105"/>
        <source>Custom</source>
        <translation>Personalizar</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../forms/configdialog.ui" line="14"/>
        <source>Qmmp Settings</source>
        <translation>Configuración de Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="58"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="67"/>
        <source>Plugins</source>
        <translation>Módulos</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="76"/>
        <source>Advanced</source>
        <translation>Avanzado</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="85"/>
        <source>Connectivity</source>
        <translation>Conectividad</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="94"/>
        <location filename="../forms/configdialog.ui" line="996"/>
        <source>Audio</source>
        <translation>Sonido</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="153"/>
        <source>Metadata</source>
        <translation>Metainformación</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="183"/>
        <source>Convert %20 to blanks</source>
        <translation>Convertir los %20 en espacios</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="159"/>
        <source>Load metadata from files</source>
        <translation>Cargar la metainformación de los archivos</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="176"/>
        <source>Convert underscores to blanks</source>
        <translation>Convertir los guiones bajos en espacios</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="226"/>
        <source>Group format:</source>
        <translation>Formato del grupo:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="242"/>
        <location filename="../forms/configdialog.ui" line="267"/>
        <location filename="../forms/configdialog.ui" line="710"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="169"/>
        <source>Read tags while loading a playlist</source>
        <translation>Leer etiquetas mientras se carga la lista de reproducción</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="193"/>
        <source>Group Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="202"/>
        <source>Group size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="219"/>
        <source>Show dividing line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="251"/>
        <source>Extra row format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="276"/>
        <source>Show extra row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="283"/>
        <source>Show cover</source>
        <translation>Mostrar carátula</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="293"/>
        <source>Directory Scanning Options</source>
        <translation>Opciones de escaneo de directorio</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="299"/>
        <source>Restrict files to:</source>
        <translation>Restringir archivos a:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="313"/>
        <location filename="../forms/configdialog.ui" line="588"/>
        <source>Exclude files:</source>
        <translation>Excluir archivos:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="330"/>
        <source>Miscellaneous</source>
        <translation>Miceláneos</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="336"/>
        <source>Auto-save playlist when modified</source>
        <translation>Auto-guardar lista de reproducción al modificarse</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="343"/>
        <source>Clear previous playlist when opening new one</source>
        <translation>Limpiar lista de reproducción previa cuando se abre una nueva</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="396"/>
        <location filename="../configdialog.cpp" line="341"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="413"/>
        <location filename="../configdialog.cpp" line="344"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="459"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="464"/>
        <source>Filename</source>
        <translation>Nombre del archivo</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="476"/>
        <source>Look and Feel</source>
        <translation>Apariencia y comportamiento</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="482"/>
        <source>Language:</source>
        <translation>Idioma:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="515"/>
        <source>Display average bitrate</source>
        <translation>Mostrar tasa de bits promedio</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="525"/>
        <source>Playback</source>
        <translation>Reproducción</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="531"/>
        <source>Continue playback on startup</source>
        <translation>Continuar la reproducción al iniciar</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="538"/>
        <source>Determine file type by content</source>
        <translation>Determine tipo de archivos por contenido</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="545"/>
        <source>Add files from command line to this playlist:</source>
        <translation>Añadir archivos desde consola hacia lista de reproducción</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="562"/>
        <source>Cover Image Retrieve</source>
        <translation>Obtener las imagenes de carátula</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="568"/>
        <source>Use separate image files</source>
        <translation>Usar archivos de imágen separados</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="578"/>
        <source>Include files:</source>
        <translation>Incluir archivos:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="600"/>
        <source>Recursive search depth:</source>
        <translation>Profundidad de la búsqueda recursiva:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="632"/>
        <source>URL Dialog</source>
        <translation>Diálogo URL</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="638"/>
        <source>Auto-paste URL from clipboard</source>
        <translation>Auto-pegar URL desde portapapeles</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="648"/>
        <source>CUE Editor</source>
        <translation>Editor CUE</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="654"/>
        <source>Use system font</source>
        <translation>Usar tipografía del sistema </translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="675"/>
        <source>Font:</source>
        <translation>Tipografía:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="700"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="739"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="751"/>
        <source>Enable proxy usage</source>
        <translation>Habilitar el uso de proxy</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="758"/>
        <source>Proxy type:</source>
        <translation>Tipo de proxy:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="768"/>
        <source>Proxy host name:</source>
        <translation>Nombre del servidor proxy: </translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="785"/>
        <source>Proxy port:</source>
        <translation>Puerto del proxy: </translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="802"/>
        <source>Use authentication with proxy</source>
        <translation>Usar autentificación con el proxy</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="809"/>
        <source>Proxy user name:</source>
        <translation>Usuario del proxy:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="826"/>
        <source>Proxy password:</source>
        <translation>Contraseña del proxy:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="866"/>
        <source>Replay Gain</source>
        <translation>Normalización</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="872"/>
        <source>Replay Gain mode:</source>
        <translation>Método de normalización:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="889"/>
        <source>Preamp:</source>
        <translation>Preamp:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="921"/>
        <location filename="../forms/configdialog.ui" line="966"/>
        <source>dB</source>
        <translation>dB</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="934"/>
        <source>Default gain:</source>
        <translation>Normalización predeterminada:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="986"/>
        <source>Use  peak info to prevent clipping</source>
        <translation>Procesar picos para evitar cortes</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1002"/>
        <source>Buffer size:</source>
        <translation>Tamaño del buffer:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1028"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1061"/>
        <source>Use software volume control</source>
        <translation>Usar control de volumen por software</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1101"/>
        <source>Use two passes for equalizer</source>
        <translation>Usar dos pases para equalizador</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1044"/>
        <source>Volume adjustment step:</source>
        <translation>Paso de ajuste de volúmen</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="350"/>
        <source>Skip already existing tracks when adding</source>
        <translation>Saltar pistas existentes al agregar</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="357"/>
        <source>Stop playback after removing of current track</source>
        <translation>Detener la reproducción al eliminar la pista actual</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1068"/>
        <source>Output bit depth:</source>
        <translation>Salida de profundidad de bits</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1009"/>
        <source>Use dithering</source>
        <translation>Usar tramado (dithering)</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="63"/>
        <source>1 row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="64"/>
        <source>3 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="65"/>
        <source>4 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="66"/>
        <source>5 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="67"/>
        <source>Track</source>
        <translation>Pista</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="68"/>
        <source>Album</source>
        <translation>Álbum</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="69"/>
        <source>Disabled</source>
        <translation>Deshabilitado</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="76"/>
        <source>HTTP</source>
        <translation>HTTP</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="77"/>
        <source>SOCKS5</source>
        <translation>SOCKS5</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="204"/>
        <source>Transports</source>
        <translation>Transportes</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="215"/>
        <source>Decoders</source>
        <translation>Decodificadores</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="226"/>
        <source>Engines</source>
        <translation>Motores</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="238"/>
        <source>Effects</source>
        <translation>Efectos</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="249"/>
        <source>Visualization</source>
        <translation>Visualización</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="272"/>
        <source>Output</source>
        <translation>Salida</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="283"/>
        <source>File Dialogs</source>
        <translation>Diálogos de archivo</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="295"/>
        <source>User Interfaces</source>
        <translation>Interfaz de usuario</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="354"/>
        <source>&lt;Autodetect&gt;</source>
        <translation>&lt;Autodetectar&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="355"/>
        <source>Brazilian Portuguese</source>
        <translation>Portugués Brasil</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="356"/>
        <source>Chinese Simplified</source>
        <translation>Chino simplificado</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="357"/>
        <source>Chinese Traditional</source>
        <translation>Chino tradicional</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="358"/>
        <source>Czech</source>
        <translation>Checo</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="359"/>
        <source>Dutch</source>
        <translation>Holandés</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="360"/>
        <source>English</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="361"/>
        <source>French</source>
        <translation>Fránces</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="362"/>
        <source>Galician</source>
        <translation>Gallego</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="363"/>
        <source>German</source>
        <translation>Alemán</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="364"/>
        <source>Greek</source>
        <translation>Griego</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="365"/>
        <source>Hebrew</source>
        <translation>Hebreo</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="366"/>
        <source>Hungarian</source>
        <translation>Húngaro</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="367"/>
        <source>Indonesian</source>
        <translation>Indonesio</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="368"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="369"/>
        <source>Japanese</source>
        <translation>Japonés</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="370"/>
        <source>Kazakh</source>
        <translation>Kazajo</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="371"/>
        <source>Korean</source>
        <translation>Coreano</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="372"/>
        <source>Lithuanian</source>
        <translation>Lituano</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="373"/>
        <source>Polish</source>
        <translation>Polaco</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="374"/>
        <source>Portuguese</source>
        <translation>Portugués</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="375"/>
        <source>Russian</source>
        <translation>Ruso</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="376"/>
        <source>Serbian</source>
        <translation>Serbio</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="377"/>
        <source>Slovak</source>
        <translation>Eslovaco</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="378"/>
        <source>Swedish</source>
        <translation>Sueco</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="379"/>
        <source>Spanish</source>
        <translation>Español</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <source>Turkish</source>
        <translation>Turco</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="381"/>
        <source>Ukrainian</source>
        <translation>Ucraniano</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="382"/>
        <source>Serbian (Ijekavian)</source>
        <translation>Serbio (Ijekavian)</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="383"/>
        <source>Serbian (Ekavian)</source>
        <translation>Serbio (Ekavian)</translation>
    </message>
</context>
<context>
    <name>CoverEditor</name>
    <message>
        <location filename="../forms/covereditor.ui" line="22"/>
        <source>Image source:</source>
        <translation>Fuente imagen:</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="76"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="83"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="90"/>
        <source>Save as...</source>
        <translation>Guardar como...</translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="34"/>
        <source>External file</source>
        <translation>Archivo externo</translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="35"/>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
</context>
<context>
    <name>CoverViewer</name>
    <message>
        <location filename="../coverviewer.cpp" line="35"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Guardar como...</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="68"/>
        <source>Save Cover As</source>
        <translation>Guardar carátula como</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="70"/>
        <location filename="../coverviewer.cpp" line="83"/>
        <source>Images</source>
        <translation>Imágenes</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="81"/>
        <source>Open Image</source>
        <translation>Abrir imagen</translation>
    </message>
</context>
<context>
    <name>CueEditor</name>
    <message>
        <location filename="../forms/cueeditor.ui" line="40"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="47"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="54"/>
        <source>Save as...</source>
        <translation>Guardar como...</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <source>Open CUE File</source>
        <translation>Abrir archivo CUE</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <location filename="../cueeditor.cpp" line="150"/>
        <source>CUE Files</source>
        <translation>Archivos CUE</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="148"/>
        <source>Save CUE File</source>
        <translation>Guardar archivo CUE</translation>
    </message>
</context>
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../forms/detailsdialog.ui" line="14"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="44"/>
        <source>Open the directory containing this file</source>
        <translation>Abrir el directorio que contiene este archivo</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="47"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="63"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="207"/>
        <source>%1/%2</source>
        <translation>%1/%2</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="257"/>
        <source>Cover</source>
        <translation>Portada</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="283"/>
        <source>Lyrics</source>
        <translation>Letras</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="303"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="304"/>
        <source>Artist</source>
        <translation>Intérprete</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="305"/>
        <source>Album artist</source>
        <translation>Artista del álbum</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="306"/>
        <source>Album</source>
        <translation>Álbum</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="307"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="308"/>
        <source>Genre</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="309"/>
        <source>Composer</source>
        <translation>Compositor</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="310"/>
        <source>Year</source>
        <translation>Año</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="311"/>
        <source>Track</source>
        <translation>Pista</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="312"/>
        <source>Disc number</source>
        <translation>Número de disco</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="331"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>Bitrate</source>
        <translation>Tasa de bits</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>kbps</source>
        <translation>kbps</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Sample rate</source>
        <translation>Frecuencia</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="336"/>
        <source>Channels</source>
        <translation>Canales</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>Sample size</source>
        <translation>Tamaño de muestra</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>bits</source>
        <translation>bits</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="338"/>
        <source>Format name</source>
        <translation>Nombre de formato</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>File size</source>
        <translation>Tamaño del archivo</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>No</source>
        <translation>No</translation>
    </message>
</context>
<context>
    <name>JumpToTrackDialog</name>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="14"/>
        <source>Jump To Track</source>
        <translation>Saltar a pista</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="46"/>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="85"/>
        <location filename="../jumptotrackdialog.cpp" line="120"/>
        <location filename="../jumptotrackdialog.cpp" line="151"/>
        <source>Queue</source>
        <translation>Cola</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="105"/>
        <source>Jump To</source>
        <translation>Ir a</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="95"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="96"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="118"/>
        <location filename="../jumptotrackdialog.cpp" line="149"/>
        <source>Unqueue</source>
        <translation>Quitar de la cola</translation>
    </message>
</context>
<context>
    <name>MetaDataFormatterMenu</name>
    <message>
        <location filename="../metadataformattermenu.cpp" line="27"/>
        <source>Artist</source>
        <translation>Intérprete</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="28"/>
        <source>Album</source>
        <translation>Álbum</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="29"/>
        <source>Album Artist</source>
        <translation>Artista del álbum</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="32"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="33"/>
        <source>Track Number</source>
        <translation>Número de pista</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="34"/>
        <source>Two-digit Track Number</source>
        <translation>Número de pista de dos dígitos</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="38"/>
        <source>Track Index</source>
        <translation>Índice de pistas</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="40"/>
        <source>Genre</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="41"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="42"/>
        <source>Composer</source>
        <translation>Compositor</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="47"/>
        <location filename="../metadataformattermenu.cpp" line="61"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="55"/>
        <source>Artist - Album</source>
        <translation>Artista - Álbum</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>Duration | Format | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>Duration | Format | Bitrate | Sample rate </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>Year | Duration | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="68"/>
        <source>Parent Directory Path</source>
        <translation>Ruta del Directorio Principal</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="70"/>
        <source>Bitrate</source>
        <translation>Tasa de bits</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="71"/>
        <source>Sample Rate</source>
        <translation>Frecuencia de muestreo</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="72"/>
        <source>Number of Channels</source>
        <translation>Número de Canales</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="73"/>
        <source>Sample Size</source>
        <translation>Tamaño de muestra</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="74"/>
        <source>Format</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="75"/>
        <source>Decoder</source>
        <translation>Decodificador</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="78"/>
        <source>File Size</source>
        <translation>Tamaño de Archivo</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="43"/>
        <source>Disc Number</source>
        <translation>Número de disco</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="48"/>
        <source>File Name</source>
        <translation>Nombre de archivo</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="49"/>
        <source>File Path</source>
        <translation>Ruta del archivo</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="44"/>
        <source>Year</source>
        <translation>Año</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="51"/>
        <location filename="../metadataformattermenu.cpp" line="57"/>
        <location filename="../metadataformattermenu.cpp" line="65"/>
        <source>Condition</source>
        <translation>Condición</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="50"/>
        <source>Artist - Title</source>
        <translation>Artista - Título</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="56"/>
        <source>Artist - [Year] Album</source>
        <translation>Artista - [Año] Álbum</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>%y | %if(%l,%l | ,)%{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="67"/>
        <source>Parent Directory Name</source>
        <translation>Nombre de Directorio Padre</translation>
    </message>
</context>
<context>
    <name>PlayListDownloader</name>
    <message>
        <location filename="../playlistdownloader.cpp" line="123"/>
        <source>Unsupported playlist format</source>
        <translation>Formato de lista sin soporte</translation>
    </message>
</context>
<context>
    <name>PlayListHeaderModel</name>
    <message>
        <location filename="../playlistheadermodel.cpp" line="35"/>
        <source>Artist - Title</source>
        <translation>Artista - Título</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="185"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="186"/>
        <source>Add Column</source>
        <translation>Agregar columna</translation>
    </message>
</context>
<context>
    <name>PlayListManager</name>
    <message>
        <location filename="../playlistmanager.cpp" line="177"/>
        <location filename="../playlistmanager.cpp" line="319"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
</context>
<context>
    <name>PlayListTrack</name>
    <message>
        <location filename="../playlisttrack.cpp" line="245"/>
        <source>Streams</source>
        <translation>Flujos</translation>
    </message>
    <message>
        <location filename="../playlisttrack.cpp" line="250"/>
        <source>Empty group</source>
        <translation>Grupo vacío</translation>
    </message>
</context>
<context>
    <name>QmmpUiSettings</name>
    <message>
        <location filename="../qmmpuisettings.cpp" line="39"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpuisettings.cpp" line="64"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
</context>
<context>
    <name>QtFileDialogFactory</name>
    <message>
        <location filename="../qtfiledialog.cpp" line="35"/>
        <source>Qt File Dialog</source>
        <translation>Diálogo de archivos Qt</translation>
    </message>
</context>
<context>
    <name>ShortcutDialog</name>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="14"/>
        <source>Change Shortcut</source>
        <translation>Cambiar atajo</translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="29"/>
        <source>Press the key combination you want to assign</source>
        <translation>Pulse la combinación de teclas que quiere asignar</translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="52"/>
        <source>Clear</source>
        <translation>Limpiar</translation>
    </message>
</context>
<context>
    <name>TagEditor</name>
    <message>
        <location filename="../forms/tageditor.ui" line="14"/>
        <source>Tag Editor</source>
        <translation>Editor de etiquetas</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="38"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="64"/>
        <source>Artist:</source>
        <translation>Intérprete:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="90"/>
        <source>Album:</source>
        <translation>Álbum:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="113"/>
        <source>Album artist:</source>
        <translation>Artista del álbum:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="123"/>
        <source>Composer:</source>
        <translation>Compositor:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="143"/>
        <source>Genre:</source>
        <translation>Género:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="169"/>
        <source>Track:</source>
        <translation>Pista:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="190"/>
        <location filename="../forms/tageditor.ui" line="228"/>
        <location filename="../forms/tageditor.ui" line="260"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="203"/>
        <source>Year:</source>
        <translation>Año:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="250"/>
        <source>Disc number:</source>
        <translation>Número de disco:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="275"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="311"/>
        <source>Include selected tag in file</source>
        <translation>Incluir la etiqueta seleccionada en el archivo</translation>
    </message>
</context>
<context>
    <name>TemplateEditor</name>
    <message>
        <location filename="../forms/templateeditor.ui" line="14"/>
        <source>Template Editor</source>
        <translation>Editor de plantillas</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="39"/>
        <source>Reset</source>
        <translation>Restaurar</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="46"/>
        <source>Insert</source>
        <translation>Insertar</translation>
    </message>
</context>
<context>
    <name>UiHelper</name>
    <message>
        <location filename="../uihelper.cpp" line="136"/>
        <location filename="../uihelper.cpp" line="148"/>
        <source>All Supported Bitstreams</source>
        <translation>Todos los flujos de bits soportados</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="142"/>
        <source>Select one or more files to open</source>
        <translation>Seleccione uno o más archivos a abrir</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="154"/>
        <source>Select one or more files to play</source>
        <translation>Seleccione uno o más archivos a reproducir</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="162"/>
        <source>Choose a directory</source>
        <translation>Elija un directorio</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="178"/>
        <location filename="../uihelper.cpp" line="202"/>
        <source>Playlist Files</source>
        <translation>Archivos listas de reproducción</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="180"/>
        <source>Open Playlist</source>
        <translation>Abrir lista de reproducción</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="205"/>
        <location filename="../uihelper.cpp" line="224"/>
        <source>Save Playlist</source>
        <translation>Guardar lista de reproducción</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="224"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1 ya existe.
¿Quiere reemplazarlo?</translation>
    </message>
</context>
<context>
    <name>VisualMenu</name>
    <message>
        <location filename="../visualmenu.cpp" line="26"/>
        <source>Visualization</source>
        <translation>Visualización</translation>
    </message>
</context>
<context>
    <name>WinFileAssocPage</name>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="43"/>
        <source>Media files handled by Qmmp:</source>
        <translation>Archivos de medios gestionados por Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="17"/>
        <source>Select All</source>
        <translation>Seleccionar todo</translation>
    </message>
</context>
</TS>
