<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>OssSettingsDialog</name>
    <message>
        <location filename="../osssettingsdialog.ui" line="14"/>
        <source>OSS Plugin Settings</source>
        <translation>Ρυθμίσεις πρόσθετου OSS</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation>Ρυθμίσεις συσκευής</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation>Συσκευή ήχου</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="70"/>
        <source>Mixer device</source>
        <translation>Συσκευή μίκτη</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="102"/>
        <source>Advanced Settings</source>
        <translation>Προηγμένες ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="123"/>
        <source>Soundcard</source>
        <translation>Κάρτα ήχου</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="196"/>
        <source>Buffer time (ms):</source>
        <translation>Χρόνος ενδιάμεσης μνήμης (ms)</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="206"/>
        <source>Period time (ms):</source>
        <translation>Χρόνος περιόδου (ms):</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="236"/>
        <source>PCM over Master</source>
        <translation>PCM πάνω από Master</translation>
    </message>
</context>
<context>
    <name>OutputOSSFactory</name>
    <message>
        <location filename="../outputossfactory.cpp" line="36"/>
        <source>OSS Plugin</source>
        <translation>Πρόσθετο OSS</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="55"/>
        <source>About OSS Output Plugin</source>
        <translation>Σχετικά με το πρόσθετο OSS</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="56"/>
        <source>Qmmp OSS Output Plugin</source>
        <translation>Qmmp σχετικά με το πρόσθετο OSS</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="57"/>
        <source>Written by: Yuriy Zhuravlev &lt;slalkerg@gmail.com&gt;</source>
        <translation>Γράφτηκε από τον: Yuriy Zhuravlev &lt;slalkerg@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="58"/>
        <source>Based on code by: Brad Hughes &lt;bhughes@trolltech.com&gt;</source>
        <translation>Βασισμένο στον κώδικα του: Brad Hughes &lt;bhughes@trolltech.com&gt;</translation>
    </message>
</context>
</TS>
