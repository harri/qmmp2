<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>OssSettingsDialog</name>
    <message>
        <location filename="../osssettingsdialog.ui" line="14"/>
        <source>OSS Plugin Settings</source>
        <translation>OSS Eklenti Ayarları</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation>Aygıt Ayarları</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation>Ses aygıtı</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="70"/>
        <source>Mixer device</source>
        <translation>Karıştırıcı aygıtı</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="102"/>
        <source>Advanced Settings</source>
        <translation>Gelişmiş Ayarlar</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="123"/>
        <source>Soundcard</source>
        <translation>Ses kartı</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="196"/>
        <source>Buffer time (ms):</source>
        <translation>Arabellek süresi (ms):</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="206"/>
        <source>Period time (ms):</source>
        <translation>Periyot süresi (ms):</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="236"/>
        <source>PCM over Master</source>
        <translation>Master üzeri PCM</translation>
    </message>
</context>
<context>
    <name>OutputOSSFactory</name>
    <message>
        <location filename="../outputossfactory.cpp" line="36"/>
        <source>OSS Plugin</source>
        <translation>OSS Eklentisi</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="55"/>
        <source>About OSS Output Plugin</source>
        <translation>OSS Çıktı Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="56"/>
        <source>Qmmp OSS Output Plugin</source>
        <translation>Qmmp OSS Çıktı Eklentisi</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="57"/>
        <source>Written by: Yuriy Zhuravlev &lt;slalkerg@gmail.com&gt;</source>
        <translation>Yazan: Yuriy Zhuravlev &lt;slalkerg@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="58"/>
        <source>Based on code by: Brad Hughes &lt;bhughes@trolltech.com&gt;</source>
        <translation>Brad Hughes &lt;bhughes@trolltech.com&gt; tarafından yazılan kod temellidir</translation>
    </message>
</context>
</TS>
