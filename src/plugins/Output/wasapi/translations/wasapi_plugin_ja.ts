<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>OutputWASAPIFactory</name>
    <message>
        <location filename="../outputwasapifactory.cpp" line="29"/>
        <source>WASAPI Plugin</source>
        <translation>WASAPI プラグイン</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="53"/>
        <source>About WASAPI Output Plugin</source>
        <translation>WASAPI プラグインについて</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="54"/>
        <source>Qmmp WASAPI Output Plugin</source>
        <translation>QMMP WASAPI 出力プラグイン</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>WASAPISettingsDialog</name>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="14"/>
        <source>WASAPI Plugin Settings</source>
        <translation>WASAPI プラグイン設定</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="35"/>
        <source>Device:</source>
        <translation>出力デバイス:</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="55"/>
        <source>Buffer size:</source>
        <translation>バッファーの大きさ:</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="62"/>
        <source>ms</source>
        <translation>ミリ秒</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="78"/>
        <source>Exclusive mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.cpp" line="65"/>
        <source>Default</source>
        <translation>標準</translation>
    </message>
</context>
</TS>
