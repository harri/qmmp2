<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>OutputWASAPIFactory</name>
    <message>
        <location filename="../outputwasapifactory.cpp" line="29"/>
        <source>WASAPI Plugin</source>
        <translation>תוסף WASAPI</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="53"/>
        <source>About WASAPI Output Plugin</source>
        <translation>אודות תוסף פלט WASAPI</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="54"/>
        <source>Qmmp WASAPI Output Plugin</source>
        <translation>תוסף פלט WASAPI של Qmmp</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WASAPISettingsDialog</name>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="14"/>
        <source>WASAPI Plugin Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="35"/>
        <source>Device:</source>
        <translation type="unfinished">התקן:</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="55"/>
        <source>Buffer size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="62"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="78"/>
        <source>Exclusive mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.cpp" line="65"/>
        <source>Default</source>
        <translation type="unfinished">משתמט</translation>
    </message>
</context>
</TS>
