<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>OutputQtMultimediaFactory</name>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="32"/>
        <source>Qt Multimedia Plugin</source>
        <translation>Plugin Qt Multimedia</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="56"/>
        <source>About Qt Multimedia Output Plugin</source>
        <translation>Sobre o plugin Qt Multimedia</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="57"/>
        <source>Qmmp Qt Multimedia Output Plugin</source>
        <translation>Plugin Qmmp Qt Multimedia</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="58"/>
        <source>Written by: Ivan Ponomarev &lt;ivantrue@gmail.com&gt;</source>
        <translation>Desenvolvido por: Ivan Ponomarev &lt;ivantrue@gmail.com&gt;</translation>
    </message>
</context>
<context>
    <name>QtMultimediaSettingsDialog</name>
    <message>
        <location filename="../qtmultimediasettingsdialog.ui" line="14"/>
        <source>Qt Multimedia Plugin Settings</source>
        <translation>Preferências do plugin Qt Multimedia</translation>
    </message>
    <message>
        <location filename="../qtmultimediasettingsdialog.ui" line="46"/>
        <source>Device:</source>
        <translation>Dispositivo:</translation>
    </message>
    <message>
        <location filename="../qtmultimediasettingsdialog.cpp" line="36"/>
        <source>Default</source>
        <translation>Pré-definido</translation>
    </message>
</context>
</TS>
