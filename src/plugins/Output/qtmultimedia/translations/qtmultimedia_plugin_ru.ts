<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>OutputQtMultimediaFactory</name>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="32"/>
        <source>Qt Multimedia Plugin</source>
        <translation>Модуль Qt Multimedia</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="56"/>
        <source>About Qt Multimedia Output Plugin</source>
        <translation>О модуле вывода Qt Multimedia</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="57"/>
        <source>Qmmp Qt Multimedia Output Plugin</source>
        <translation>Модуль вывода Qt Multimedia для Qmmp</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="58"/>
        <source>Written by: Ivan Ponomarev &lt;ivantrue@gmail.com&gt;</source>
        <translation>Разработчик: Иван Пономарев &lt;ivantrue@gmail.com&gt;</translation>
    </message>
</context>
<context>
    <name>QtMultimediaSettingsDialog</name>
    <message>
        <location filename="../qtmultimediasettingsdialog.ui" line="14"/>
        <source>Qt Multimedia Plugin Settings</source>
        <translation>Настройки модуля Qt Multimedia</translation>
    </message>
    <message>
        <location filename="../qtmultimediasettingsdialog.ui" line="46"/>
        <source>Device:</source>
        <translation>Устройство:</translation>
    </message>
    <message>
        <location filename="../qtmultimediasettingsdialog.cpp" line="36"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
</context>
</TS>
