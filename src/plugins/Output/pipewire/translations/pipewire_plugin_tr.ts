<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>OutputPipeWireFactory</name>
    <message>
        <location filename="../outputpipewirefactory.cpp" line="30"/>
        <source>PipeWire Plugin</source>
        <translation>PipeWire Eklentisi</translation>
    </message>
    <message>
        <location filename="../outputpipewirefactory.cpp" line="55"/>
        <source>About PipeWire Output Plugin</source>
        <translation>PipeWire Çıktı Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../outputpipewirefactory.cpp" line="56"/>
        <source>Qmmp PipeWire Output Plugin</source>
        <translation>Qmmp PipeWire Çıktı Eklentisi</translation>
    </message>
    <message>
        <location filename="../outputpipewirefactory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
