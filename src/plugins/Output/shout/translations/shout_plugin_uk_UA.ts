<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>OutputShoutFactory</name>
    <message>
        <location filename="../outputshoutfactory.cpp" line="39"/>
        <source>Icecast Plugin</source>
        <translation>Втулок Icecast</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="63"/>
        <source>About Icecast Output Plugin</source>
        <translation>Про втулок виведення Icecast</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="64"/>
        <source>Qmmp Icecast Output Plugin</source>
        <translation>Втулок виведення Icecast для Qmmp</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="65"/>
        <source>Compiled against libshout-%1</source>
        <translation>Зібрано з libshout-%1</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="66"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ShoutSettingsDialog</name>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="14"/>
        <source>Connection Settings</source>
        <translation>Налаштування з&apos;єднання</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="32"/>
        <source>Host:</source>
        <translation>Хост:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="42"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="56"/>
        <source>Mount point:</source>
        <translation>Точка монтування:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="66"/>
        <source>User:</source>
        <translation>Користувач:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="76"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="86"/>
        <source>Quality:</source>
        <translation>Якість:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="106"/>
        <source>Sample rate:</source>
        <translation>Частота дискретизації:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="136"/>
        <source>Public</source>
        <translation>Публічний</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="143"/>
        <source>Hz</source>
        <translation>Гц</translation>
    </message>
</context>
</TS>
