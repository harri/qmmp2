<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt">
<context>
    <name>Oss4SettingsDialog</name>
    <message>
        <location filename="../oss4settingsdialog.ui" line="14"/>
        <source>OSS4 Plugin Settings</source>
        <translation type="unfinished">OSS4 įskiepio nustatymai</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.ui" line="61"/>
        <source>Device:</source>
        <translation type="unfinished">Įrenginys:</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.cpp" line="69"/>
        <source>Default (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutputOSS4Factory</name>
    <message>
        <location filename="../outputoss4factory.cpp" line="36"/>
        <source>OSS4 Plugin</source>
        <translation>OSS4 Įskiepis</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="55"/>
        <source>About OSS4 Output Plugin</source>
        <translation>Apie  OSS4 išvesties įskiepį</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="56"/>
        <source>Qmmp OSS4 Output Plugin</source>
        <translation>Qmmp OSS4 išvesties įskiepis</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Sukūrė: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
