<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>Oss4SettingsDialog</name>
    <message>
        <location filename="../oss4settingsdialog.ui" line="14"/>
        <source>OSS4 Plugin Settings</source>
        <translation>Impostazioni dell&apos;estensione OSS4</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.ui" line="61"/>
        <source>Device:</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.cpp" line="69"/>
        <source>Default (%1)</source>
        <translation>Predefinito (%1)</translation>
    </message>
</context>
<context>
    <name>OutputOSS4Factory</name>
    <message>
        <location filename="../outputoss4factory.cpp" line="36"/>
        <source>OSS4 Plugin</source>
        <translation>Estensione OSS4</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="55"/>
        <source>About OSS4 Output Plugin</source>
        <translation>Informazioni sull&apos;estensione d&apos;uscita OSS4</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="56"/>
        <source>Qmmp OSS4 Output Plugin</source>
        <translation>Estensione d&apos;uscita OSS4 per Qmmp</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Autori: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
