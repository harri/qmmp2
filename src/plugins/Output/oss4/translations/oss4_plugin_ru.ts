<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>Oss4SettingsDialog</name>
    <message>
        <location filename="../oss4settingsdialog.ui" line="14"/>
        <source>OSS4 Plugin Settings</source>
        <translation>Настройки модуля OSS4</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.ui" line="61"/>
        <source>Device:</source>
        <translation>Устройство:</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.cpp" line="69"/>
        <source>Default (%1)</source>
        <translation>По умолчанию (%1)</translation>
    </message>
</context>
<context>
    <name>OutputOSS4Factory</name>
    <message>
        <location filename="../outputoss4factory.cpp" line="36"/>
        <source>OSS4 Plugin</source>
        <translation>Модуль OSS4</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="55"/>
        <source>About OSS4 Output Plugin</source>
        <translation>Модуль поддержки OSS4 для Qmmp</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="56"/>
        <source>Qmmp OSS4 Output Plugin</source>
        <translation>Модуль OSS4 для Qmmp</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Разработчик: Илья Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
