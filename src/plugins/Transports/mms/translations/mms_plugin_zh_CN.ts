<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MMSInputFactory</name>
    <message>
        <location filename="../mmsinputfactory.cpp" line="32"/>
        <source>MMS Plugin</source>
        <translation>MMS 插件</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="51"/>
        <source>About MMS Transport Plugin</source>
        <translation>关于 MMS T传输插件</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="52"/>
        <source>Qmmp MMS Transport Plugin</source>
        <translation>Qmmp MMS 传输插件</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MmsSettingsDialog</name>
    <message>
        <location filename="../mmssettingsdialog.ui" line="14"/>
        <source>MMS Plugin Settings</source>
        <translation>MMS 插件设置</translation>
    </message>
    <message>
        <location filename="../mmssettingsdialog.ui" line="29"/>
        <source>Buffer size:</source>
        <translation>缓存大小：</translation>
    </message>
    <message>
        <location filename="../mmssettingsdialog.ui" line="64"/>
        <source>KB</source>
        <translation>千字节</translation>
    </message>
</context>
</TS>
