<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>MMSInputFactory</name>
    <message>
        <location filename="../mmsinputfactory.cpp" line="32"/>
        <source>MMS Plugin</source>
        <translation>MMS Plugin</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="51"/>
        <source>About MMS Transport Plugin</source>
        <translation>Tietoja: MMS Transport Plugin</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="52"/>
        <source>Qmmp MMS Transport Plugin</source>
        <translation>Qmmp MMS Transport Plugin</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Kirjoittanut: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>MmsSettingsDialog</name>
    <message>
        <location filename="../mmssettingsdialog.ui" line="14"/>
        <source>MMS Plugin Settings</source>
        <translation>Asetukset MMS Plugin</translation>
    </message>
    <message>
        <location filename="../mmssettingsdialog.ui" line="29"/>
        <source>Buffer size:</source>
        <translation>Puskurin koko:</translation>
    </message>
    <message>
        <location filename="../mmssettingsdialog.ui" line="64"/>
        <source>KB</source>
        <translation>kt</translation>
    </message>
</context>
</TS>
