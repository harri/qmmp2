<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt">
<context>
    <name>MMSInputFactory</name>
    <message>
        <location filename="../mmsinputfactory.cpp" line="32"/>
        <source>MMS Plugin</source>
        <translation>MMS įskiepis</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="51"/>
        <source>About MMS Transport Plugin</source>
        <translation>Apie MMS transporto įskiepį</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="52"/>
        <source>Qmmp MMS Transport Plugin</source>
        <translation>Qmmp MMS transporto įskiepis</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Sukūrė: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>MmsSettingsDialog</name>
    <message>
        <location filename="../mmssettingsdialog.ui" line="14"/>
        <source>MMS Plugin Settings</source>
        <translation type="unfinished">MMS įskiepio nustatymai</translation>
    </message>
    <message>
        <location filename="../mmssettingsdialog.ui" line="29"/>
        <source>Buffer size:</source>
        <translation type="unfinished">Buferio dydis:</translation>
    </message>
    <message>
        <location filename="../mmssettingsdialog.ui" line="64"/>
        <source>KB</source>
        <translation type="unfinished">КB</translation>
    </message>
</context>
</TS>
