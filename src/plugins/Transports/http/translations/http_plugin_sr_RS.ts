<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_RS">
<context>
    <name>HTTPInputFactory</name>
    <message>
        <location filename="../httpinputfactory.cpp" line="33"/>
        <source>HTTP Plugin</source>
        <translation>ХТТП прикључак</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="52"/>
        <source>About HTTP Transport Plugin</source>
        <translation>О прикључку ХТТП преноса</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="53"/>
        <source>Qmmp HTTP Transport Plugin</source>
        <translation>Кумп прикључак за ХТТП пренос</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="54"/>
        <source>Compiled against libcurl-%1</source>
        <translation>Компилован на libcurl-%1</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>HttpSettingsDialog</name>
    <message>
        <location filename="../httpsettingsdialog.ui" line="14"/>
        <source>HTTP Plugin Settings</source>
        <translation type="unfinished">Поставке ХТТП прикључка</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="115"/>
        <source>Metadata encoding</source>
        <translation type="unfinished">Кодирање метаподатака</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="121"/>
        <source>Automatic charset detection</source>
        <translation type="unfinished">Аутоматско откривање кодирања</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="131"/>
        <source>Language:</source>
        <translation type="unfinished">Језик:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="151"/>
        <source>Default encoding:</source>
        <translation type="unfinished">Подразумевано кодирање:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="76"/>
        <source>User Agent:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="34"/>
        <source>Default buffer size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="50"/>
        <source>This value is used if information about bitrate is &lt;b&gt;not&lt;/b&gt; available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="53"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="69"/>
        <source>Change User Agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="90"/>
        <source>Buffer duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="97"/>
        <source>This value is used if information about bitrate is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="100"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
