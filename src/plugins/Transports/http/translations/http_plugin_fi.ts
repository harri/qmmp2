<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>HTTPInputFactory</name>
    <message>
        <location filename="../httpinputfactory.cpp" line="33"/>
        <source>HTTP Plugin</source>
        <translation>HTTP Plugin</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="52"/>
        <source>About HTTP Transport Plugin</source>
        <translation>Tietoja: HTTP Transport Plugin</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="53"/>
        <source>Qmmp HTTP Transport Plugin</source>
        <translation>Qmmp HTTP Transport Plugin</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="54"/>
        <source>Compiled against libcurl-%1</source>
        <translation>Koostettu libcurl-%1</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Kirjoittanut: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>HttpSettingsDialog</name>
    <message>
        <location filename="../httpsettingsdialog.ui" line="14"/>
        <source>HTTP Plugin Settings</source>
        <translation>Asetukset HTTP Plugin</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="115"/>
        <source>Metadata encoding</source>
        <translation>Metatietojen koodaus</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="121"/>
        <source>Automatic charset detection</source>
        <translation>Automaattinen merkistön havaitseminen</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="131"/>
        <source>Language:</source>
        <translation>Kieli:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="151"/>
        <source>Default encoding:</source>
        <translation>Oletuksena koodaus:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="76"/>
        <source>User Agent:</source>
        <translation>Käytön suoritin:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="34"/>
        <source>Default buffer size:</source>
        <translation>Puskurin oletuskoko:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="50"/>
        <source>This value is used if information about bitrate is &lt;b&gt;not&lt;/b&gt; available.</source>
        <translation>Tätä arvoa käytetään, jos bittinopeudesta &lt;b&gt;ei ole&lt;/b&gt; tietoa saatavilla.</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="53"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="69"/>
        <source>Change User Agent</source>
        <translation>Vaihda käytön suoritin:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="90"/>
        <source>Buffer duration:</source>
        <translation>Puskurin kesto:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="97"/>
        <source>This value is used if information about bitrate is available.</source>
        <translation>Tätä arvoa käytetään, jos tietoa bittinopeudesta on saatavilla.</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="100"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
</context>
</TS>
