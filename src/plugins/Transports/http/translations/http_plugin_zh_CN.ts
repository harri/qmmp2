<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>HTTPInputFactory</name>
    <message>
        <location filename="../httpinputfactory.cpp" line="33"/>
        <source>HTTP Plugin</source>
        <translation>HTTP 插件</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="52"/>
        <source>About HTTP Transport Plugin</source>
        <translation>关于 HTTP 传输插件</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="53"/>
        <source>Qmmp HTTP Transport Plugin</source>
        <translation>Qmmp HTTP 传输插件</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="54"/>
        <source>Compiled against libcurl-%1</source>
        <translation>编译依赖 libcurl-%1</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HttpSettingsDialog</name>
    <message>
        <location filename="../httpsettingsdialog.ui" line="14"/>
        <source>HTTP Plugin Settings</source>
        <translation>HTTP 插件设置</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="115"/>
        <source>Metadata encoding</source>
        <translation>元数据编码</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="121"/>
        <source>Automatic charset detection</source>
        <translation>自动检测字符集</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="131"/>
        <source>Language:</source>
        <translation>语言：</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="151"/>
        <source>Default encoding:</source>
        <translation>默认编码：</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="76"/>
        <source>User Agent:</source>
        <translation>用户代理：</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="34"/>
        <source>Default buffer size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="50"/>
        <source>This value is used if information about bitrate is &lt;b&gt;not&lt;/b&gt; available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="53"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="69"/>
        <source>Change User Agent</source>
        <translation>更改用户代理</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="90"/>
        <source>Buffer duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="97"/>
        <source>This value is used if information about bitrate is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="100"/>
        <source>ms</source>
        <translation>毫秒</translation>
    </message>
</context>
</TS>
