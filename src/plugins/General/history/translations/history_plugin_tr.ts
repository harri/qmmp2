<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>DateInputDialog</name>
    <message>
        <location filename="../dateinputdialog.ui" line="14"/>
        <source>Select Date</source>
        <translation>Tarih Seçin</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../history.cpp" line="57"/>
        <source>History</source>
        <translation>Geçmiş</translation>
    </message>
    <message>
        <location filename="../history.cpp" line="58"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
</context>
<context>
    <name>HistoryFactory</name>
    <message>
        <location filename="../historyfactory.cpp" line="31"/>
        <source>Listening History Plugin</source>
        <translation>Dinleme Geçmişi Eklentisi</translation>
    </message>
    <message>
        <location filename="../historyfactory.cpp" line="51"/>
        <source>About Listening History Plugin</source>
        <translation>Dinleme Geçmişi Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../historyfactory.cpp" line="52"/>
        <source>Qmmp Listening History Plugin</source>
        <translation>Qmmp Dinleme Geçmişi Eklentisi</translation>
    </message>
    <message>
        <location filename="../historyfactory.cpp" line="53"/>
        <source>This plugin collects information about listened tracks</source>
        <translation>Bu eklenti, dinlenen parçalar hakkında bilgi toplar</translation>
    </message>
    <message>
        <location filename="../historyfactory.cpp" line="54"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>HistorySettingsDialog</name>
    <message>
        <location filename="../historysettingsdialog.ui" line="14"/>
        <source>Listening History Plugin Settings</source>
        <translation>Dinleme Geçmişi Eklentisi Ayarları</translation>
    </message>
    <message>
        <location filename="../historysettingsdialog.ui" line="31"/>
        <source>Title format:</source>
        <translation>Başlık formatı:</translation>
    </message>
</context>
<context>
    <name>HistoryWindow</name>
    <message>
        <location filename="../historywindow.ui" line="14"/>
        <location filename="../historywindow.ui" line="110"/>
        <source>History</source>
        <translation>Geçmiş</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="29"/>
        <source>Time Range</source>
        <translation>Zaman Aralığı</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="35"/>
        <source>From:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="52"/>
        <source>To:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="69"/>
        <source>Last week</source>
        <translation>Son hafta</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="76"/>
        <source>Last month</source>
        <translation>Son ay</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="83"/>
        <source>Execute</source>
        <translation>Yürüt</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="129"/>
        <source>Time</source>
        <translation>Zaman</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="134"/>
        <location filename="../historywindow.ui" line="183"/>
        <source>Song</source>
        <translation>Şarkı</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="143"/>
        <source>Distribution</source>
        <translation>Dağıtım</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="156"/>
        <source>Day</source>
        <translation>Gün</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="161"/>
        <location filename="../historywindow.ui" line="188"/>
        <location filename="../historywindow.ui" line="215"/>
        <location filename="../historywindow.ui" line="242"/>
        <source>Play counts</source>
        <translation>Oynatma sayıları</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="170"/>
        <source>Top Songs</source>
        <translation>Zirvedeki Şarkılar</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="197"/>
        <source>Top Artists</source>
        <translation>Zirvedeki Sanatçılar</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="210"/>
        <source>Artist</source>
        <translation>Sanatçı</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="224"/>
        <source>Top Genres</source>
        <translation>Zirvedeki Tarzlar</translation>
    </message>
    <message>
        <location filename="../historywindow.ui" line="237"/>
        <source>Genre</source>
        <translation>Tarz</translation>
    </message>
    <message>
        <location filename="../historywindow.cpp" line="130"/>
        <source>dd MMMM yyyy</source>
        <translation>gg AAAA yyyy</translation>
    </message>
    <message>
        <location filename="../historywindow.cpp" line="131"/>
        <source>hh:mm:ss</source>
        <translation>ss:dd:snsn</translation>
    </message>
    <message>
        <location filename="../historywindow.cpp" line="198"/>
        <source>MM-yyyy</source>
        <translation>AA-yyyy</translation>
    </message>
    <message>
        <location filename="../historywindow.cpp" line="199"/>
        <source>dd MMMM</source>
        <translation>gg AAAA</translation>
    </message>
    <message>
        <location filename="../historywindow.cpp" line="499"/>
        <source>Add to Playlist</source>
        <translation>Çalma Listesine Ekle</translation>
    </message>
    <message>
        <location filename="../historywindow.cpp" line="500"/>
        <source>&amp;View Track Details</source>
        <translation>&amp;Parça Detaylarını Göster</translation>
    </message>
    <message>
        <location filename="../historywindow.cpp" line="502"/>
        <source>Remove from History</source>
        <translation>Tarihçeden Kaldır</translation>
    </message>
</context>
</TS>
