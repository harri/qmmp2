<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>Lyrics</name>
    <message>
        <location filename="../lyrics.cpp" line="34"/>
        <source>View Lyrics</source>
        <translation>Şarkı Sözlerini Göster</translation>
    </message>
    <message>
        <location filename="../lyrics.cpp" line="35"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
</context>
<context>
    <name>LyricsFactory</name>
    <message>
        <location filename="../lyricsfactory.cpp" line="31"/>
        <source>Lyrics Plugin</source>
        <translation>Şarkı Sözü Eklentisi</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="36"/>
        <source>Lyrics</source>
        <translation>Şarkı Sözleri</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="36"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="68"/>
        <source>About Lyrics Plugin</source>
        <translation>Şarkı Sözü Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="69"/>
        <source>Qmmp Lyrics Plugin</source>
        <translation>Qmmp Şarkı Sözü Eklentisi</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="70"/>
        <source>This plugin retrieves lyrics from LyricWiki</source>
        <translation>Bu eklenti şarkı sözlerini LyricWiki adresinden alır</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="71"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="72"/>
        <source>Based on Ultimate Lyrics script by Vladimir Brkic &lt;vladimir_brkic@yahoo.com&gt;</source>
        <translation>Vladimir Brkic&apos;in Ultimate Lyrics tabanlı betiği &lt;vladimir_brkic@yahoo.com&gt;</translation>
    </message>
</context>
<context>
    <name>LyricsSettingsDialog</name>
    <message>
        <location filename="../lyricssettingsdialog.ui" line="14"/>
        <source>Lyrics Plugin Settings</source>
        <translation>Lyrics Eklentisi Ayarları</translation>
    </message>
    <message>
        <location filename="../lyricssettingsdialog.ui" line="29"/>
        <source>Lyrics providers:</source>
        <translation>Lyrics sağlayıcıları:</translation>
    </message>
</context>
<context>
    <name>LyricsWidget</name>
    <message>
        <location filename="../lyricswidget.ui" line="14"/>
        <source>Lyrics Plugin</source>
        <translation>Şarkı Sözü Eklentisi</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="31"/>
        <source>Provider:</source>
        <translation>Sağlayıcı:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="115"/>
        <source>Title:</source>
        <translation>Başlık:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="132"/>
        <source>Album:</source>
        <translation>Albüm:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="142"/>
        <source>Artist:</source>
        <translation>Artist:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="159"/>
        <source>Track:</source>
        <translation>Parça:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="171"/>
        <source>Year:</source>
        <translation>Yıl:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="148"/>
        <location filename="../lyricswidget.cpp" line="234"/>
        <source>&lt;h2&gt;%1 - %2&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;%1 - %2&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="159"/>
        <source>Not found</source>
        <translation>Bulunamadı</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="169"/>
        <source>Error: %1 - %2</source>
        <translation>Hata: %1 - %2</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="182"/>
        <source>Receiving</source>
        <translation>Alınıyor</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="236"/>
        <source>Tag</source>
        <translation>Etiket</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="257"/>
        <source>Cache</source>
        <translation>Önbellek</translation>
    </message>
</context>
<context>
    <name>UltimateLyricsParser</name>
    <message>
        <location filename="../ultimatelyricsparser.cpp" line="116"/>
        <source>%1 (line: %2)</source>
        <translation>%1 (satır: %2)</translation>
    </message>
</context>
</TS>
