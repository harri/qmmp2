<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>KdeNotify</name>
    <message>
        <location filename="../kdenotify.cpp" line="120"/>
        <source>Qmmp now playing:</source>
        <translation>Qmmp şimdi çalıyor:</translation>
    </message>
</context>
<context>
    <name>KdeNotifyFactory</name>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="29"/>
        <source>KDE notification plugin</source>
        <translation>KDE bildirim eklentisi</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="49"/>
        <source>About KDE Notification Plugin</source>
        <translation>KDE Bildirim Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="50"/>
        <source>KDE notification plugin for Qmmp</source>
        <translation>Qmmp için KDE Bildirim Eklentisi</translation>
    </message>
</context>
<context>
    <name>KdeNotifySettingsDialog</name>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="14"/>
        <source>KDE Notification Plugin Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="39"/>
        <source>Options</source>
        <translation>Seçenekler</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="45"/>
        <source>Notification delay:</source>
        <translation>Bildirim gecikmesi:</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="52"/>
        <source>Update visible notification instead create new</source>
        <translation>Yeni oluşturmak yerine görünür bildirimi güncelleyin</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="72"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="85"/>
        <source>Volume change notification</source>
        <translation>Ses değişimi bildirimi</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="95"/>
        <source>Appearance</source>
        <translation>Dış görünüş</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="101"/>
        <source>Show covers</source>
        <translation>Kapakları göster</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="110"/>
        <source>Edit template</source>
        <translation>Şablonu düzenle</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.cpp" line="76"/>
        <source>Notification Template</source>
        <translation>Bildirim Şablonu</translation>
    </message>
</context>
</TS>
