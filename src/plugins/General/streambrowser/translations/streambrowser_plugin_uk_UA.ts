<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>EditStreamDialog</name>
    <message>
        <location filename="../editstreamdialog.ui" line="14"/>
        <source>Edit Stream</source>
        <translation>Редагувати потокове радіо</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="34"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="44"/>
        <source>Name:</source>
        <translation>Ім&apos;я:</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="54"/>
        <source>Genre:</source>
        <translation>Жанр:</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="64"/>
        <source>Bitrate:</source>
        <translation>Швидкість потоку:</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="74"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
</context>
<context>
    <name>StreamBrowser</name>
    <message>
        <location filename="../streambrowser.cpp" line="33"/>
        <source>Add Stream</source>
        <translation>Додати потік</translation>
    </message>
    <message>
        <location filename="../streambrowser.cpp" line="34"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
</context>
<context>
    <name>StreamBrowserFactory</name>
    <message>
        <location filename="../streambrowserfactory.cpp" line="29"/>
        <source>Stream Browser Plugin</source>
        <translation>Втулок огляду потоків</translation>
    </message>
    <message>
        <location filename="../streambrowserfactory.cpp" line="50"/>
        <source>About Stream Browser Plugin</source>
        <translation>Про втулок огляду потоків</translation>
    </message>
    <message>
        <location filename="../streambrowserfactory.cpp" line="51"/>
        <source>Qmmp Stream Browser Plugin</source>
        <translation>Втулок огляду потоків для Qmmp</translation>
    </message>
    <message>
        <location filename="../streambrowserfactory.cpp" line="52"/>
        <source>This plugin allows one to add stream from IceCast stream directory</source>
        <translation>Цей втулок дозволяє додавати потоки з каталогу IceCast</translation>
    </message>
    <message>
        <location filename="../streambrowserfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>StreamWindow</name>
    <message>
        <location filename="../streamwindow.ui" line="14"/>
        <source>Stream Browser</source>
        <translation>Огляд потоків</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="31"/>
        <source>Filter:</source>
        <translation>Фільтр:</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="47"/>
        <source>Favorites</source>
        <translation>Обране</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="73"/>
        <source>IceCast</source>
        <translation>IceCast</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="102"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="109"/>
        <source>Update</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="55"/>
        <location filename="../streamwindow.cpp" line="69"/>
        <source>Name</source>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="55"/>
        <location filename="../streamwindow.cpp" line="69"/>
        <source>Genre</source>
        <translation>Жанр</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="55"/>
        <location filename="../streamwindow.cpp" line="69"/>
        <source>Bitrate</source>
        <translation>Швидкість потоку</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="55"/>
        <location filename="../streamwindow.cpp" line="69"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="125"/>
        <source>&amp;Add to favorites</source>
        <translation>&amp;Додати до вибраного</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="128"/>
        <source>&amp;Add to playlist</source>
        <translation>&amp;Додати до грайлиста</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="131"/>
        <source>&amp;Create</source>
        <translation>&amp;Створити</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="132"/>
        <source>&amp;Edit</source>
        <translation>&amp;Зміни</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="136"/>
        <location filename="../streamwindow.cpp" line="139"/>
        <source>&amp;Remove</source>
        <translation>&amp;Вилучити</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="152"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="155"/>
        <location filename="../streamwindow.cpp" line="156"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="175"/>
        <source>Receiving</source>
        <translation>Отримання</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="278"/>
        <source>Edit Stream</source>
        <translation>Редагувати потокове радіо</translation>
    </message>
</context>
</TS>
