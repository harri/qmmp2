<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>HotkeyDialog</name>
    <message>
        <location filename="../hotkeydialog.ui" line="14"/>
        <source>Modify Shortcut</source>
        <translation>התאם קיצור דרך</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="32"/>
        <source>Press the key combination you want to assign</source>
        <translation>הזן את צירוף המקש אשר ברצונך להקצות</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="52"/>
        <source>Clear</source>
        <translation>טהר</translation>
    </message>
</context>
<context>
    <name>HotkeyFactory</name>
    <message>
        <location filename="../hotkeyfactory.cpp" line="31"/>
        <source>Global Hotkey Plugin</source>
        <translation>תוסף מקש חם גלובלי</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="57"/>
        <source>About Global Hotkey Plugin</source>
        <translation>אודות תוסף מקש חם גלובלי</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="58"/>
        <source>Qmmp Global Hotkey Plugin</source>
        <translation>תוסף מקש חם גלובלי של Qmmp</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="59"/>
        <source>This plugin adds support for multimedia keys or global key combinations</source>
        <translation>תוסף זה מוסיף תמיכה למקשי מולטימדיה או צירופי מקש גלובלי</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="60"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HotkeySettingsDialog</name>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="14"/>
        <source>Global Hotkey Plugin Settings</source>
        <translation type="unfinished">הגדרות תוסף מקש חם גלובלי</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="42"/>
        <source>Action</source>
        <translation type="unfinished">פעולה</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="47"/>
        <source>Shortcut</source>
        <translation type="unfinished">קיצור דרך</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="55"/>
        <source>Reset</source>
        <translation type="unfinished">אפס</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="39"/>
        <source>Play</source>
        <translation type="unfinished">נגן</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="40"/>
        <source>Stop</source>
        <translation type="unfinished">הפסק</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="41"/>
        <source>Pause</source>
        <translation type="unfinished">השהה</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="42"/>
        <source>Play/Pause</source>
        <translation type="unfinished">נגן/השהה</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="43"/>
        <source>Next</source>
        <translation type="unfinished">הבא</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="44"/>
        <source>Previous</source>
        <translation type="unfinished">הקודם</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="45"/>
        <source>Show/Hide</source>
        <translation type="unfinished">הצגה/הסתרה</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="46"/>
        <source>Volume +</source>
        <translation type="unfinished">קול +</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="47"/>
        <source>Volume -</source>
        <translation type="unfinished">קול -</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="48"/>
        <source>Forward 5 seconds</source>
        <translation type="unfinished">5 שניות קדימה</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="49"/>
        <source>Rewind 5 seconds</source>
        <translation type="unfinished">5 שניות אחורה</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="50"/>
        <source>Jump to track</source>
        <translation type="unfinished">קפוץ אל רצועה</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="51"/>
        <source>Mute</source>
        <translation type="unfinished">השתק</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Warning</source>
        <translation type="unfinished">אזהרה</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Key sequence &apos;%1&apos; is already used</source>
        <translation type="unfinished">רצף מקש &apos;%1&apos; מצוי כבר בשימוש</translation>
    </message>
</context>
</TS>
