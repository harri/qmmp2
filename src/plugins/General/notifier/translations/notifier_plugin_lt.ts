<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt">
<context>
    <name>NotifierFactory</name>
    <message>
        <location filename="../notifierfactory.cpp" line="29"/>
        <source>Notifier Plugin</source>
        <translation>Notifier įskiepis</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="49"/>
        <source>About Notifier Plugin</source>
        <translation>Apie Notifier įskiepį</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="50"/>
        <source>Qmmp Notifier Plugin</source>
        <translation>Qmmp Notifier Įskiepis</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Sukūrė: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>NotifierSettingsDialog</name>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="14"/>
        <source>Notifier Plugin Settings</source>
        <translation type="unfinished">Notifier įskiepio nustatymai</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="35"/>
        <source>Desktop Notification</source>
        <translation type="unfinished">Pranešimas darbalaukyje</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="51"/>
        <source>Font:</source>
        <translation type="unfinished">Šriftas:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="71"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="78"/>
        <location filename="../notifiersettingsdialog.ui" line="279"/>
        <source>0</source>
        <translation type="unfinished">0</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="85"/>
        <source>Transparency:</source>
        <translation type="unfinished">Permatomumas:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="229"/>
        <source>Position</source>
        <translation type="unfinished">Padėtis</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="251"/>
        <source>Edit template</source>
        <translation type="unfinished">Taisyti šabloną</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="299"/>
        <source>Cover size:</source>
        <translation type="unfinished">Viršelio dydis:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="325"/>
        <source>Volume change notification</source>
        <translation type="unfinished">Pranešti pasikeitus garsumui</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="332"/>
        <source>Delay (ms):</source>
        <translation type="unfinished">Užlaikymas (ms):</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="361"/>
        <source>Playback resume notification</source>
        <translation type="unfinished">Pranešti pratęsus grojimą</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="368"/>
        <source>Song change notification</source>
        <translation type="unfinished">Pranešti pasikeitus dainai</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="375"/>
        <source>Disable notifications when another application is in the Full Screen Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="378"/>
        <source>Disable for full screen windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="388"/>
        <source>Psi Notification</source>
        <translation type="unfinished">Psi pranešimas</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="400"/>
        <source>Enable Psi notification</source>
        <translation type="unfinished">Įjungti Psi pranešimą</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.cpp" line="115"/>
        <source>Notification Template</source>
        <translation type="unfinished">Pranešimų šablonas</translation>
    </message>
</context>
<context>
    <name>PopupWidget</name>
    <message>
        <location filename="../popupwidget.cpp" line="115"/>
        <source>Volume:</source>
        <translation>Garsumas:</translation>
    </message>
</context>
</TS>
