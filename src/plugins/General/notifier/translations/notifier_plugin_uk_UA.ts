<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>NotifierFactory</name>
    <message>
        <location filename="../notifierfactory.cpp" line="29"/>
        <source>Notifier Plugin</source>
        <translation>Втулок сповіщень</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="49"/>
        <source>About Notifier Plugin</source>
        <translation>Про втулок сповіщень</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="50"/>
        <source>Qmmp Notifier Plugin</source>
        <translation>Втулок сповіщень для Qmmp</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>NotifierSettingsDialog</name>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="14"/>
        <source>Notifier Plugin Settings</source>
        <translation>Налаштування втулка сповіщень</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="35"/>
        <source>Desktop Notification</source>
        <translation>Сповіщення на стільниці</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="51"/>
        <source>Font:</source>
        <translation>Шрифт:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="71"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="78"/>
        <location filename="../notifiersettingsdialog.ui" line="279"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="85"/>
        <source>Transparency:</source>
        <translation>Прозорість:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="229"/>
        <source>Position</source>
        <translation>Позиція</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="251"/>
        <source>Edit template</source>
        <translation>Змінити шаблон</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="299"/>
        <source>Cover size:</source>
        <translation>Розмір обкладинки:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="325"/>
        <source>Volume change notification</source>
        <translation>Сповіщення зміни гучности</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="332"/>
        <source>Delay (ms):</source>
        <translation>Затримка (мс):</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="361"/>
        <source>Playback resume notification</source>
        <translation>Сповіщати про продовження відтворення</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="368"/>
        <source>Song change notification</source>
        <translation>Сповіщення зміни доріжки</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="375"/>
        <source>Disable notifications when another application is in the Full Screen Mode</source>
        <translation>Вимикає сповіщення, якщо виконується програма в повноекранному режимі</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="378"/>
        <source>Disable for full screen windows</source>
        <translation>Вимкнути для повноекранних вікон</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="388"/>
        <source>Psi Notification</source>
        <translation>Сповіщення Psi</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="400"/>
        <source>Enable Psi notification</source>
        <translation>Увімкнути сповіщення Psi</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.cpp" line="115"/>
        <source>Notification Template</source>
        <translation>Шаблон сповіщення</translation>
    </message>
</context>
<context>
    <name>PopupWidget</name>
    <message>
        <location filename="../popupwidget.cpp" line="115"/>
        <source>Volume:</source>
        <translation>Гучність:</translation>
    </message>
</context>
</TS>
