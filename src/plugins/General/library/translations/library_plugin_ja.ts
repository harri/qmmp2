<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>Library</name>
    <message>
        <location filename="../library.cpp" line="63"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../library.cpp" line="64"/>
        <source>Alt+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../library.cpp" line="70"/>
        <source>Update library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../library.cpp" line="192"/>
        <location filename="../library.cpp" line="193"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryFactory</name>
    <message>
        <location filename="../libraryfactory.cpp" line="33"/>
        <source>Media Library Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="38"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="38"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="84"/>
        <source>About Media Library Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="85"/>
        <source>Qmmp Media Library Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="86"/>
        <source>This plugin represents a database to store music files tags for a fast access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="87"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>LibraryModel</name>
    <message>
        <location filename="../librarymodel.cpp" line="206"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="354"/>
        <source>Error</source>
        <translation>エラーです</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="354"/>
        <source>Unable to connect to database</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="375"/>
        <source>%n day(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="376"/>
        <source>%n hour(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="377"/>
        <source>%n minute(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="378"/>
        <source>%n second(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="382"/>
        <source>%1 %2 %3 %4</source>
        <comment>days hours minutes seconds</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="384"/>
        <source>%1 %2 %3</source>
        <comment>hours minutes seconds</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="386"/>
        <source>%1 %2</source>
        <comment>minutes seconds</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="389"/>
        <source>Number of tracks: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="390"/>
        <source>Number of albums: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="391"/>
        <source>Number of artists: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="392"/>
        <source>Total duration: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="395"/>
        <source>Library Information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibrarySettingsDialog</name>
    <message>
        <location filename="../librarysettingsdialog.ui" line="14"/>
        <source>Media Library Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="39"/>
        <source>Recreate database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="46"/>
        <source>Show album year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="55"/>
        <source>Add</source>
        <translation>追加</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="66"/>
        <source>Remove</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="95"/>
        <source>List of directories for scanning:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.cpp" line="65"/>
        <source>Select Directories for Scanning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryWidget</name>
    <message>
        <location filename="../librarywidget.cpp" line="51"/>
        <source>&amp;Add to Playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="52"/>
        <source>Replace Playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="53"/>
        <source>&amp;View Track Details</source>
        <translation>トラックの詳細を表示(&amp;D)</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="55"/>
        <source>Quick Search</source>
        <translation>絞り込み検索</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="56"/>
        <source>&amp;Library Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="90"/>
        <source>Scanning directories...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarywidget.ui" line="14"/>
        <source>Media Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
