<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>Converter</name>
    <message>
        <location filename="../converter.cpp" line="125"/>
        <location filename="../converter.cpp" line="228"/>
        <source>Cancelled</source>
        <translation>Скасовано</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="140"/>
        <location filename="../converter.cpp" line="210"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="178"/>
        <source>Converting</source>
        <translation>Перетворення</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="239"/>
        <source>Encoding</source>
        <translation>Кодування</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="277"/>
        <source>Finished</source>
        <translation>Завершено</translation>
    </message>
</context>
<context>
    <name>ConverterDialog</name>
    <message>
        <location filename="../converterdialog.ui" line="14"/>
        <source>Audio Converter</source>
        <translation>Конвертер авдіо</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="44"/>
        <source>Progress</source>
        <translation>Поступ</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="49"/>
        <source>State</source>
        <translation>Стан</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="63"/>
        <source>Output directory:</source>
        <translation>Вихідна тека:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="80"/>
        <source>Output file name:</source>
        <translation>Ім&apos;я вихідного файлу:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="97"/>
        <source>Preset:</source>
        <translation>Пресет:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="114"/>
        <source>Overwrite existing files</source>
        <translation>Переписати наявні файли</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="124"/>
        <source>Convert</source>
        <translation>Перетворити</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="131"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="116"/>
        <source>Choose a directory</source>
        <translation>Оберіть теку</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="39"/>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="205"/>
        <source>Create a Copy</source>
        <translation>Створити копію</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="136"/>
        <location filename="../converterdialog.cpp" line="358"/>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="141"/>
        <source>Waiting</source>
        <translation>Очікування</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="203"/>
        <source>Create</source>
        <translation>Створити</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="204"/>
        <source>Edit</source>
        <translation>Редагувати</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="206"/>
        <source>Delete</source>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="358"/>
        <source>Unable to execute &quot;%1&quot;. Program not found.</source>
        <translation>Неможливо запустити &quot;%1&quot;. Програми не найдено.</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Process &quot;%1&quot; finished with error.</source>
        <translation>Процес &quot;%1&quot; завершено з помилкою.</translation>
    </message>
</context>
<context>
    <name>ConverterFactory</name>
    <message>
        <location filename="../converterfactory.cpp" line="28"/>
        <source>Converter Plugin</source>
        <translation>Втулок перетворювача</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="49"/>
        <source>About Converter Plugin</source>
        <translation>Про втулок перетворювача</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="50"/>
        <source>Qmmp Converter Plugin</source>
        <translation>Втулок перетворювача для Qmmp</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="51"/>
        <source>This plugin converts supported audio files to other file formats using external command-line encoders</source>
        <translation>Цей втулок покликано для перетворення підтримуваних авдіофайлів на инший формат з використанням зовнішніх консольних програм</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ConverterHelper</name>
    <message>
        <location filename="../converterhelper.cpp" line="33"/>
        <source>Convert</source>
        <translation>Перетворити</translation>
    </message>
    <message>
        <location filename="../converterhelper.cpp" line="34"/>
        <source>Meta+C</source>
        <translation>Meta+C</translation>
    </message>
</context>
<context>
    <name>ConverterPresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.ui" line="14"/>
        <source>Preset Editor</source>
        <translation>Редагувач пресету</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="29"/>
        <source>General</source>
        <translation>Загальне</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="35"/>
        <source>Name:</source>
        <translation>Ім&apos;я:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="45"/>
        <source>Extension:</source>
        <translation>Розширення:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="58"/>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="77"/>
        <source>Options</source>
        <translation>Параметри</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="83"/>
        <source>Write tags</source>
        <translation>Записати теґи</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="90"/>
        <source>Convert to 16 bit</source>
        <translation>Перетворити на 16 біт</translation>
    </message>
</context>
<context>
    <name>PresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.cpp" line="40"/>
        <source>%1 (Read Only)</source>
        <translation>%1 (Лише для читання)</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="73"/>
        <source>Output file</source>
        <translation>Вихідний файл</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="74"/>
        <source>Input file</source>
        <translation>Вхідний файл</translation>
    </message>
</context>
</TS>
