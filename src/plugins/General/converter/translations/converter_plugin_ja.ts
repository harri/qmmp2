<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>Converter</name>
    <message>
        <location filename="../converter.cpp" line="125"/>
        <location filename="../converter.cpp" line="228"/>
        <source>Cancelled</source>
        <translation>中止しました</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="140"/>
        <location filename="../converter.cpp" line="210"/>
        <source>Error</source>
        <translation>事故発生</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="178"/>
        <source>Converting</source>
        <translation>転換しています</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="239"/>
        <source>Encoding</source>
        <translation>符号化しています</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="277"/>
        <source>Finished</source>
        <translation>完了しました</translation>
    </message>
</context>
<context>
    <name>ConverterDialog</name>
    <message>
        <location filename="../converterdialog.ui" line="14"/>
        <source>Audio Converter</source>
        <translation>音響転換</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="44"/>
        <source>Progress</source>
        <translation>進捗</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="49"/>
        <source>State</source>
        <translation>状況</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="63"/>
        <source>Output directory:</source>
        <translation>出力先ディレクトリ:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="80"/>
        <source>Output file name:</source>
        <translation>出力ファイルの名前:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="97"/>
        <source>Preset:</source>
        <translation>プリセット:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="114"/>
        <source>Overwrite existing files</source>
        <translation>既存のファイルを上書き</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="124"/>
        <source>Convert</source>
        <translation>転換</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="131"/>
        <source>Stop</source>
        <translation>終止</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="116"/>
        <source>Choose a directory</source>
        <translation>ディレクトリを選択</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="39"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="205"/>
        <source>Create a Copy</source>
        <translation>複製を作成</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="136"/>
        <location filename="../converterdialog.cpp" line="358"/>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Error</source>
        <translation>事故発生</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="141"/>
        <source>Waiting</source>
        <translation>待機中です</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="203"/>
        <source>Create</source>
        <translation>作成</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="204"/>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="206"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="358"/>
        <source>Unable to execute &quot;%1&quot;. Program not found.</source>
        <translation>&quot;%1&quot; を実行できません。プログラムがみつかりません。</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Process &quot;%1&quot; finished with error.</source>
        <translation>プロセス &quot;%1&quot; がエラーで終了しました。</translation>
    </message>
</context>
<context>
    <name>ConverterFactory</name>
    <message>
        <location filename="../converterfactory.cpp" line="28"/>
        <source>Converter Plugin</source>
        <translation>ファイル形式転換プラグイン</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="49"/>
        <source>About Converter Plugin</source>
        <translation>転換プラグインについて</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="50"/>
        <source>Qmmp Converter Plugin</source>
        <translation>QMMP 転換プラグイン</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="51"/>
        <source>This plugin converts supported audio files to other file formats using external command-line encoders</source>
        <translation>このプラグインは外部のコマンドライン方式のエンコーダーを利用してサポート対象形式間で音声ファイルを転換し別の音声ファイルを生成します。</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ConverterHelper</name>
    <message>
        <location filename="../converterhelper.cpp" line="33"/>
        <source>Convert</source>
        <translation>転換</translation>
    </message>
    <message>
        <location filename="../converterhelper.cpp" line="34"/>
        <source>Meta+C</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConverterPresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.ui" line="14"/>
        <source>Preset Editor</source>
        <translation>プリセットエディター</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="29"/>
        <source>General</source>
        <translation>総合</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="35"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="45"/>
        <source>Extension:</source>
        <translation>拡張:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="58"/>
        <source>Command</source>
        <translation>コマンド</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="77"/>
        <source>Options</source>
        <translation>オプション</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="83"/>
        <source>Write tags</source>
        <translation>タグを書き込む</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="90"/>
        <source>Convert to 16 bit</source>
        <translation>16ビットに変換</translation>
    </message>
</context>
<context>
    <name>PresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.cpp" line="40"/>
        <source>%1 (Read Only)</source>
        <translation>%1 (読み込み専用)</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="73"/>
        <source>Output file</source>
        <translation>出力ファイル</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="74"/>
        <source>Input file</source>
        <translation>入力ファイル</translation>
    </message>
</context>
</TS>
