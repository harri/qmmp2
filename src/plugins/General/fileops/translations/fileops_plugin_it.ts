<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>FileOps</name>
    <message>
        <location filename="../fileops.cpp" line="107"/>
        <location filename="../fileops.cpp" line="150"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="108"/>
        <location filename="../fileops.cpp" line="151"/>
        <source>Destination directory doesn&apos;t exist</source>
        <translation>Cartella di destinazione</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="154"/>
        <source>Move Files</source>
        <translation>Sposta file</translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="155"/>
        <source>Are you sure you want to move %n file(s)?</source>
        <translation>
            <numerusform>Vuoi davvero spostare %n file?</numerusform>
            <numerusform>Vuoi davvero spostare %n file?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="177"/>
        <source>Copying</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="178"/>
        <location filename="../fileops.cpp" line="275"/>
        <source>Stop</source>
        <translation>Ferma</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="223"/>
        <source>Copying file %1/%2</source>
        <translation>Copia in corso del file %1/%2</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="274"/>
        <source>Moving</source>
        <translation>Spostamento</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="313"/>
        <source>Moving file %1/%2</source>
        <translation>Spostamento del file %1/%2</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="123"/>
        <source>Remove Files</source>
        <translation>Rimozione file</translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="124"/>
        <source>Are you sure you want to remove %n file(s) from disk?</source>
        <translation>
            <numerusform>Vuoi davvero rimuovere %n file dal disco?</numerusform>
            <numerusform>Vuoi davvero rimuovere %n file dal disco?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>FileOpsFactory</name>
    <message>
        <location filename="../fileopsfactory.cpp" line="29"/>
        <source>File Operations Plugin</source>
        <translation>Estensione per le operazioni su file</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="49"/>
        <source>About File Operations Plugin</source>
        <translation>Informazioni sull&apos;estensione per le operazioni su file</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="50"/>
        <source>Qmmp File Operations Plugin</source>
        <translation>Estensione per le operazioni su file di Qmmp</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Autori: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>FileOpsSettingsDialog</name>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="14"/>
        <source>File Operations Settings</source>
        <translation>Impostazioni per le operazioni su file</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="51"/>
        <source>Enabled</source>
        <translation>Abilitato</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="56"/>
        <source>Operation</source>
        <translation>Operazione</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="61"/>
        <source>Menu text</source>
        <translation>Menu testo</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="66"/>
        <source>Shortcut</source>
        <translation>Scorciatoia</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="82"/>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="99"/>
        <location filename="../fileopssettingsdialog.cpp" line="222"/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="127"/>
        <source>Destination:</source>
        <translation>Destinazione:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="137"/>
        <location filename="../fileopssettingsdialog.ui" line="154"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="144"/>
        <location filename="../fileopssettingsdialog.cpp" line="155"/>
        <source>File name pattern:</source>
        <translation>Schema nome del file:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="127"/>
        <source>New action</source>
        <translation>Nuova azione</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="184"/>
        <source>Command:</source>
        <translation>Comando:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="219"/>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="220"/>
        <source>Rename</source>
        <translation>Rinomina</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="221"/>
        <source>Move</source>
        <translation>Sposta</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="223"/>
        <source>Execute</source>
        <translation>Esegui</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="235"/>
        <source>Choose a directory</source>
        <translation>Scegli una cartella</translation>
    </message>
</context>
</TS>
