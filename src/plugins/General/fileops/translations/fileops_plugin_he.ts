<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>FileOps</name>
    <message>
        <location filename="../fileops.cpp" line="107"/>
        <location filename="../fileops.cpp" line="150"/>
        <source>Error</source>
        <translation>שגיאה</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="108"/>
        <location filename="../fileops.cpp" line="151"/>
        <source>Destination directory doesn&apos;t exist</source>
        <translation>מדור יעד לא קיים</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="154"/>
        <source>Move Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="155"/>
        <source>Are you sure you want to move %n file(s)?</source>
        <translation>
            <numerusform>האם אתה בטוח כי ברצונך להעביר קובץ %n?</numerusform>
            <numerusform>האם אתה בטוח כי ברצונך להעביר %n קבצים?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="177"/>
        <source>Copying</source>
        <translation>העתקה</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="178"/>
        <location filename="../fileops.cpp" line="275"/>
        <source>Stop</source>
        <translation>הפסק</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="223"/>
        <source>Copying file %1/%2</source>
        <translation>העתקת קובץ %1/%2</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="274"/>
        <source>Moving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="313"/>
        <source>Moving file %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="123"/>
        <source>Remove Files</source>
        <translation>הסר קבצים</translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="124"/>
        <source>Are you sure you want to remove %n file(s) from disk?</source>
        <translation>
            <numerusform>האם אתה בטוח כי ברצונך להסיר קובץ %n מתוך הכונן?</numerusform>
            <numerusform>האם אתה בטוח כי ברצונך להסיר %n קבצים מתוך הכונן?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>FileOpsFactory</name>
    <message>
        <location filename="../fileopsfactory.cpp" line="29"/>
        <source>File Operations Plugin</source>
        <translation>תוסף מבצעי קובץ</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="49"/>
        <source>About File Operations Plugin</source>
        <translation>אודות תוסף מבצעי קובץ</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="50"/>
        <source>Qmmp File Operations Plugin</source>
        <translation>תוסף מבצעי קובץ של Qmmp</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileOpsSettingsDialog</name>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="14"/>
        <source>File Operations Settings</source>
        <translation type="unfinished">הגדרות מבצעי קובץ</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="51"/>
        <source>Enabled</source>
        <translation type="unfinished">מאופשר</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="56"/>
        <source>Operation</source>
        <translation type="unfinished">מבצע</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="61"/>
        <source>Menu text</source>
        <translation type="unfinished">תמליל תפריט</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="66"/>
        <source>Shortcut</source>
        <translation type="unfinished">קיצור דרך</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="82"/>
        <source>Add</source>
        <translation type="unfinished">הוסף</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="99"/>
        <location filename="../fileopssettingsdialog.cpp" line="222"/>
        <source>Remove</source>
        <translation type="unfinished">הסר</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="127"/>
        <source>Destination:</source>
        <translation type="unfinished">יעד:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="137"/>
        <location filename="../fileopssettingsdialog.ui" line="154"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="144"/>
        <location filename="../fileopssettingsdialog.cpp" line="155"/>
        <source>File name pattern:</source>
        <translation type="unfinished">תבנית שם קובץ:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="127"/>
        <source>New action</source>
        <translation type="unfinished">פעולה חדשה</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="184"/>
        <source>Command:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="219"/>
        <source>Copy</source>
        <translation type="unfinished">העתק</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="220"/>
        <source>Rename</source>
        <translation type="unfinished">שנה שם</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="221"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="223"/>
        <source>Execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="235"/>
        <source>Choose a directory</source>
        <translation type="unfinished">בחר מדור</translation>
    </message>
</context>
</TS>
