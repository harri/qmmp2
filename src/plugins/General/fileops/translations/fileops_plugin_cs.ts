<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>FileOps</name>
    <message>
        <location filename="../fileops.cpp" line="107"/>
        <location filename="../fileops.cpp" line="150"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="108"/>
        <location filename="../fileops.cpp" line="151"/>
        <source>Destination directory doesn&apos;t exist</source>
        <translation>Cílový adresář neexistuje</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="154"/>
        <source>Move Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="155"/>
        <source>Are you sure you want to move %n file(s)?</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="177"/>
        <source>Copying</source>
        <translation>Kopíruje se</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="178"/>
        <location filename="../fileops.cpp" line="275"/>
        <source>Stop</source>
        <translation>Přerušit</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="223"/>
        <source>Copying file %1/%2</source>
        <translation>Kopíruje se soubor %1/%2</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="274"/>
        <source>Moving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="313"/>
        <source>Moving file %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="123"/>
        <source>Remove Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="124"/>
        <source>Are you sure you want to remove %n file(s) from disk?</source>
        <translation>
            <numerusform>Jste si jisti, že chcete z disku odstranit %n soubor?</numerusform>
            <numerusform>Jste si jisti, že chcete z disku odstranit %n soubory?</numerusform>
            <numerusform>Jste si jisti, že chcete z disku odstranit %n souborů?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>FileOpsFactory</name>
    <message>
        <location filename="../fileopsfactory.cpp" line="29"/>
        <source>File Operations Plugin</source>
        <translation>Modul pro operace se soubory</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="49"/>
        <source>About File Operations Plugin</source>
        <translation>O modulu pro operace se soubory</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="50"/>
        <source>Qmmp File Operations Plugin</source>
        <translation>Modul Qmmp pro operace se soubory</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileOpsSettingsDialog</name>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="14"/>
        <source>File Operations Settings</source>
        <translation type="unfinished">Nastavení operací se soubory</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="51"/>
        <source>Enabled</source>
        <translation type="unfinished">Povoleno</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="56"/>
        <source>Operation</source>
        <translation type="unfinished">Operace</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="61"/>
        <source>Menu text</source>
        <translation type="unfinished">Text v nabídce</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="66"/>
        <source>Shortcut</source>
        <translation type="unfinished">Zkratka</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="82"/>
        <source>Add</source>
        <translation type="unfinished">Přidat</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="99"/>
        <location filename="../fileopssettingsdialog.cpp" line="222"/>
        <source>Remove</source>
        <translation type="unfinished">Odstranit</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="127"/>
        <source>Destination:</source>
        <translation type="unfinished">Cíl:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="137"/>
        <location filename="../fileopssettingsdialog.ui" line="154"/>
        <source>...</source>
        <translation type="unfinished">…</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="144"/>
        <location filename="../fileopssettingsdialog.cpp" line="155"/>
        <source>File name pattern:</source>
        <translation type="unfinished">Vzor jména:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="127"/>
        <source>New action</source>
        <translation type="unfinished">Nová akce</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="184"/>
        <source>Command:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="219"/>
        <source>Copy</source>
        <translation type="unfinished">Kopírovat</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="220"/>
        <source>Rename</source>
        <translation type="unfinished">Přejmenovat</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="221"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="223"/>
        <source>Execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="235"/>
        <source>Choose a directory</source>
        <translation type="unfinished">Vyberte adresář</translation>
    </message>
</context>
</TS>
