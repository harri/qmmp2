<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>TaskbarFactory</name>
    <message>
        <location filename="../taskbarfactory.cpp" line="28"/>
        <source>Taskbar Plugin</source>
        <translation>Görev Çubuğu Eklentisi</translation>
    </message>
    <message>
        <location filename="../taskbarfactory.cpp" line="49"/>
        <source>About Taskbar Plugin</source>
        <translation>Görev Çubuğu Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../taskbarfactory.cpp" line="50"/>
        <source>Qmmp Taskbar Plugin</source>
        <translation>Qmmp Görev Çubuğu Eklentisi</translation>
    </message>
    <message>
        <location filename="../taskbarfactory.cpp" line="51"/>
        <source>This plugin adds support for progress indicator in the Windows taskbar</source>
        <translation>Bu eklenti Windows Görev çubuğunda ilerleme göstergesi desteği ekler</translation>
    </message>
    <message>
        <location filename="../taskbarfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../taskbarfactory.cpp" line="53"/>
        <source>Based on QtWinExtras module of the Qt Toolkit</source>
        <translation>Qt Toolkit&apos;in QtWinExtras modülüne dayalıdır</translation>
    </message>
    <message>
        <location filename="../taskbarfactory.cpp" line="54"/>
        <source>QtWinExtras developers:</source>
        <translation>QtWinExtras geliştircileri:</translation>
    </message>
    <message>
        <location filename="../taskbarfactory.cpp" line="55"/>
        <source>Ivan Vizir &lt;define-true-false@yandex.com&gt;</source>
        <translation>Ivan Vizir &lt;define-true-false@yandex.com&gt;</translation>
    </message>
    <message>
        <location filename="../taskbarfactory.cpp" line="56"/>
        <source>The Qt Company Ltd.</source>
        <translation>Qt Company Ltd. Şti.</translation>
    </message>
</context>
</TS>
