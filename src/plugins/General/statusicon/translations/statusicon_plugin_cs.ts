<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>StatusIcon</name>
    <message>
        <location filename="../statusicon.cpp" line="70"/>
        <source>Play</source>
        <translation>Přehrát</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="71"/>
        <source>Pause</source>
        <translation>Pauza</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="72"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="74"/>
        <source>Next</source>
        <translation>Další</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="75"/>
        <source>Previous</source>
        <translation>Předchozí</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="77"/>
        <source>Exit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="119"/>
        <source>Stopped</source>
        <translation>Zastaveno</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="144"/>
        <source>Now Playing</source>
        <translation>Nyní hraje</translation>
    </message>
</context>
<context>
    <name>StatusIconFactory</name>
    <message>
        <location filename="../statusiconfactory.cpp" line="29"/>
        <source>Status Icon Plugin</source>
        <translation>Modul stavové ikony</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="49"/>
        <source>About Status Icon Plugin</source>
        <translation>O modulu stavové ikony</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="50"/>
        <source>Qmmp Status Icon Plugin</source>
        <translation>Modul stavové ikony Qmmp</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="51"/>
        <source>Written by:</source>
        <translation>Autoři:</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="52"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="53"/>
        <source>Artur Guzik &lt;a.guzik88@gmail.com&gt;</source>
        <translation>Artur Guzik &lt;a.guzik88@gmail.com&gt;</translation>
    </message>
</context>
<context>
    <name>StatusIconSettingsDialog</name>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="14"/>
        <source>Status Icon Plugin Settings</source>
        <translation type="unfinished">Nastavení modulu stavové ikony</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="29"/>
        <source>Balloon message</source>
        <translation type="unfinished">Vyskakovací zpráva</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="38"/>
        <location filename="../statusiconsettingsdialog.ui" line="105"/>
        <source>Delay, ms:</source>
        <translation type="unfinished">Prodleva, ms:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="79"/>
        <source>Tooltip</source>
        <translation type="unfinished">Popisek</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="91"/>
        <source>Try to split file name when no tag</source>
        <translation type="unfinished">Pokusit se rozdělit název souboru, není-li tag</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="98"/>
        <source>Show progress bar</source>
        <translation type="unfinished">Zobrazit ukazatel průběhu</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="131"/>
        <source>Transparency:</source>
        <translation type="unfinished">Průhlednost:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="165"/>
        <source>0</source>
        <translation type="unfinished">0</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="174"/>
        <source>Cover size:</source>
        <translation type="unfinished">Velikost obalu:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="214"/>
        <source>32</source>
        <translation type="unfinished">32</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="225"/>
        <source>Edit template</source>
        <translation type="unfinished">Upravit šablonu</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="250"/>
        <source>Use standard icons</source>
        <translation type="unfinished">Použít standardní ikony</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.cpp" line="85"/>
        <source>Tooltip Template</source>
        <translation type="unfinished">Šablona popisku</translation>
    </message>
</context>
</TS>
