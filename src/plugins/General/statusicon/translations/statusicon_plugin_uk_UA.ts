<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>StatusIcon</name>
    <message>
        <location filename="../statusicon.cpp" line="70"/>
        <source>Play</source>
        <translation>Грати</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="71"/>
        <source>Pause</source>
        <translation>Павза</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="72"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="74"/>
        <source>Next</source>
        <translation>Уперед</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="75"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="77"/>
        <source>Exit</source>
        <translation>Вийти</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="119"/>
        <source>Stopped</source>
        <translation>Зупинено</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="144"/>
        <source>Now Playing</source>
        <translation>Зараз грає</translation>
    </message>
</context>
<context>
    <name>StatusIconFactory</name>
    <message>
        <location filename="../statusiconfactory.cpp" line="29"/>
        <source>Status Icon Plugin</source>
        <translation>Втулок Status Icon</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="49"/>
        <source>About Status Icon Plugin</source>
        <translation>Про втулок Status Icon</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="50"/>
        <source>Qmmp Status Icon Plugin</source>
        <translation>Втулок Status Icon для Qmmp</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="51"/>
        <source>Written by:</source>
        <translation>Розробники:</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="52"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="53"/>
        <source>Artur Guzik &lt;a.guzik88@gmail.com&gt;</source>
        <translation>Артур Ґузік &lt;a.guzik88@gmail.com&gt;</translation>
    </message>
</context>
<context>
    <name>StatusIconSettingsDialog</name>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="14"/>
        <source>Status Icon Plugin Settings</source>
        <translation>Налаштування втулка Status Icon</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="29"/>
        <source>Balloon message</source>
        <translation>Виринне повідомлення</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="38"/>
        <location filename="../statusiconsettingsdialog.ui" line="105"/>
        <source>Delay, ms:</source>
        <translation>Затримка, мс:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="79"/>
        <source>Tooltip</source>
        <translation>Підказка</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="91"/>
        <source>Try to split file name when no tag</source>
        <translation>Розділити ім&apos;я файлу, якщо нема теґів</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="98"/>
        <source>Show progress bar</source>
        <translation>Показувати смугу поступу</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="131"/>
        <source>Transparency:</source>
        <translation>Прозорість:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="165"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="174"/>
        <source>Cover size:</source>
        <translation>Розмір обкладинки:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="214"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="225"/>
        <source>Edit template</source>
        <translation>Змінити шаблон</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="250"/>
        <source>Use standard icons</source>
        <translation>Використовувати стандартні піктограми</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.cpp" line="85"/>
        <source>Tooltip Template</source>
        <translation>Шаблон виринної підказки</translation>
    </message>
</context>
</TS>
