<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_RS">
<context>
    <name>ScrobblerFactory</name>
    <message>
        <location filename="../scrobblerfactory.cpp" line="31"/>
        <source>Scrobbler Plugin</source>
        <translation>Скроблер</translation>
    </message>
    <message>
        <location filename="../scrobblerfactory.cpp" line="51"/>
        <source>About Scrobbler Plugin</source>
        <translation>О прикључку за скробловање</translation>
    </message>
    <message>
        <location filename="../scrobblerfactory.cpp" line="52"/>
        <source>Qmmp AudioScrobbler Plugin</source>
        <translation>Кумп прикључак за скробловање</translation>
    </message>
    <message>
        <location filename="../scrobblerfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ScrobblerSettingsDialog</name>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="14"/>
        <source>Scrobbler Plugin Settings</source>
        <translation type="unfinished">Поставке скроблера</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="20"/>
        <source>Last.fm</source>
        <translation type="unfinished">Ласт.фм</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="31"/>
        <location filename="../scrobblersettingsdialog.ui" line="104"/>
        <source>Session:</source>
        <translation type="unfinished">Сесија:</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="41"/>
        <location filename="../scrobblersettingsdialog.ui" line="117"/>
        <source>Check</source>
        <translation type="unfinished">Провери</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="63"/>
        <location filename="../scrobblersettingsdialog.ui" line="95"/>
        <source>Register new session</source>
        <translation type="unfinished">Региструј нову сесију</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="73"/>
        <source>Libre.fm</source>
        <translation type="unfinished">Либре.фм</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="110"/>
        <location filename="../scrobblersettingsdialog.cpp" line="136"/>
        <location filename="../scrobblersettingsdialog.cpp" line="187"/>
        <source>Message</source>
        <translation type="unfinished">Порука</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="111"/>
        <source>1. Wait for browser startup</source>
        <translation type="unfinished">1. Сачекајте да се прегледач покрене</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="112"/>
        <source>2. Allow Qmmp to scrobble tracks to your %1 account</source>
        <translation type="unfinished">2. Дозволите Кумпу да скроблује нумере на ваш %1 налог</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="113"/>
        <source>3. Press &quot;OK&quot;</source>
        <translation type="unfinished">3. Притисните „У реду“</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="118"/>
        <location filename="../scrobblersettingsdialog.cpp" line="122"/>
        <location filename="../scrobblersettingsdialog.cpp" line="151"/>
        <location filename="../scrobblersettingsdialog.cpp" line="155"/>
        <location filename="../scrobblersettingsdialog.cpp" line="195"/>
        <location filename="../scrobblersettingsdialog.cpp" line="199"/>
        <source>Error</source>
        <translation type="unfinished">Грешка</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="118"/>
        <location filename="../scrobblersettingsdialog.cpp" line="151"/>
        <location filename="../scrobblersettingsdialog.cpp" line="195"/>
        <source>Network error</source>
        <translation type="unfinished">Грешка мреже</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="122"/>
        <location filename="../scrobblersettingsdialog.cpp" line="155"/>
        <source>Unable to register new session</source>
        <translation type="unfinished">Не могу да региструјем нову сесију</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="136"/>
        <source>New session has been received successfully</source>
        <translation type="unfinished">Нова сесија је успешно регистрована</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="187"/>
        <source>Permission granted</source>
        <translation type="unfinished">Дозвола одобрена</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="199"/>
        <source>Permission denied</source>
        <translation type="unfinished">Дозвола одбијена</translation>
    </message>
</context>
</TS>
