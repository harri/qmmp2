<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="gl_ES">
<context>
    <name>ScrobblerFactory</name>
    <message>
        <location filename="../scrobblerfactory.cpp" line="31"/>
        <source>Scrobbler Plugin</source>
        <translation>Engadido Scrobbler</translation>
    </message>
    <message>
        <location filename="../scrobblerfactory.cpp" line="51"/>
        <source>About Scrobbler Plugin</source>
        <translation>Sobre o engadido Scrobbler</translation>
    </message>
    <message>
        <location filename="../scrobblerfactory.cpp" line="52"/>
        <source>Qmmp AudioScrobbler Plugin</source>
        <translation>Engadio AudioScrobbler de Qmmp</translation>
    </message>
    <message>
        <location filename="../scrobblerfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Escrito por: LLya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ScrobblerSettingsDialog</name>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="14"/>
        <source>Scrobbler Plugin Settings</source>
        <translation>Preferencias do engadido Scrobbler</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="20"/>
        <source>Last.fm</source>
        <translation>Last.fm</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="31"/>
        <location filename="../scrobblersettingsdialog.ui" line="104"/>
        <source>Session:</source>
        <translation>Sesión:</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="41"/>
        <location filename="../scrobblersettingsdialog.ui" line="117"/>
        <source>Check</source>
        <translation>Comprobar</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="63"/>
        <location filename="../scrobblersettingsdialog.ui" line="95"/>
        <source>Register new session</source>
        <translation>Rexistrar nova sesión</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.ui" line="73"/>
        <source>Libre.fm</source>
        <translation>Libre.fm</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="110"/>
        <location filename="../scrobblersettingsdialog.cpp" line="136"/>
        <location filename="../scrobblersettingsdialog.cpp" line="187"/>
        <source>Message</source>
        <translation>Mensaxe</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="111"/>
        <source>1. Wait for browser startup</source>
        <translation>1. Esperar a que o navegador inicie</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="112"/>
        <source>2. Allow Qmmp to scrobble tracks to your %1 account</source>
        <translation>2. Permitir a Qmmp &quot;scrobblear&quot; pistas a túa conta %1</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="113"/>
        <source>3. Press &quot;OK&quot;</source>
        <translation>3. Prema &quot;OK&quot;</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="118"/>
        <location filename="../scrobblersettingsdialog.cpp" line="122"/>
        <location filename="../scrobblersettingsdialog.cpp" line="151"/>
        <location filename="../scrobblersettingsdialog.cpp" line="155"/>
        <location filename="../scrobblersettingsdialog.cpp" line="195"/>
        <location filename="../scrobblersettingsdialog.cpp" line="199"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="118"/>
        <location filename="../scrobblersettingsdialog.cpp" line="151"/>
        <location filename="../scrobblersettingsdialog.cpp" line="195"/>
        <source>Network error</source>
        <translation>Erro de rede</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="122"/>
        <location filename="../scrobblersettingsdialog.cpp" line="155"/>
        <source>Unable to register new session</source>
        <translation>Non se puido rexistrar nova sesión</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="136"/>
        <source>New session has been received successfully</source>
        <translation>A nova sesión recibiuse correctamente</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="187"/>
        <source>Permission granted</source>
        <translation>Permiso concedido</translation>
    </message>
    <message>
        <location filename="../scrobblersettingsdialog.cpp" line="199"/>
        <source>Permission denied</source>
        <translation>Permiso denegado</translation>
    </message>
</context>
</TS>
