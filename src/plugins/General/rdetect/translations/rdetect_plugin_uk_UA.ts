<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>RDetectFactory</name>
    <message>
        <location filename="../rdetectfactory.cpp" line="30"/>
        <source>Volume Detection Plugin</source>
        <translation>Втулок визначення томів</translation>
    </message>
    <message>
        <location filename="../rdetectfactory.cpp" line="50"/>
        <source>About Volume Detection Plugin</source>
        <translation>Про втулок визначення томів</translation>
    </message>
    <message>
        <location filename="../rdetectfactory.cpp" line="51"/>
        <source>Qmmp Removable Volume Detection Plugin</source>
        <translation>Втулок визначення знімних томів для Qmmp</translation>
    </message>
    <message>
        <location filename="../rdetectfactory.cpp" line="52"/>
        <source>This plugin provides removable volume detection</source>
        <translation>Цей втулок відстежує знімні томи</translation>
    </message>
    <message>
        <location filename="../rdetectfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов  &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>RDetectSettingsDialog</name>
    <message>
        <location filename="../rdetectsettingsdialog.ui" line="14"/>
        <source>Volume Detection Plugin Settings</source>
        <translation>Налаштування втулка визначення томів</translation>
    </message>
    <message>
        <location filename="../rdetectsettingsdialog.ui" line="29"/>
        <source>CD Audio Detection</source>
        <translation>Автовизначення авдіо-КД</translation>
    </message>
    <message>
        <location filename="../rdetectsettingsdialog.ui" line="38"/>
        <source>Add tracks to playlist automatically</source>
        <translation>Автоматично додавати доріжки до списку</translation>
    </message>
    <message>
        <location filename="../rdetectsettingsdialog.ui" line="45"/>
        <source>Remove tracks from playlist automatically</source>
        <translation>Автоматично вилучати доріжки зі списку</translation>
    </message>
    <message>
        <location filename="../rdetectsettingsdialog.ui" line="55"/>
        <source>Removable Device Detection</source>
        <translation>Виявлення змінних пристроїв</translation>
    </message>
    <message>
        <location filename="../rdetectsettingsdialog.ui" line="64"/>
        <source>Add files to playlist automatically</source>
        <translation>Автоматично додавати файли до списку</translation>
    </message>
    <message>
        <location filename="../rdetectsettingsdialog.ui" line="71"/>
        <source>Remove files from playlist automatically</source>
        <translation>Автоматично вилучати файли зі списку</translation>
    </message>
</context>
<context>
    <name>RemovableHelper</name>
    <message>
        <location filename="../removablehelper.cpp" line="134"/>
        <source>Add CD &quot;%1&quot;</source>
        <translation>Додати CD &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../removablehelper.cpp" line="138"/>
        <source>Add Volume &quot;%1&quot;</source>
        <translation>Додати том &quot;%1&quot;</translation>
    </message>
</context>
</TS>
