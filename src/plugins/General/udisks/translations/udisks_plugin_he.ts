<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>UDisksFactory</name>
    <message>
        <location filename="../udisksfactory.cpp" line="30"/>
        <source>UDisks Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="50"/>
        <source>About UDisks Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="51"/>
        <source>Qmmp UDisks Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="52"/>
        <source>This plugin provides removable devices detection using UDisks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UDisksPlugin</name>
    <message>
        <location filename="../udisksplugin.cpp" line="132"/>
        <source>Add CD &quot;%1&quot;</source>
        <translation type="unfinished">הוסף תקליטור &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../udisksplugin.cpp" line="140"/>
        <source>Add Volume &quot;%1&quot;</source>
        <translation type="unfinished">הוסף כרך &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>UDisksSettingsDialog</name>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="14"/>
        <source>UDisks Plugin Settings</source>
        <translation type="unfinished">הגדרות תוסף UDisks</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="29"/>
        <source>CD Audio Detection</source>
        <translation type="unfinished">איתור אוטומטי של תקליטור שמע</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="38"/>
        <source>Add tracks to playlist automatically</source>
        <translation type="unfinished">הוסף רצועות אוטומטית לתוך רשימת נגינה</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="45"/>
        <source>Remove tracks from playlist automatically</source>
        <translation type="unfinished">הסר רצועות אוטומטית מתוך רשימת נגינה</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="55"/>
        <source>Removable Device Detection</source>
        <translation type="unfinished">איתור התקנים נשלפים</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="64"/>
        <source>Add files to playlist automatically</source>
        <translation type="unfinished">הוסף קבצים אוטומטית לתוך רשימת נגינה</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="71"/>
        <source>Remove files from playlist automatically</source>
        <translation type="unfinished">הסר קבצים אוטומטית מתוך רשימת נגינה</translation>
    </message>
</context>
</TS>
