<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>UDisksFactory</name>
    <message>
        <location filename="../udisksfactory.cpp" line="30"/>
        <source>UDisks Plugin</source>
        <translation>UDisks プラグイン</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="50"/>
        <source>About UDisks Plugin</source>
        <translation>UDisks プラグインについて</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="51"/>
        <source>Qmmp UDisks Plugin</source>
        <translation>QMMP UDisks プラグイン</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="52"/>
        <source>This plugin provides removable devices detection using UDisks</source>
        <translation>このプラグインは着脱可能なデバイスの検知を UDisks により行ないます</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>UDisksPlugin</name>
    <message>
        <location filename="../udisksplugin.cpp" line="132"/>
        <source>Add CD &quot;%1&quot;</source>
        <translation>CD &quot;%1&quot; を追加</translation>
    </message>
    <message>
        <location filename="../udisksplugin.cpp" line="140"/>
        <source>Add Volume &quot;%1&quot;</source>
        <translation>量目 &quot;%1&quot; を追加</translation>
    </message>
</context>
<context>
    <name>UDisksSettingsDialog</name>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="14"/>
        <source>UDisks Plugin Settings</source>
        <translation>UDisks プラグイン設定</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="29"/>
        <source>CD Audio Detection</source>
        <translation>音楽 CD 検知</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="38"/>
        <source>Add tracks to playlist automatically</source>
        <translation>プレイリストにトラックを自動追加</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="45"/>
        <source>Remove tracks from playlist automatically</source>
        <translation>プレイリストからトラックを自動除去</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="55"/>
        <source>Removable Device Detection</source>
        <translation>着脱可能なデバイスの検知</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="64"/>
        <source>Add files to playlist automatically</source>
        <translation>プレイリストにファイルを自動追加</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="71"/>
        <source>Remove files from playlist automatically</source>
        <translation>プレイリストからファイルを自動除去</translation>
    </message>
</context>
</TS>
