<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>UDisksFactory</name>
    <message>
        <location filename="../udisksfactory.cpp" line="30"/>
        <source>UDisks Plugin</source>
        <translation>UDisks Eklentisi</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="50"/>
        <source>About UDisks Plugin</source>
        <translation>UDisks Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="51"/>
        <source>Qmmp UDisks Plugin</source>
        <translation>Qmmp UDisks Eklentisi</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="52"/>
        <source>This plugin provides removable devices detection using UDisks</source>
        <translation>Bu eklenti UDisks kullanarak çıkarılabilir aygıt algılama sağlar</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>UDisksPlugin</name>
    <message>
        <location filename="../udisksplugin.cpp" line="132"/>
        <source>Add CD &quot;%1&quot;</source>
        <translation>CD &quot;%1&quot; Ekle</translation>
    </message>
    <message>
        <location filename="../udisksplugin.cpp" line="140"/>
        <source>Add Volume &quot;%1&quot;</source>
        <translation>&quot;%1&quot; Aygıtını Ekle</translation>
    </message>
</context>
<context>
    <name>UDisksSettingsDialog</name>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="14"/>
        <source>UDisks Plugin Settings</source>
        <translation>UDisks Eklenti Ayarları</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="29"/>
        <source>CD Audio Detection</source>
        <translation>CD Ses Algılama</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="38"/>
        <source>Add tracks to playlist automatically</source>
        <translation>Parçaları listeye otomatik olarka ekle</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="45"/>
        <source>Remove tracks from playlist automatically</source>
        <translation>Parçaları listeden otomatik olarak kaldır</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="55"/>
        <source>Removable Device Detection</source>
        <translation>Çıkarılabilir Aygıt Algılama</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="64"/>
        <source>Add files to playlist automatically</source>
        <translation>Dosyaları listeye otomatik olarak ekle</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="71"/>
        <source>Remove files from playlist automatically</source>
        <translation>Dosyaları listeden otomatik olarak kaldır</translation>
    </message>
</context>
</TS>
