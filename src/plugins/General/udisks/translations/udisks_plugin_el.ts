<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>UDisksFactory</name>
    <message>
        <location filename="../udisksfactory.cpp" line="30"/>
        <source>UDisks Plugin</source>
        <translation>Πρόσθετο UDisks</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="50"/>
        <source>About UDisks Plugin</source>
        <translation>Σχετικά με το πρόσθετο UDisks</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="51"/>
        <source>Qmmp UDisks Plugin</source>
        <translation>Qmmp πρόσθετο UDisks</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="52"/>
        <source>This plugin provides removable devices detection using UDisks</source>
        <translation>Αυτό το πρόσθετο παρέχει την ανίχνευση αφαιρούμενων συσκευών μέσω UDisks</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Γράφτηκε από τον: Ilya Kotov &lt;forkotov02@hotmail.ru&gt;</translation>
    </message>
</context>
<context>
    <name>UDisksPlugin</name>
    <message>
        <location filename="../udisksplugin.cpp" line="132"/>
        <source>Add CD &quot;%1&quot;</source>
        <translation>Προσθήκη CD «%1»</translation>
    </message>
    <message>
        <location filename="../udisksplugin.cpp" line="140"/>
        <source>Add Volume &quot;%1&quot;</source>
        <translation>Προσθήκη Έντασης «%1»</translation>
    </message>
</context>
<context>
    <name>UDisksSettingsDialog</name>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="14"/>
        <source>UDisks Plugin Settings</source>
        <translation>Ρυθμίσεις πρόσθετου UDisks</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="29"/>
        <source>CD Audio Detection</source>
        <translation>Ανίχνευση CD ήχου</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="38"/>
        <source>Add tracks to playlist automatically</source>
        <translation>Προσθήκη των κομματιών αυτόματα στη λίστα αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="45"/>
        <source>Remove tracks from playlist automatically</source>
        <translation>Αφαίρεση των κομματιών αυτόματα από τη λίστα αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="55"/>
        <source>Removable Device Detection</source>
        <translation>Ανίχνευση αφαιρούμενης συσκευής</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="64"/>
        <source>Add files to playlist automatically</source>
        <translation>Προσθήκη αρχείων αυτόματα στη λίστα αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="71"/>
        <source>Remove files from playlist automatically</source>
        <translation>Αφαίρεση των αρχείων από τη λίστα αναπαραγωγής αυτόματα</translation>
    </message>
</context>
</TS>
