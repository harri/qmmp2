<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>TrackChangeFactory</name>
    <message>
        <location filename="../trackchangefactory.cpp" line="29"/>
        <source>Track Change Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangefactory.cpp" line="49"/>
        <source>About Track Change Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangefactory.cpp" line="50"/>
        <source>Qmmp Track Change Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangefactory.cpp" line="51"/>
        <source>This plugin executes external command when current track is changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangefactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackChangeSettingsDialog</name>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="14"/>
        <source>Track Change Plugin Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="29"/>
        <source>Command to run when Qmmp starts new track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="39"/>
        <location filename="../trackchangesettingsdialog.ui" line="56"/>
        <location filename="../trackchangesettingsdialog.ui" line="73"/>
        <location filename="../trackchangesettingsdialog.ui" line="90"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="46"/>
        <source>Command to run toward to end of a track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="63"/>
        <source>Command to run when Qmmp reaches the end of the playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="80"/>
        <source>Command to run when title changes (i.e. network streams title)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="97"/>
        <source>Command to run on application startup:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="104"/>
        <source>Command to run on application exit:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
