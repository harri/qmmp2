<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>RGScanDialog</name>
    <message>
        <location filename="../rgscandialog.ui" line="14"/>
        <source>ReplayGain Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="84"/>
        <source>Write track gain/peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="42"/>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="47"/>
        <source>Progress</source>
        <translation>İlerliyor</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="52"/>
        <source>Track Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="57"/>
        <source>Album Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="62"/>
        <source>Track Peak</source>
        <translation>Parça Zirvesi</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="67"/>
        <source>Album Peak</source>
        <translation>Albüm Zirvesi</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="77"/>
        <source>Skip already scanned files</source>
        <translation>Zaten taranmış dosyaları atla</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="113"/>
        <source>Calculate</source>
        <translation>Hesapla</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="120"/>
        <source>Write Tags</source>
        <translation>Etiketleri Yaz</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="91"/>
        <source>Write album gain/peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscandialog.cpp" line="127"/>
        <location filename="../rgscandialog.cpp" line="233"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../rgscandialog.cpp" line="137"/>
        <location filename="../rgscandialog.cpp" line="138"/>
        <location filename="../rgscandialog.cpp" line="163"/>
        <location filename="../rgscandialog.cpp" line="228"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
</context>
<context>
    <name>RGScanFactory</name>
    <message>
        <location filename="../rgscanfactory.cpp" line="28"/>
        <source>ReplayGain Scanner Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="49"/>
        <source>About ReplayGain Scanner Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="50"/>
        <source>ReplayGain Scanner Plugin for Qmmp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="51"/>
        <source>This plugin scans audio files and gives information for volume normalization</source>
        <translation>Bu eklenti ses dosyalarını tarar ve ses normalleştirmesi için bilgilendirim verir.</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="53"/>
        <source>Based on source code by:</source>
        <translation>Kaynak koduna dayalı:</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="54"/>
        <source>David Robinson &lt;David@Robinson.org&gt;</source>
        <translation>David Robinson &lt;David@Robinson.org&gt;</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="55"/>
        <source>Glen Sawyer &lt;mp3gain@hotmail.com&gt;</source>
        <translation>Glen Sawyer &lt;mp3gain@hotmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="56"/>
        <source>Frank Klemm</source>
        <translation>Frank Klemm</translation>
    </message>
</context>
<context>
    <name>RGScanHelper</name>
    <message>
        <location filename="../rgscanhelper.cpp" line="32"/>
        <source>ReplayGain Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rgscanhelper.cpp" line="33"/>
        <source>Meta+R</source>
        <translation>Meta+R</translation>
    </message>
</context>
</TS>
