<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>RGScanDialog</name>
    <message>
        <location filename="../rgscandialog.ui" line="14"/>
        <source>ReplayGain Scanner</source>
        <translation>Сканер ReplayGain</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="84"/>
        <source>Write track gain/peak</source>
        <translation>Записувати пік/підсилення для доріжки</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="42"/>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="47"/>
        <source>Progress</source>
        <translation>Поступ</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="52"/>
        <source>Track Gain</source>
        <translation>Підсилення доріжки</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="57"/>
        <source>Album Gain</source>
        <translation>Підсилення альбому</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="62"/>
        <source>Track Peak</source>
        <translation>Пік доріжки</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="67"/>
        <source>Album Peak</source>
        <translation>Пік альбому</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="77"/>
        <source>Skip already scanned files</source>
        <translation>Пропустити вже відскановані файли</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="113"/>
        <source>Calculate</source>
        <translation>Розрахувати</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="120"/>
        <source>Write Tags</source>
        <translation>Записати теґи</translation>
    </message>
    <message>
        <location filename="../rgscandialog.ui" line="91"/>
        <source>Write album gain/peak</source>
        <translation>Записувати пік/підсилення для альбому</translation>
    </message>
    <message>
        <location filename="../rgscandialog.cpp" line="127"/>
        <location filename="../rgscandialog.cpp" line="233"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../rgscandialog.cpp" line="137"/>
        <location filename="../rgscandialog.cpp" line="138"/>
        <location filename="../rgscandialog.cpp" line="163"/>
        <location filename="../rgscandialog.cpp" line="228"/>
        <source>%1 dB</source>
        <translation>%1 дБ</translation>
    </message>
</context>
<context>
    <name>RGScanFactory</name>
    <message>
        <location filename="../rgscanfactory.cpp" line="28"/>
        <source>ReplayGain Scanner Plugin</source>
        <translation>Втулок сканування ReplayGain</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="49"/>
        <source>About ReplayGain Scanner Plugin</source>
        <translation>Про втулок сканування ReplayGain</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="50"/>
        <source>ReplayGain Scanner Plugin for Qmmp</source>
        <translation>Втулок сканування ReplayGain для Qmmp</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="51"/>
        <source>This plugin scans audio files and gives information for volume normalization</source>
        <translation>Цей втулок сканує авдіофайли та видає інформацію для нормалізації гучности</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="53"/>
        <source>Based on source code by:</source>
        <translation>Заснований на джерельному коді таких розробників:</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="54"/>
        <source>David Robinson &lt;David@Robinson.org&gt;</source>
        <translation>Девід Робінзон &lt;David@Robinson.org&gt;</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="55"/>
        <source>Glen Sawyer &lt;mp3gain@hotmail.com&gt;</source>
        <translation>Ґлен Сойєр &lt;mp3gain@hotmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../rgscanfactory.cpp" line="56"/>
        <source>Frank Klemm</source>
        <translation>Френк Клемм</translation>
    </message>
</context>
<context>
    <name>RGScanHelper</name>
    <message>
        <location filename="../rgscanhelper.cpp" line="32"/>
        <source>ReplayGain Scanner</source>
        <translation>Сканер ReplaGain</translation>
    </message>
    <message>
        <location filename="../rgscanhelper.cpp" line="33"/>
        <source>Meta+R</source>
        <translation>Meta+R</translation>
    </message>
</context>
</TS>
