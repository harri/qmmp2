<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>DecoderXmpFactory</name>
    <message>
        <location filename="../decoderxmpfactory.cpp" line="39"/>
        <source>XMP Plugin</source>
        <translation>XMP Eklentisi</translation>
    </message>
    <message>
        <location filename="../decoderxmpfactory.cpp" line="54"/>
        <source>Module Files</source>
        <translation>Modül Dosyaları</translation>
    </message>
    <message>
        <location filename="../decoderxmpfactory.cpp" line="109"/>
        <source>About XMP Audio Plugin</source>
        <translation>XMP Ses Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../decoderxmpfactory.cpp" line="110"/>
        <source>Qmmp XMP Audio Plugin</source>
        <translation>Qmmp XMP Ses Eklentisi</translation>
    </message>
    <message>
        <location filename="../decoderxmpfactory.cpp" line="111"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../decoderxmpfactory.cpp" line="112"/>
        <source>Compiled against libxmp-%1</source>
        <translation>libxmp-%1&apos;e  dayanarak derlendi</translation>
    </message>
</context>
<context>
    <name>XmpMetaDataModel</name>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="51"/>
        <source>Volume scale</source>
        <translation>Ses ölçeği</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="52"/>
        <source>Number of patterns</source>
        <translation>Desenlerin sayısı</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="53"/>
        <source>Number of tracks</source>
        <translation>Parçaların sayısı</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="54"/>
        <source>Tracks per pattern</source>
        <translation>Desen başına parçalar</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="55"/>
        <source>Number of instruments</source>
        <translation>Müzik aletlerinin sayısı</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="56"/>
        <source>Number of samples</source>
        <translation>Örneklerin sayısı</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="57"/>
        <source>Initial speed</source>
        <translation>Başlangıç hızı</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="58"/>
        <source>Initial BPM</source>
        <translation>İlk BPM</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="59"/>
        <source>Module length in patterns</source>
        <translation>Desenlerde modül uzunluğu</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="79"/>
        <source>Samples</source>
        <translation>Örnekler</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="87"/>
        <source>Instruments</source>
        <translation>Müzik aletleri</translation>
    </message>
    <message>
        <location filename="../xmpmetadatamodel.cpp" line="92"/>
        <source>Comment</source>
        <translation>Yorum</translation>
    </message>
</context>
<context>
    <name>XmpSettingsDialog</name>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="14"/>
        <source>XMP Plugin Settings</source>
        <translation>XMP Eklenti Ayarları</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="34"/>
        <source>Amplification factor:</source>
        <translation>Yükseltme etmeni:</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="51"/>
        <source>Stereo mixing:</source>
        <translation>Steryo karıştırma:</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="68"/>
        <source> Interpolation type:</source>
        <translation>İnterpolasyon türü:</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="78"/>
        <source>Lowpass filter effect</source>
        <translation>Düşük Geçişli filtre etkisi</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="85"/>
        <source>Use vblank timing</source>
        <translation>Vblank zamanlamasını kullan</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="92"/>
        <source>Emulate Protracker 2.x FX9 bug</source>
        <translation>Protracker 2.x FX9 hatasını taklit et</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="99"/>
        <source>Sample rate:</source>
        <translation>Örnekleme oranı:</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="109"/>
        <source>Emulate sample loop bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.ui" line="116"/>
        <source>Use Paula mixer in Amiga modules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.cpp" line="33"/>
        <source>22050 Hz</source>
        <translation>22050 Hz</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.cpp" line="34"/>
        <source>44100 Hz</source>
        <translation>44100 Hz</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.cpp" line="35"/>
        <source>48000 Hz</source>
        <translation>48000 Hz</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.cpp" line="36"/>
        <source>Nearest neighbor</source>
        <translation>Yakınındaki komşu</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.cpp" line="37"/>
        <source>Linear</source>
        <translation>Doğrusal</translation>
    </message>
    <message>
        <location filename="../xmpsettingsdialog.cpp" line="38"/>
        <source>Cubic spline</source>
        <translation>Kübik kama</translation>
    </message>
</context>
</TS>
