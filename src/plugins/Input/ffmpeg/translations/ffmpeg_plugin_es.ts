<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>DecoderFFmpegFactory</name>
    <message>
        <location filename="../decoderffmpegfactory.cpp" line="144"/>
        <source>FFmpeg Plugin</source>
        <translation>Módulo FFmpeg</translation>
    </message>
    <message>
        <location filename="../decoderffmpegfactory.cpp" line="146"/>
        <source>FFmpeg Formats</source>
        <translation>Formatos FFmpeg</translation>
    </message>
    <message>
        <location filename="../decoderffmpegfactory.cpp" line="340"/>
        <source>About FFmpeg Audio Plugin</source>
        <translation>Acerca del módulo de audio FFmpeg</translation>
    </message>
    <message>
        <location filename="../decoderffmpegfactory.cpp" line="341"/>
        <source>Qmmp FFmpeg Audio Plugin</source>
        <translation>Módulo de audio FFmpeg para Qmmp</translation>
    </message>
    <message>
        <location filename="../decoderffmpegfactory.cpp" line="342"/>
        <source>Compiled against:</source>
        <translation>Compilado contra:</translation>
    </message>
    <message>
        <location filename="../decoderffmpegfactory.cpp" line="355"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Escrito por: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>FFmpegSettingsDialog</name>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="14"/>
        <source>FFmpeg Plugin Settings</source>
        <translation>Configuración del módulo FFmpeg</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="55"/>
        <source>Formats</source>
        <translation>Formatos</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="61"/>
        <source>Windows Media Audio</source>
        <translation>Windows Media Audio</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="68"/>
        <source>Monkey&apos;s Audio (APE)</source>
        <translation>Monkey&apos;s Audio (APE)</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="75"/>
        <source>True Audio</source>
        <translation>True Audio</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="82"/>
        <source>ADTS AAC</source>
        <translation>ADTS AAC</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="89"/>
        <source>MP3 (MPEG audio layer 3)</source>
        <translation>MP3 (MPEG Audio Layer 3)</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="96"/>
        <source>MPEG-4 AAC/ALAC</source>
        <translation>MPEG-4 AAC/ALAC</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="103"/>
        <source>RealAudio 1.0/2.0</source>
        <translation>RealAudio 1.0/2.0</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="110"/>
        <source>Shorten</source>
        <translation>Shorten</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="117"/>
        <source>AC3/EAC</source>
        <translation>AC3/EAC</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="124"/>
        <source>DTS/DTS-Core</source>
        <translation>DTS/DTS-Core</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="131"/>
        <source>Matroska Audio (Dolby TrueHD Lossless)</source>
        <translation>Matroska Audio (Dolby TrueHD sin pérdida)</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="138"/>
        <source>VQF</source>
        <translation>VQF</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="145"/>
        <source>Tom&apos;s lossless Audio Kompressor (TAK)</source>
        <translation>Compresor de audio sin pérdida de Tom (TAK)</translation>
    </message>
    <message>
        <location filename="../ffmpegsettingsdialog.ui" line="152"/>
        <source>Direct Stream Digital (DSD)</source>
        <translation>Direct Stream Digital (DSD)</translation>
    </message>
</context>
</TS>
