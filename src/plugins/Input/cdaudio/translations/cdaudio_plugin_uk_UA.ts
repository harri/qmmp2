<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>CDAudioSettingsDialog</name>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="14"/>
        <source>CD Audio Plugin Settings</source>
        <translation>Налаштування втулка AudioCD</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="139"/>
        <source>Override device:</source>
        <translation>Перевизначити пристрій:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="129"/>
        <source>Limit cd speed:</source>
        <translation>Обмежити швидкість КД:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="122"/>
        <source>Use cd-text</source>
        <translation>Використовувати cd-text</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="39"/>
        <source>CDDB</source>
        <translation>CDDB</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="48"/>
        <source>Use HTTP instead of CDDBP</source>
        <translation>Використовувати HTTP замість CDDBP</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="58"/>
        <source>Server:</source>
        <translation>Сервер:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="65"/>
        <source>Path:</source>
        <translation>Шлях:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="75"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="87"/>
        <source>Clear CDDB cache</source>
        <translation>Очистити кеш CDDB</translation>
    </message>
</context>
<context>
    <name>DecoderCDAudioFactory</name>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="52"/>
        <source>CD Audio Plugin</source>
        <translation>Втулок AudioCD</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="98"/>
        <source>About CD Audio Plugin</source>
        <translation>Про втулок AudioCD</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="99"/>
        <source>Qmmp CD Audio Plugin</source>
        <translation>Втулок AudioCD для Qmmp</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="101"/>
        <source>Compiled against libcdio-%1 and libcddb-%2</source>
        <translation>Зібрано з libcdio-%1 та libcddb-%2</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="104"/>
        <source>Compiled against libcdio-%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="107"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="108"/>
        <source>Usage: open cdda:/// using Add URL dialog or command line</source>
        <translation>Використання: відкрийте cdda:/// використовуючи діалог &quot;Додати адресу&quot; чи командний рядок</translation>
    </message>
</context>
</TS>
