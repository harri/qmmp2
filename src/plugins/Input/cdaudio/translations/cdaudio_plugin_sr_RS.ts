<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_RS">
<context>
    <name>CDAudioSettingsDialog</name>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="14"/>
        <source>CD Audio Plugin Settings</source>
        <translation type="unfinished">Поставке музичког диска</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="139"/>
        <source>Override device:</source>
        <translation type="unfinished">Дефиниши уређај:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="129"/>
        <source>Limit cd speed:</source>
        <translation type="unfinished">Ограничи брзину диска:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="122"/>
        <source>Use cd-text</source>
        <translation type="unfinished">Користи ЦД-текст</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="39"/>
        <source>CDDB</source>
        <translation type="unfinished">ЦДДБ</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="48"/>
        <source>Use HTTP instead of CDDBP</source>
        <translation type="unfinished">ХТТП уместо ЦДДБП</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="58"/>
        <source>Server:</source>
        <translation type="unfinished">Сервер:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="65"/>
        <source>Path:</source>
        <translation type="unfinished">Путања:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="75"/>
        <source>Port:</source>
        <translation type="unfinished">Порт:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="87"/>
        <source>Clear CDDB cache</source>
        <translation type="unfinished">Очисти ЦДДБ кеш</translation>
    </message>
</context>
<context>
    <name>DecoderCDAudioFactory</name>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="52"/>
        <source>CD Audio Plugin</source>
        <translation>Музички диск</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="98"/>
        <source>About CD Audio Plugin</source>
        <translation>О прикључку за музички диск</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="99"/>
        <source>Qmmp CD Audio Plugin</source>
        <translation>Кумп прикључак за музички диск</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="101"/>
        <source>Compiled against libcdio-%1 and libcddb-%2</source>
        <translation>Компилован на libcdio-%1 и libcddb-%2</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="104"/>
        <source>Compiled against libcdio-%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="107"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="108"/>
        <source>Usage: open cdda:/// using Add URL dialog or command line</source>
        <translation>Употреба: отворите cdda:/// користећи дијалог за додавање УРЛ-а или командну линију</translation>
    </message>
</context>
</TS>
