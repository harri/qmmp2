<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>CDAudioSettingsDialog</name>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="14"/>
        <source>CD Audio Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin Audio CD</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="139"/>
        <source>Override device:</source>
        <translation type="unfinished">Perangkat timpa:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="129"/>
        <source>Limit cd speed:</source>
        <translation type="unfinished">Batas kecepatan cd:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="122"/>
        <source>Use cd-text</source>
        <translation type="unfinished">Gunakan cd-text</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="39"/>
        <source>CDDB</source>
        <translation type="unfinished">CDDB</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="48"/>
        <source>Use HTTP instead of CDDBP</source>
        <translation type="unfinished">Gunakan HTTP sebagai gantinya CDDBP</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="58"/>
        <source>Server:</source>
        <translation type="unfinished">Server:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="65"/>
        <source>Path:</source>
        <translation type="unfinished">Alur:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="75"/>
        <source>Port:</source>
        <translation type="unfinished">Port:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="87"/>
        <source>Clear CDDB cache</source>
        <translation type="unfinished">Bersihkan cache CDDB</translation>
    </message>
</context>
<context>
    <name>DecoderCDAudioFactory</name>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="52"/>
        <source>CD Audio Plugin</source>
        <translation>Plugin Audio CD</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="98"/>
        <source>About CD Audio Plugin</source>
        <translation>Tentang Plugin Audio CD</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="99"/>
        <source>Qmmp CD Audio Plugin</source>
        <translation>Plugin Audio CD Qmmp</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="101"/>
        <source>Compiled against libcdio-%1 and libcddb-%2</source>
        <translation>Dikompilasi terhadap libcdio-%1 dan libcddb-%2</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="104"/>
        <source>Compiled against libcdio-%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="107"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="108"/>
        <source>Usage: open cdda:/// using Add URL dialog or command line</source>
        <translation>Penggunaan: buka cdda:// menggunakan dialog Tambah URL atau baris perintah</translation>
    </message>
</context>
</TS>
