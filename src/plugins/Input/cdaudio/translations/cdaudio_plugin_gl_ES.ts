<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="gl_ES">
<context>
    <name>CDAudioSettingsDialog</name>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="14"/>
        <source>CD Audio Plugin Settings</source>
        <translation>Preferencias do engadido cd CD de audio</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="139"/>
        <source>Override device:</source>
        <translation>Anular dispositivo:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="129"/>
        <source>Limit cd speed:</source>
        <translation>Limitar a velocidade de cd:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="122"/>
        <source>Use cd-text</source>
        <translation>Usar cd-text</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="39"/>
        <source>CDDB</source>
        <translation>CDDB</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="48"/>
        <source>Use HTTP instead of CDDBP</source>
        <translation>Usar HTTP en lugar de CDDBP</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="58"/>
        <source>Server:</source>
        <translation>Servidor:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="65"/>
        <source>Path:</source>
        <translation>Ruta:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="75"/>
        <source>Port:</source>
        <translation>Porto:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="87"/>
        <source>Clear CDDB cache</source>
        <translation>Limpar caché CDDB</translation>
    </message>
</context>
<context>
    <name>DecoderCDAudioFactory</name>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="52"/>
        <source>CD Audio Plugin</source>
        <translation>Engadido de CD de audio</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="98"/>
        <source>About CD Audio Plugin</source>
        <translation>Sobre o engadido de CD de audio</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="99"/>
        <source>Qmmp CD Audio Plugin</source>
        <translation>Engadido de CD de audio de Qmmp</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="101"/>
        <source>Compiled against libcdio-%1 and libcddb-%2</source>
        <translation>Compilado usando libcdio-%1 e libcddb-%2</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="104"/>
        <source>Compiled against libcdio-%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="107"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Escrito por: LLya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="108"/>
        <source>Usage: open cdda:/// using Add URL dialog or command line</source>
        <translation>Uso: open cdda:/// usando diálogo de engadir URL ou liña de comandos</translation>
    </message>
</context>
</TS>
