<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>DecoderOpusFactory</name>
    <message>
        <location filename="../decoderopusfactory.cpp" line="42"/>
        <source>Opus Plugin</source>
        <translation>Модуль Opus</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="45"/>
        <source>Ogg Opus Files</source>
        <translation>Файлы Ogg Opus</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="125"/>
        <source>About Opus Audio Plugin</source>
        <translation>Об аудио-модуле Opus</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="126"/>
        <source>Qmmp Opus Audio Plugin</source>
        <translation>Модуль Opus для Qmmp</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="127"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Разработчик: Илья Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>OpusMetaDataModel</name>
    <message>
        <location filename="../opusmetadatamodel.cpp" line="55"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
</context>
</TS>
