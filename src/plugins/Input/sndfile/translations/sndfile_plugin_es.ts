<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>DecoderSndFileFactory</name>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="119"/>
        <source>Sndfile Plugin</source>
        <translation>Módulo Sndfile</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="123"/>
        <source>PCM Files</source>
        <translation>Archivos PCM</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="228"/>
        <source>About Sndfile Audio Plugin</source>
        <translation>Acerca del módulo de sonido Sndfile</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="229"/>
        <source>Qmmp Sndfile Audio Plugin</source>
        <translation>Módulo de sonido Sndfile para Qmmp</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="230"/>
        <source>Compiled against %1</source>
        <translation>Compilado contra %1</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="231"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Escrito por: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
