<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>DecoderSndFileFactory</name>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="119"/>
        <source>Sndfile Plugin</source>
        <translation>Plugin Sndfile</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="123"/>
        <source>PCM Files</source>
        <translation>Arquivos PCM</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="228"/>
        <source>About Sndfile Audio Plugin</source>
        <translation>Sobre o plugin Sndfile Audio</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="229"/>
        <source>Qmmp Sndfile Audio Plugin</source>
        <translation>Plugin Qmmp Sndfile Audio</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="230"/>
        <source>Compiled against %1</source>
        <translation>Compilado com %1</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="231"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
