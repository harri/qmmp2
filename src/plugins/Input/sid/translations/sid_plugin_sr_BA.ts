<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_BA">
<context>
    <name>DecoderSIDFactory</name>
    <message>
        <location filename="../decodersidfactory.cpp" line="57"/>
        <source>SID Plugin</source>
        <translation>СИД прикључак</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="59"/>
        <source>SID Files</source>
        <translation>СИД фајлови</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="112"/>
        <source>About SID Audio Plugin</source>
        <translation>О СИД прикључку</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="113"/>
        <source>Qmmp SID Audio Plugin</source>
        <translation>Кумп СИД прикључак</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="114"/>
        <source>This plugin plays Commodore 64 music files using libsidplayfp library</source>
        <translation>Користи libsidplayfp библиотеку за пуштање музичких фајлова за Комодоре 64</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="115"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>SidSettingsDialog</name>
    <message>
        <location filename="../sidsettingsdialog.ui" line="14"/>
        <source>SID Plugin Settings</source>
        <translation type="unfinished">Поставке СИД прикључка</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="55"/>
        <source>Fast resampling</source>
        <translation type="unfinished">Брзо преузорковање</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="62"/>
        <source>Sample rate:</source>
        <translation type="unfinished">Узорковање:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="69"/>
        <source>HVSC database file:</source>
        <translation type="unfinished">Фајл ХВСЦ базе података:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="76"/>
        <source>Resampling method:</source>
        <translation type="unfinished">Метода преузорковања:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="93"/>
        <source>Defaults song length, sec:</source>
        <translation type="unfinished">Подраз. дужина нумера [s]:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="100"/>
        <source>Enable HVSC song length database</source>
        <translation type="unfinished">Омогући ХВСЦ базу дужине нумера</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="107"/>
        <source>Emulation:</source>
        <translation type="unfinished">Емулација:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="41"/>
        <source>44100 Hz</source>
        <translation type="unfinished">44100 Hz</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="42"/>
        <source>48000 Hz</source>
        <translation type="unfinished">48000 Hz</translation>
    </message>
</context>
</TS>
