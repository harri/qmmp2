<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DecoderSIDFactory</name>
    <message>
        <location filename="../decodersidfactory.cpp" line="57"/>
        <source>SID Plugin</source>
        <translation>SID-Modul</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="59"/>
        <source>SID Files</source>
        <translation>SID-Dateien</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="112"/>
        <source>About SID Audio Plugin</source>
        <translation>Über SID-Audiomodul</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="113"/>
        <source>Qmmp SID Audio Plugin</source>
        <translation>Qmmp SID-Audiomodul</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="114"/>
        <source>This plugin plays Commodore 64 music files using libsidplayfp library</source>
        <translation>Dieses Modul spielt Commodore-64-Musikdateien unter Verwendung der Bibliothek libsidplayfp ab</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="115"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Geschrieben von: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>SidSettingsDialog</name>
    <message>
        <location filename="../sidsettingsdialog.ui" line="14"/>
        <source>SID Plugin Settings</source>
        <translation>Einstellungen SID-Modul</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="55"/>
        <source>Fast resampling</source>
        <translation>Schnelle Abtastratenkonvertierung</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="62"/>
        <source>Sample rate:</source>
        <translation>Abtastrate:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="69"/>
        <source>HVSC database file:</source>
        <translation>HVSC-Datenbankdatei:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="76"/>
        <source>Resampling method:</source>
        <translation>Abtastratenkonvertierungsverfahren:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="93"/>
        <source>Defaults song length, sec:</source>
        <translation>Standardeinstellungen Liedlänge, Sek.:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="100"/>
        <source>Enable HVSC song length database</source>
        <translation>HVSC-Liedlängendatenbank aktivieren</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="107"/>
        <source>Emulation:</source>
        <translation>Emulation:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="41"/>
        <source>44100 Hz</source>
        <translation>44100 Hz</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="42"/>
        <source>48000 Hz</source>
        <translation>48000 Hz</translation>
    </message>
</context>
</TS>
