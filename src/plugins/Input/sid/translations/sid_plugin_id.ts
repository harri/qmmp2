<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>DecoderSIDFactory</name>
    <message>
        <location filename="../decodersidfactory.cpp" line="57"/>
        <source>SID Plugin</source>
        <translation>Plugin SID</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="59"/>
        <source>SID Files</source>
        <translation>File SID</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="112"/>
        <source>About SID Audio Plugin</source>
        <translation>Tentang Plugin Audio SID</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="113"/>
        <source>Qmmp SID Audio Plugin</source>
        <translation>Plugin Audio SID Qmmp</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="114"/>
        <source>This plugin plays Commodore 64 music files using libsidplayfp library</source>
        <translation>Plugin ini memutar file-file musik Commodore 64 menggunakan pustaka libsidplayfp</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="115"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>SidSettingsDialog</name>
    <message>
        <location filename="../sidsettingsdialog.ui" line="14"/>
        <source>SID Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin SID</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="55"/>
        <source>Fast resampling</source>
        <translation type="unfinished">Resampling cepat</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="62"/>
        <source>Sample rate:</source>
        <translation type="unfinished">Sample rate:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="69"/>
        <source>HVSC database file:</source>
        <translation type="unfinished">File database HVSC:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="76"/>
        <source>Resampling method:</source>
        <translation type="unfinished">Metode resampling:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="93"/>
        <source>Defaults song length, sec:</source>
        <translation type="unfinished">Panjang lagu baku, det:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="100"/>
        <source>Enable HVSC song length database</source>
        <translation type="unfinished">Aktifkan database panjang lagu HVSC</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="107"/>
        <source>Emulation:</source>
        <translation type="unfinished">Emulasi:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="41"/>
        <source>44100 Hz</source>
        <translation type="unfinished">44100 Hz</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="42"/>
        <source>48000 Hz</source>
        <translation type="unfinished">48000 Hz</translation>
    </message>
</context>
</TS>
