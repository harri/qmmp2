<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk">
<context>
    <name>CueSettingsDialog</name>
    <message>
        <location filename="../cuesettingsdialog.ui" line="14"/>
        <source>CUE Plugin Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="29"/>
        <source>Common settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="35"/>
        <source>Load incorrect cue sheets if possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="45"/>
        <source>CUE encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="51"/>
        <source>Automatic charset detection</source>
        <translation type="unfinished">Automatická voľba znakovej sady</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="61"/>
        <source>Language:</source>
        <translation type="unfinished">Jazyk:</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="81"/>
        <source>Default encoding:</source>
        <translation type="unfinished">Východzie kódovanie:</translation>
    </message>
</context>
<context>
    <name>DecoderCUEFactory</name>
    <message>
        <location filename="../decodercuefactory.cpp" line="38"/>
        <source>CUE Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="41"/>
        <source>CUE Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="81"/>
        <source>About CUE Audio Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="82"/>
        <source>Qmmp CUE Audio Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="83"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
