<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>DecoderWildMidiFactory</name>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="49"/>
        <source>WildMidi Plugin</source>
        <translation>Plugin WildMidi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="54"/>
        <source>Midi Files</source>
        <translation>File Midi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="105"/>
        <source>About WildMidi Audio Plugin</source>
        <translation>Tentang Plugin Audio WildMidi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="106"/>
        <source>Qmmp WildMidi Audio Plugin</source>
        <translation>Plugin Audio WildMidi Qmmp</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="107"/>
        <source>This plugin uses WildMidi library to play midi files</source>
        <translation>Plugin ini menggunakan pustaka WildMidi untuk memainkan file midi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="108"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>WildMidiSettingsDialog</name>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="14"/>
        <source>WildMidi Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin WildMidi</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="29"/>
        <source>Instrument configuration: </source>
        <translation type="unfinished">Konfigurasi instrumen: </translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="43"/>
        <source>Sample rate:</source>
        <translation type="unfinished">Sample rate:</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="50"/>
        <source>Enhanced resampling</source>
        <translation type="unfinished">Resampling canggih</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="57"/>
        <source>Reverberation</source>
        <translation type="unfinished">Gema</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.cpp" line="39"/>
        <source>44100 Hz</source>
        <translation type="unfinished">44100 Hz</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.cpp" line="40"/>
        <source>48000 Hz</source>
        <translation type="unfinished">48000 Hz</translation>
    </message>
</context>
</TS>
