<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>DecoderWildMidiFactory</name>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="49"/>
        <source>WildMidi Plugin</source>
        <translation>WildMidi Eklentisi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="54"/>
        <source>Midi Files</source>
        <translation>Midi Dosyaları</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="105"/>
        <source>About WildMidi Audio Plugin</source>
        <translation>WildMidi Ses Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="106"/>
        <source>Qmmp WildMidi Audio Plugin</source>
        <translation>Qmmp WildMidi  Ses Eklentisi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="107"/>
        <source>This plugin uses WildMidi library to play midi files</source>
        <translation>Bu eklenti midi dosyalarını oynatmada WildMidi kitaplığını kullanır</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="108"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>WildMidiSettingsDialog</name>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="14"/>
        <source>WildMidi Plugin Settings</source>
        <translation>WildMidi Eklenti Ayarları</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="29"/>
        <source>Instrument configuration: </source>
        <translation>Müzik aleti yapılandırması:</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="43"/>
        <source>Sample rate:</source>
        <translation>Örnekleme oranı:</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="50"/>
        <source>Enhanced resampling</source>
        <translation>Gelişmiş yeniden örnekleme</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="57"/>
        <source>Reverberation</source>
        <translation>Yankılanma</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.cpp" line="39"/>
        <source>44100 Hz</source>
        <translation>44100 Hz</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.cpp" line="40"/>
        <source>48000 Hz</source>
        <translation>48000 Hz</translation>
    </message>
</context>
</TS>
