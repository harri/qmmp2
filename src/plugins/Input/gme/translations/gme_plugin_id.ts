<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>DecoderGmeFactory</name>
    <message>
        <location filename="../decodergmefactory.cpp" line="37"/>
        <source>GME Plugin</source>
        <translation>Plugin GME</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="42"/>
        <source>Game Music Files</source>
        <translation>File Musik Game</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="101"/>
        <source>About GME Audio Plugin</source>
        <translation>Tentang Plugin Audio GME</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="102"/>
        <source>Qmmp GME Audio Plugin</source>
        <translation>Plugin Audio GME Qmmp</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="103"/>
        <source>This plugin uses Game_Music_Emu library to play game music files</source>
        <translation>Plugin ini menggunakan pustaka Game_Music_Emu untuk memainkan file musik game</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="104"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>GmeSettingsDialog</name>
    <message>
        <location filename="../gmesettingsdialog.ui" line="14"/>
        <source>GME Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin GME</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="34"/>
        <source>Fadeout length:</source>
        <translation type="unfinished">Panjang fadeout:</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="44"/>
        <source>ms</source>
        <translation type="unfinished">md</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="57"/>
        <source>Enable fadeout</source>
        <translation type="unfinished">Aktifkan fadeout</translation>
    </message>
</context>
</TS>
