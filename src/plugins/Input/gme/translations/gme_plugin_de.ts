<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DecoderGmeFactory</name>
    <message>
        <location filename="../decodergmefactory.cpp" line="37"/>
        <source>GME Plugin</source>
        <translation>GME-Modul</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="42"/>
        <source>Game Music Files</source>
        <translation>Musikdateien aus Spielen (Game Music)</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="101"/>
        <source>About GME Audio Plugin</source>
        <translation>Über GME-Audiomodul</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="102"/>
        <source>Qmmp GME Audio Plugin</source>
        <translation>Qmmp GME-Audiomodul</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="103"/>
        <source>This plugin uses Game_Music_Emu library to play game music files</source>
        <translation>Dieses Modul verwendet die Bibliothek „Game_Music_Emu“, um Musikdateien aus Spielen abzuspielen</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="104"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Geschrieben von: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>GmeSettingsDialog</name>
    <message>
        <location filename="../gmesettingsdialog.ui" line="14"/>
        <source>GME Plugin Settings</source>
        <translation>Einstellungen GME-Modul</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="34"/>
        <source>Fadeout length:</source>
        <translation>Ausblendlänge:</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="44"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="57"/>
        <source>Enable fadeout</source>
        <translation>Ausblendung aktivieren</translation>
    </message>
</context>
</TS>
