<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>DecoderGmeFactory</name>
    <message>
        <location filename="../decodergmefactory.cpp" line="37"/>
        <source>GME Plugin</source>
        <translation>תוסף GME</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="42"/>
        <source>Game Music Files</source>
        <translation>קבצי מוזיקה של משחק</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="101"/>
        <source>About GME Audio Plugin</source>
        <translation>אודות תוסף שמע GME</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="102"/>
        <source>Qmmp GME Audio Plugin</source>
        <translation>תוסף שמע GME של Qmmp</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="103"/>
        <source>This plugin uses Game_Music_Emu library to play game music files</source>
        <translation>תוסף זה משתמש בספריית Game_Music_Emu לצורך ניגון קבצי מוזיקה של משחק וידאו</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="104"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GmeSettingsDialog</name>
    <message>
        <location filename="../gmesettingsdialog.ui" line="14"/>
        <source>GME Plugin Settings</source>
        <translation type="unfinished">הגדרות תוסף GME</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="34"/>
        <source>Fadeout length:</source>
        <translation type="unfinished">אריכות Fadeout:</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="44"/>
        <source>ms</source>
        <translation type="unfinished">מ״ש</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="57"/>
        <source>Enable fadeout</source>
        <translation type="unfinished">אפשר fadeout</translation>
    </message>
</context>
</TS>
