<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>DecoderGmeFactory</name>
    <message>
        <location filename="../decodergmefactory.cpp" line="37"/>
        <source>GME Plugin</source>
        <translation>GME Eklentisi</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="42"/>
        <source>Game Music Files</source>
        <translation>Oyun Müzik Dosyaları</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="101"/>
        <source>About GME Audio Plugin</source>
        <translation>GME Ses Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="102"/>
        <source>Qmmp GME Audio Plugin</source>
        <translation>Qmmp GME Ses Eklentisi</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="103"/>
        <source>This plugin uses Game_Music_Emu library to play game music files</source>
        <translation>Bu eklenti midi dosyalarını oynatmada Game_Music_Emu kitaplığını kullanır</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="104"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>GmeSettingsDialog</name>
    <message>
        <location filename="../gmesettingsdialog.ui" line="14"/>
        <source>GME Plugin Settings</source>
        <translation>GME Eklenti Ayarları</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="34"/>
        <source>Fadeout length:</source>
        <translation>Sönme uzunluğu:</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="44"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="57"/>
        <source>Enable fadeout</source>
        <translation>Sönmeyi etkinleştir</translation>
    </message>
</context>
</TS>
