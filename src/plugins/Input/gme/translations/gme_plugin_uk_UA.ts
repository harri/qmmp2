<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DecoderGmeFactory</name>
    <message>
        <location filename="../decodergmefactory.cpp" line="37"/>
        <source>GME Plugin</source>
        <translation>Втулок GME</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="42"/>
        <source>Game Music Files</source>
        <translation>Програми звукогенераторів</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="101"/>
        <source>About GME Audio Plugin</source>
        <translation>Про авдіовтулок GME</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="102"/>
        <source>Qmmp GME Audio Plugin</source>
        <translation>Авдіодіовтулок GME для Qmmp</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="103"/>
        <source>This plugin uses Game_Music_Emu library to play game music files</source>
        <translation>Для відтворення використовується бібліотека Game_Music_Emu</translation>
    </message>
    <message>
        <location filename="../decodergmefactory.cpp" line="104"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>GmeSettingsDialog</name>
    <message>
        <location filename="../gmesettingsdialog.ui" line="14"/>
        <source>GME Plugin Settings</source>
        <translation>Налаштування втулка GME</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="34"/>
        <source>Fadeout length:</source>
        <translation>Тривалість затухання:</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="44"/>
        <source>ms</source>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../gmesettingsdialog.ui" line="57"/>
        <source>Enable fadeout</source>
        <translation>Увімкнути затухання</translation>
    </message>
</context>
</TS>
