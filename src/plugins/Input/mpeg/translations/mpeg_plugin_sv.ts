<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>DecoderMpegFactory</name>
    <message>
        <location filename="../decodermpegfactory.cpp" line="178"/>
        <source>MPEG Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="181"/>
        <source>MPEG Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="417"/>
        <source>About MPEG Audio Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="418"/>
        <source>MPEG 1.0/2.0/2.5 layer 1/2/3 audio decoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="419"/>
        <source>Compiled against:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="428"/>
        <source>mpg123, API version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="432"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="433"/>
        <source>Source code based on mq3 and madplay projects</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MPEGMetaDataModel</name>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="70"/>
        <location filename="../mpegmetadatamodel.cpp" line="73"/>
        <location filename="../mpegmetadatamodel.cpp" line="76"/>
        <location filename="../mpegmetadatamodel.cpp" line="79"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="82"/>
        <source>Protection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="83"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="84"/>
        <source>Original</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MpegSettingsDialog</name>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="14"/>
        <source>MPEG Plugin Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="35"/>
        <source>Decoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="41"/>
        <source>MAD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="48"/>
        <source>MPG123</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="71"/>
        <source>Enable CRC checking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="87"/>
        <source>Tag Priority</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="99"/>
        <source>First:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="119"/>
        <location filename="../mpegsettingsdialog.ui" line="168"/>
        <location filename="../mpegsettingsdialog.ui" line="217"/>
        <source>ID3v1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="124"/>
        <location filename="../mpegsettingsdialog.ui" line="173"/>
        <location filename="../mpegsettingsdialog.ui" line="222"/>
        <source>ID3v2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="129"/>
        <location filename="../mpegsettingsdialog.ui" line="178"/>
        <location filename="../mpegsettingsdialog.ui" line="227"/>
        <source>APE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="134"/>
        <location filename="../mpegsettingsdialog.ui" line="183"/>
        <location filename="../mpegsettingsdialog.ui" line="232"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="148"/>
        <source>Second:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="197"/>
        <source>Third:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="253"/>
        <source>Merge selected tag types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="272"/>
        <source>Encodings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="294"/>
        <source>ID3v2 encoding:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="333"/>
        <source>ID3v1 encoding:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="343"/>
        <source>Try to detect encoding</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
