<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>DecoderFLACFactory</name>
    <message>
        <location filename="../decoderflacfactory.cpp" line="53"/>
        <source>FLAC Plugin</source>
        <translation>FLAC Plugin</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="55"/>
        <source>FLAC Files</source>
        <translation>FLAC-tiedostot</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="217"/>
        <source>About FLAC Audio Plugin</source>
        <translation>Tietoja: FLAC Audio Plugin</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="218"/>
        <source>Qmmp FLAC Audio Plugin</source>
        <translation>Qmmp FLAC Audio Plugin</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="219"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Kirjoittanut: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
