<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_BA">
<context>
    <name>AboutQSUIDialog</name>
    <message>
        <location filename="../forms/aboutqsuidialog.ui" line="14"/>
        <source>About QSUI</source>
        <translation>О КСУИ-ију</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="42"/>
        <source>Qmmp Simple User Interface (QSUI)</source>
        <translation>Кумп једноставно корисничко сучеље (КСУИ)</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="43"/>
        <source>Qmmp version: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Издање Кумпа: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="45"/>
        <source>Developers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="46"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="48"/>
        <source>Translators:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="44"/>
        <source>Simple user interface based on standard widgets set.</source>
        <translation>Једноставно корисничко сучеље базирано на основном сету виџета.</translation>
    </message>
</context>
<context>
    <name>FileSystemBrowser</name>
    <message>
        <location filename="../filesystembrowser.cpp" line="100"/>
        <source>Add to Playlist</source>
        <translation>Додај на листу</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="101"/>
        <source>Replace Playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="102"/>
        <source>Change Directory</source>
        <translation>Промијени фасциклу</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="105"/>
        <source>Tree View Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="107"/>
        <source>Quick Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="116"/>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="110"/>
        <source>By Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="111"/>
        <source>By Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="112"/>
        <source>By Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="113"/>
        <source>By Date</source>
        <translation type="unfinished">по датуму</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="219"/>
        <source>Select Directory</source>
        <translation>Избор фасцикле</translation>
    </message>
</context>
<context>
    <name>QSUISettings</name>
    <message>
        <location filename="../forms/qsuisettings.ui" line="24"/>
        <source>View</source>
        <translation>Приказ</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="36"/>
        <source>Hide on close</source>
        <translation>Сакриј по затварању</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="43"/>
        <source>Start hidden</source>
        <translation>Покрени скривено</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="210"/>
        <source>Visualization Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="216"/>
        <source>Color #1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="283"/>
        <source>Color #2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="347"/>
        <source>Color #3:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="436"/>
        <source>Reset colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="703"/>
        <source>Override group colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="806"/>
        <source>Override current track colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="816"/>
        <source>Current track text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="915"/>
        <source>Waveform Seekbar Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="921"/>
        <source>Progress bar:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1010"/>
        <source>RMS:</source>
        <extracomment>Root mean square</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1055"/>
        <source>Waveform:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1096"/>
        <source>Fonts</source>
        <translation>Фонтови</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1102"/>
        <source>Use system fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1176"/>
        <source>Playlist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="62"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="30"/>
        <source>Main Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="52"/>
        <source>Window title format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="74"/>
        <source>Song Display</source>
        <translation>Приказ нумере</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="80"/>
        <source>Show protocol</source>
        <translation>Протокол</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="148"/>
        <source>Show song numbers</source>
        <translation>Број нумере</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="155"/>
        <source>Show song lengths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="165"/>
        <source>Align song numbers</source>
        <translation>Уравнај бројеве нумера</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="87"/>
        <source>Show anchor</source>
        <translation>Сидро</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="101"/>
        <source>Show popup information</source>
        <translation>Облачић с подацима</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="113"/>
        <source>Edit template</source>
        <translation>Уреди шаблон</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1243"/>
        <source>Reset fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1192"/>
        <source>Column headers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1258"/>
        <source>Tab names:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1339"/>
        <source>Miscellaneous</source>
        <translation>Разно</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="94"/>
        <source>Show splitters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="142"/>
        <source>Single Column Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="189"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="251"/>
        <source>Peaks:</source>
        <translation>Врхови:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="315"/>
        <location filename="../forms/qsuisettings.ui" line="1003"/>
        <source>Background:</source>
        <translation>Позадина:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="448"/>
        <source>Playlist Colors</source>
        <translation>Боје листи нумера</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="470"/>
        <source>Background #1:</source>
        <translation>Позадина #1:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="601"/>
        <source>Normal text:</source>
        <translation>Обични текст:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="569"/>
        <source>Background #2:</source>
        <translation>Позадина #2:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="633"/>
        <source>Highlighted background:</source>
        <translation>Означена позадина:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="665"/>
        <source>Highlighted text:</source>
        <translation>Означени текст:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="867"/>
        <source>Current track background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1268"/>
        <source>Groups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1275"/>
        <source>Extra group row:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1372"/>
        <source>Tab position:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1409"/>
        <source>Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1417"/>
        <source>Icon size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1442"/>
        <source>Customize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="454"/>
        <source>Use system colors</source>
        <translation>Системске боје</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="761"/>
        <source>Group background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="713"/>
        <source>Group text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="515"/>
        <source>Splitter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1345"/>
        <source>Tabs</source>
        <translation>Језичци</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1351"/>
        <source>Show close buttons</source>
        <translation>Дугмад за затварање</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1358"/>
        <source>Show tab list menu</source>
        <translation>Мени списка језичака</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1365"/>
        <source>Show &apos;New Playlist&apos; button</source>
        <translation>Дугме „Нова листа нумера“</translation>
    </message>
</context>
<context>
    <name>QSUIVisualization</name>
    <message>
        <location filename="../qsuivisualization.cpp" line="125"/>
        <source>Cover</source>
        <translation type="unfinished">Омот</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="128"/>
        <source>Visualization Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="131"/>
        <source>Analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="132"/>
        <source>Scope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="139"/>
        <source>Analyzer Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="141"/>
        <source>Cells</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="142"/>
        <source>Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="150"/>
        <source>Peaks</source>
        <translation type="unfinished">Врхови</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="153"/>
        <source>Refresh Rate</source>
        <translation type="unfinished">Фреквенција</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="156"/>
        <source>50 fps</source>
        <translation type="unfinished">50 fps</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="157"/>
        <source>25 fps</source>
        <translation type="unfinished">25 fps</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="158"/>
        <source>10 fps</source>
        <translation type="unfinished">10 fps</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="159"/>
        <source>5 fps</source>
        <translation type="unfinished">5 fps</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="166"/>
        <source>Analyzer Falloff</source>
        <translation type="unfinished">Падање анализатора</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="169"/>
        <location filename="../qsuivisualization.cpp" line="183"/>
        <source>Slowest</source>
        <translation type="unfinished">Најспорије</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="170"/>
        <location filename="../qsuivisualization.cpp" line="184"/>
        <source>Slow</source>
        <translation type="unfinished">Споро</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="171"/>
        <location filename="../qsuivisualization.cpp" line="185"/>
        <source>Medium</source>
        <translation type="unfinished">Умјерено</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="172"/>
        <location filename="../qsuivisualization.cpp" line="186"/>
        <source>Fast</source>
        <translation type="unfinished">Брзо</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="173"/>
        <location filename="../qsuivisualization.cpp" line="187"/>
        <source>Fastest</source>
        <translation type="unfinished">Најбрже</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="180"/>
        <source>Peaks Falloff</source>
        <translation type="unfinished">Падање врхова</translation>
    </message>
</context>
<context>
    <name>QSUiActionManager</name>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="43"/>
        <source>&amp;Play</source>
        <translation type="unfinished">&amp;Пусти</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="43"/>
        <source>X</source>
        <translation type="unfinished">X</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="44"/>
        <source>&amp;Pause</source>
        <translation type="unfinished">П&amp;аузирај</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="44"/>
        <source>C</source>
        <translation type="unfinished">C</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="45"/>
        <source>&amp;Stop</source>
        <translation type="unfinished">&amp;Заустави</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="45"/>
        <source>V</source>
        <translation type="unfinished">V</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="46"/>
        <source>&amp;Previous</source>
        <translation type="unfinished">П&amp;ретходна</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="46"/>
        <source>Z</source>
        <translation type="unfinished">Z</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="47"/>
        <source>&amp;Next</source>
        <translation type="unfinished">&amp;Сљедећа</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="47"/>
        <source>B</source>
        <translation type="unfinished">B</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="48"/>
        <source>&amp;Play/Pause</source>
        <translation type="unfinished">П&amp;усти/паузирај</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="48"/>
        <source>Space</source>
        <translation type="unfinished">Размак</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="49"/>
        <source>&amp;Jump to Track</source>
        <translation type="unfinished">С&amp;кочи на нумеру</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="49"/>
        <source>J</source>
        <translation type="unfinished">J</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="50"/>
        <source>&amp;Play Files</source>
        <translation type="unfinished">Пусти &amp;фајлове</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="50"/>
        <source>E</source>
        <translation type="unfinished">E</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="51"/>
        <source>&amp;Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="52"/>
        <source>&amp;Repeat Playlist</source>
        <translation type="unfinished">Понављај &amp;листу</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="52"/>
        <source>R</source>
        <translation type="unfinished">R</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="53"/>
        <source>&amp;Repeat Track</source>
        <translation type="unfinished">Понављај &amp;нумеру</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="53"/>
        <source>Ctrl+R</source>
        <translation type="unfinished">Ctrl+R</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="54"/>
        <source>&amp;Shuffle</source>
        <translation type="unfinished">На&amp;сумично</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="54"/>
        <source>S</source>
        <translation type="unfinished">S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="55"/>
        <source>&amp;No Playlist Advance</source>
        <translation type="unfinished">&amp;Без напредовања листе</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="55"/>
        <source>Ctrl+N</source>
        <translation type="unfinished">Ctrl+N</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="56"/>
        <source>&amp;Transit between playlists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="57"/>
        <source>&amp;Stop After Selected</source>
        <translation type="unfinished">&amp;Заустави након избора</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="57"/>
        <source>Ctrl+S</source>
        <translation type="unfinished">Ctrl+S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="58"/>
        <source>&amp;Clear Queue</source>
        <translation type="unfinished">&amp;Очисти ред</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="58"/>
        <source>Alt+Q</source>
        <translation type="unfinished">Alt+Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="60"/>
        <source>Always on Top</source>
        <translation type="unfinished">Увијек на врху</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="61"/>
        <source>Put on All Workspaces</source>
        <translation type="unfinished">На свим површима</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="67"/>
        <source>Show Tabs</source>
        <translation type="unfinished">Прикажи језичке</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="68"/>
        <source>Block Floating Panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="69"/>
        <source>Block Toolbars</source>
        <translation type="unfinished">Закључај траке алатки</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="71"/>
        <source>Volume &amp;+</source>
        <translation type="unfinished">Јачина &amp;+</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="71"/>
        <source>0</source>
        <translation type="unfinished">0</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="72"/>
        <source>Volume &amp;-</source>
        <translation type="unfinished">Јачина &amp;-</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="72"/>
        <source>9</source>
        <translation type="unfinished">9</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="73"/>
        <source>&amp;Mute</source>
        <translation type="unfinished">Утиша&amp;ј</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="73"/>
        <source>M</source>
        <translation type="unfinished">M</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="75"/>
        <source>&amp;Add File</source>
        <translation type="unfinished">Додај &amp;фајл</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="75"/>
        <source>F</source>
        <translation type="unfinished">F</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="76"/>
        <source>&amp;Add Directory</source>
        <translation type="unfinished">Додај фас&amp;циклу</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="76"/>
        <source>D</source>
        <translation type="unfinished">D</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="77"/>
        <source>&amp;Add Url</source>
        <translation type="unfinished">Додај Ур&amp;л</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="77"/>
        <source>U</source>
        <translation type="unfinished">U</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="78"/>
        <source>&amp;Remove Selected</source>
        <translation type="unfinished">Уклони &amp;избор</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="78"/>
        <source>Del</source>
        <translation type="unfinished">Del</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="79"/>
        <source>&amp;Remove All</source>
        <translation type="unfinished">Уклони &amp;све</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="80"/>
        <source>&amp;Remove Unselected</source>
        <translation type="unfinished">Уклони &amp;неизабрано</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="81"/>
        <source>Remove unavailable files</source>
        <translation type="unfinished">Уклони недоступне фајлове</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="82"/>
        <source>Remove duplicates</source>
        <translation type="unfinished">Уклони дупликате</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="83"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="84"/>
        <source>&amp;Queue Toggle</source>
        <translation type="unfinished">Стави у &amp;ред</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="84"/>
        <source>Q</source>
        <translation type="unfinished">Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="85"/>
        <source>Invert Selection</source>
        <translation type="unfinished">Обрни избор</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="86"/>
        <source>&amp;Select None</source>
        <translation type="unfinished">&amp;Очисти избор</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="87"/>
        <source>&amp;Select All</source>
        <translation type="unfinished">Изабери &amp;све</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="87"/>
        <source>Ctrl+A</source>
        <translation type="unfinished">Ctrl+A</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="88"/>
        <source>&amp;View Track Details</source>
        <translation type="unfinished">&amp;Види детаље нумере</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="88"/>
        <source>Alt+I</source>
        <translation type="unfinished">Alt+I</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="89"/>
        <source>&amp;New List</source>
        <translation type="unfinished">&amp;Нова листа нумера</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="89"/>
        <source>Ctrl+T</source>
        <translation type="unfinished">Ctrl+T</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="90"/>
        <source>&amp;Delete List</source>
        <translation type="unfinished">&amp;Обриши листу нумера</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="90"/>
        <source>Ctrl+W</source>
        <translation type="unfinished">Ctrl+W</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="91"/>
        <source>&amp;Load List</source>
        <translation type="unfinished">&amp;Учитај листу нумера</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="91"/>
        <source>O</source>
        <translation type="unfinished">O</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="92"/>
        <source>&amp;Save List</source>
        <translation type="unfinished">&amp;Сачувај листу нумера</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="92"/>
        <source>Shift+S</source>
        <translation type="unfinished">Shift+S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="93"/>
        <source>&amp;Rename List</source>
        <translation type="unfinished">Пре&amp;именуј листу нумера</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="93"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="94"/>
        <source>&amp;Select Next Playlist</source>
        <translation type="unfinished">Изабери сље&amp;дећу</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="94"/>
        <source>Ctrl+PgDown</source>
        <translation type="unfinished">Ctrl+PgDown</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="95"/>
        <source>&amp;Select Previous Playlist</source>
        <translation type="unfinished">Изабери &amp;претходну</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="95"/>
        <source>Ctrl+PgUp</source>
        <translation type="unfinished">Ctrl+PgUp</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="96"/>
        <source>&amp;Group Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="96"/>
        <source>Ctrl+G</source>
        <translation type="unfinished">Ctrl+G</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="97"/>
        <source>&amp;Show Column Headers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="97"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="99"/>
        <source>&amp;Equalizer</source>
        <translation type="unfinished">&amp;Еквилајзер</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="99"/>
        <source>Ctrl+E</source>
        <translation type="unfinished">Ctrl+E</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="100"/>
        <source>&amp;Settings</source>
        <translation type="unfinished">По&amp;дешавање</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="100"/>
        <source>Ctrl+P</source>
        <translation type="unfinished">Ctrl+P</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="101"/>
        <source>Application Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="102"/>
        <source>&amp;About Ui</source>
        <translation type="unfinished">О &amp;сучељу</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="103"/>
        <source>&amp;About</source>
        <translation type="unfinished">&amp;О програму</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="104"/>
        <source>&amp;About Qt</source>
        <translation type="unfinished">О &amp;Куту</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="105"/>
        <source>&amp;Exit</source>
        <translation type="unfinished">&amp;Напусти</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="105"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished">Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="310"/>
        <source>Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSUiCoverWidget</name>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="32"/>
        <source>&amp;Save As...</source>
        <translation type="unfinished">&amp;Сачувај као...</translation>
    </message>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="65"/>
        <source>Save Cover As</source>
        <translation type="unfinished">Сачувај омот</translation>
    </message>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="67"/>
        <source>Images</source>
        <translation type="unfinished">Слике</translation>
    </message>
</context>
<context>
    <name>QSUiEqualizer</name>
    <message>
        <location filename="../qsuiequalizer.cpp" line="39"/>
        <source>Equalizer</source>
        <translation type="unfinished">Еквилајзер</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="48"/>
        <source>Enable equalizer</source>
        <translation type="unfinished">Омоући еквилајзер</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="54"/>
        <source>Preset:</source>
        <translation type="unfinished">Поставка:</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="62"/>
        <source>Save</source>
        <translation type="unfinished">Сачувај</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="66"/>
        <source>Delete</source>
        <translation type="unfinished">Обриши</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="70"/>
        <source>Reset</source>
        <translation type="unfinished">Ресетуј</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="83"/>
        <source>Preamp</source>
        <translation type="unfinished">Претпојачање</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="100"/>
        <location filename="../qsuiequalizer.cpp" line="195"/>
        <source>%1dB</source>
        <translation type="unfinished">%1dB</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="102"/>
        <location filename="../qsuiequalizer.cpp" line="193"/>
        <source>+%1dB</source>
        <translation type="unfinished">+%1dB</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="148"/>
        <source>preset</source>
        <translation type="unfinished">поставка</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="218"/>
        <source>Overwrite Request</source>
        <translation type="unfinished">Захтјев за пребрисавање</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="219"/>
        <source>Preset &apos;%1&apos; already exists. Overwrite?</source>
        <translation type="unfinished">Поставка „%1“ већ постоји. Желите ли да је пребришете?</translation>
    </message>
</context>
<context>
    <name>QSUiFactory</name>
    <message>
        <location filename="../qsuifactory.cpp" line="32"/>
        <source>Simple User Interface</source>
        <translation type="unfinished">Једноставно корисничко сучеље</translation>
    </message>
</context>
<context>
    <name>QSUiHotkeyEditor</name>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="33"/>
        <source>Change shortcut...</source>
        <translation type="unfinished">Измијени пречицу...</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="40"/>
        <source>Reset</source>
        <translation type="unfinished">Ресетуј</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="54"/>
        <source>Action</source>
        <translation type="unfinished">радња</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="59"/>
        <source>Shortcut</source>
        <translation type="unfinished">пречица</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="56"/>
        <source>Reset Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="57"/>
        <source>Do you want to restore default shortcuts?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="69"/>
        <source>Playback</source>
        <translation type="unfinished">Контроле</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="75"/>
        <source>View</source>
        <translation type="unfinished">Приказ</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="81"/>
        <source>Volume</source>
        <translation type="unfinished">Јачина</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="87"/>
        <source>Playlist</source>
        <translation type="unfinished">Листа нумера</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="93"/>
        <source>Misc</source>
        <translation type="unfinished">Разно</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="101"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSUiMainWindow</name>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="14"/>
        <location filename="../qsuimainwindow.cpp" line="917"/>
        <source>Qmmp</source>
        <translation type="unfinished">Кумп</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="35"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Фајл</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="40"/>
        <source>&amp;Tools</source>
        <translation type="unfinished">&amp;Алатке</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="45"/>
        <source>&amp;Help</source>
        <translation type="unfinished">По&amp;моћ</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="50"/>
        <source>&amp;Edit</source>
        <translation type="unfinished">&amp;Уређивање</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="55"/>
        <source>&amp;Playback</source>
        <translation type="unfinished">&amp;Контроле</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="60"/>
        <source>&amp;View</source>
        <translation type="unfinished">&amp;Приказ</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="84"/>
        <location filename="../forms/qsuimainwindow.ui" line="249"/>
        <source>Visualization</source>
        <translation type="unfinished">Визуелизација</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="99"/>
        <source>Files</source>
        <translation type="unfinished">Фајлови</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="114"/>
        <source>Cover</source>
        <translation type="unfinished">Омот</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="123"/>
        <source>Playlists</source>
        <translation type="unfinished">Листе нумера</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="135"/>
        <source>Waveform Seek Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="149"/>
        <source>Previous</source>
        <translation type="unfinished">Претходна</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="159"/>
        <source>Play</source>
        <translation type="unfinished">Пусти</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="169"/>
        <source>Pause</source>
        <translation type="unfinished">Паузирај</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="179"/>
        <source>Next</source>
        <translation type="unfinished">Сљедећа</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="189"/>
        <source>Stop</source>
        <translation type="unfinished">Заустави</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="194"/>
        <source>&amp;Add File</source>
        <translation type="unfinished">Додај &amp;фајл</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="199"/>
        <source>&amp;Remove All</source>
        <translation type="unfinished">Уклони &amp;све</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="204"/>
        <source>New Playlist</source>
        <translation type="unfinished">Нова листа</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="209"/>
        <source>Remove Playlist</source>
        <translation type="unfinished">Уклони листу</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="214"/>
        <source>&amp;Add Directory</source>
        <translation type="unfinished">Додај фас&amp;циклу</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="219"/>
        <source>&amp;Exit</source>
        <translation type="unfinished">&amp;Напусти</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="224"/>
        <source>About</source>
        <translation type="unfinished">О програму</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="229"/>
        <source>About Qt</source>
        <translation type="unfinished">О Куту</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="234"/>
        <source>&amp;Select All</source>
        <translation type="unfinished">Изабери &amp;све</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="239"/>
        <source>&amp;Remove Selected</source>
        <translation type="unfinished">Уклони &amp;избор</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="244"/>
        <source>&amp;Remove Unselected</source>
        <translation type="unfinished">Уклони &amp;неизабрано</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="254"/>
        <source>Settings</source>
        <translation type="unfinished">Подешавања</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="259"/>
        <location filename="../qsuimainwindow.cpp" line="295"/>
        <source>Rename Playlist</source>
        <translation type="unfinished">Преименуј листу нумера</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="86"/>
        <source>&amp;Copy Selection To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="295"/>
        <source>Playlist name:</source>
        <translation type="unfinished">Име листе нумера:</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="335"/>
        <source>Appearance</source>
        <translation type="unfinished">Изглед</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="336"/>
        <source>Shortcuts</source>
        <translation type="unfinished">Пречице</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="415"/>
        <source>Menu Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="434"/>
        <source>Add new playlist</source>
        <translation type="unfinished">Додај нову листу</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="440"/>
        <source>Show all tabs</source>
        <translation type="unfinished">Прикажи све језичке</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="467"/>
        <source>Ctrl+0</source>
        <translation type="unfinished">Ctrl+0</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="473"/>
        <source>P</source>
        <translation type="unfinished">P</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="478"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="480"/>
        <source>Volume</source>
        <translation type="unfinished">Јачина</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="482"/>
        <source>Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="485"/>
        <source>Quick Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="543"/>
        <source>Edit Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="545"/>
        <source>Sort List</source>
        <translation type="unfinished">Поређај</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="547"/>
        <location filename="../qsuimainwindow.cpp" line="563"/>
        <source>By Title</source>
        <translation type="unfinished">по наслову</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="548"/>
        <location filename="../qsuimainwindow.cpp" line="564"/>
        <source>By Album</source>
        <translation type="unfinished">по албуму</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="549"/>
        <location filename="../qsuimainwindow.cpp" line="565"/>
        <source>By Artist</source>
        <translation type="unfinished">по извођачу</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="550"/>
        <location filename="../qsuimainwindow.cpp" line="566"/>
        <source>By Album Artist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="551"/>
        <location filename="../qsuimainwindow.cpp" line="567"/>
        <source>By Filename</source>
        <translation type="unfinished">по имену фајла</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="552"/>
        <location filename="../qsuimainwindow.cpp" line="568"/>
        <source>By Path + Filename</source>
        <translation type="unfinished">по путањи и имену</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="553"/>
        <location filename="../qsuimainwindow.cpp" line="569"/>
        <source>By Date</source>
        <translation type="unfinished">по датуму</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="554"/>
        <location filename="../qsuimainwindow.cpp" line="570"/>
        <source>By Track Number</source>
        <translation type="unfinished">по броју нумере</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="555"/>
        <location filename="../qsuimainwindow.cpp" line="571"/>
        <source>By Disc Number</source>
        <translation type="unfinished">по броју диска</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="556"/>
        <location filename="../qsuimainwindow.cpp" line="572"/>
        <source>By File Creation Date</source>
        <translation type="unfinished">по датуму фајла</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="557"/>
        <location filename="../qsuimainwindow.cpp" line="573"/>
        <source>By File Modification Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="558"/>
        <source>By Group</source>
        <translation type="unfinished">по групи</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="561"/>
        <source>Sort Selection</source>
        <translation type="unfinished">Поређај избор</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="577"/>
        <source>Randomize List</source>
        <translation type="unfinished">Претумбај</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="579"/>
        <source>Reverse List</source>
        <translation type="unfinished">Обрни</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="620"/>
        <source>Actions</source>
        <translation type="unfinished">Радње</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="938"/>
        <source>&amp;New PlayList</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSUiPlayListBrowser</name>
    <message>
        <location filename="../qsuiplaylistbrowser.cpp" line="62"/>
        <source>Quick Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSUiPlayListHeader</name>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="54"/>
        <source>Add Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="55"/>
        <source>Edit Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="56"/>
        <source>Show Queue/Protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="58"/>
        <source>Auto-resize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="61"/>
        <source>Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="62"/>
        <source>Left</source>
        <comment>alignment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="63"/>
        <source>Right</source>
        <comment>alignment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="64"/>
        <source>Center</source>
        <comment>alignment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="74"/>
        <source>Remove Column</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSUiPopupSettings</name>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="14"/>
        <source>Popup Information Settings</source>
        <translation type="unfinished">Поставке облачића података</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="29"/>
        <source>Template</source>
        <translation type="unfinished">Шаблон</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="58"/>
        <source>Reset</source>
        <translation type="unfinished">Ресетуј</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="65"/>
        <source>Insert</source>
        <translation type="unfinished">Уметни</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="75"/>
        <source>Show cover</source>
        <translation type="unfinished">Прикажи омот</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="89"/>
        <source>Cover size:</source>
        <translation type="unfinished">Величина омота:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="115"/>
        <source>Transparency:</source>
        <translation type="unfinished">Прозирност:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="145"/>
        <source>Delay:</source>
        <translation type="unfinished">Застој:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="165"/>
        <source>ms</source>
        <translation type="unfinished">ms</translation>
    </message>
</context>
<context>
    <name>QSUiSettings</name>
    <message>
        <location filename="../qsuisettings.cpp" line="41"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="42"/>
        <source>16x16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="43"/>
        <source>22x22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="44"/>
        <source>32x32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="45"/>
        <source>48x48</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="46"/>
        <source>64x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="48"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="49"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="50"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="51"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSUiStatusBar</name>
    <message>
        <location filename="../qsuistatusbar.cpp" line="68"/>
        <source>tracks: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="69"/>
        <source>total time: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="87"/>
        <source>Playing</source>
        <translation type="unfinished">Пуштање</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="87"/>
        <source>Paused</source>
        <translation type="unfinished">Паузирано</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="102"/>
        <source>Buffering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="127"/>
        <source>Stopped</source>
        <translation type="unfinished">Заустављено</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="139"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="147"/>
        <source>Buffering: %1%</source>
        <translation type="unfinished">Баферујем: %1%</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="152"/>
        <source>%1 bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="154"/>
        <source>mono</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="156"/>
        <source>stereo</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../qsuistatusbar.cpp" line="158"/>
        <source>%n channels</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="159"/>
        <source>%1 Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="164"/>
        <source>%1 kbps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSUiWaveformSeekBar</name>
    <message>
        <location filename="../qsuiwaveformseekbar.cpp" line="335"/>
        <source>2 Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiwaveformseekbar.cpp" line="338"/>
        <source>RMS</source>
        <extracomment>Root mean square</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBarEditor</name>
    <message>
        <location filename="../forms/toolbareditor.ui" line="14"/>
        <source>ToolBar Editor</source>
        <translation>Уређивач траке алатки</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="62"/>
        <source>Reset</source>
        <translation>Ресетуј</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="199"/>
        <source>Toolbar:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="222"/>
        <source>&amp;Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="238"/>
        <source>Re&amp;name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="254"/>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="100"/>
        <location filename="../toolbareditor.cpp" line="198"/>
        <source>Separator</source>
        <translation>Раздвајач</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="248"/>
        <source>Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="250"/>
        <source>Toolbar %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="264"/>
        <source>Rename Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="264"/>
        <source>Toolbar name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../volumeslider.cpp" line="110"/>
        <source>%1: %2%</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
