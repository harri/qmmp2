<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>AboutQSUIDialog</name>
    <message>
        <location filename="../forms/aboutqsuidialog.ui" line="14"/>
        <source>About QSUI</source>
        <translation>Informazioni su QSUI</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="42"/>
        <source>Qmmp Simple User Interface (QSUI)</source>
        <translation>Interfaccia utente semplice per Qmmp (Qmmp Simple User Interface, QSUI)</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="43"/>
        <source>Qmmp version: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Versione Qmmp: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="45"/>
        <source>Developers:</source>
        <translation>Sviluppatori:</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="46"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="48"/>
        <source>Translators:</source>
        <translation>Traduttori</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="44"/>
        <source>Simple user interface based on standard widgets set.</source>
        <translation>Interfaccia utente semplice basata sull&apos;insieme di oggetti standard.</translation>
    </message>
</context>
<context>
    <name>FileSystemBrowser</name>
    <message>
        <location filename="../filesystembrowser.cpp" line="100"/>
        <source>Add to Playlist</source>
        <translation>Aggiungi alla scaletta</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="101"/>
        <source>Replace Playlist</source>
        <translation>Sostituisci scaletta</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="102"/>
        <source>Change Directory</source>
        <translation>Cambia cartella</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="105"/>
        <source>Tree View Mode</source>
        <translation>Modalità vista ad albero</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="107"/>
        <source>Quick Search</source>
        <translation>Ricerca rapida</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="116"/>
        <source>Sort</source>
        <translation>Ordina</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="110"/>
        <source>By Name</source>
        <translation>Per nome</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="111"/>
        <source>By Size</source>
        <translation>Per dimensione</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="112"/>
        <source>By Type</source>
        <translation>Per tipo</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="113"/>
        <source>By Date</source>
        <translation>Per data</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="219"/>
        <source>Select Directory</source>
        <translation>Seleziona cartella</translation>
    </message>
</context>
<context>
    <name>QSUISettings</name>
    <message>
        <location filename="../forms/qsuisettings.ui" line="24"/>
        <source>View</source>
        <translation>Visualizza</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="36"/>
        <source>Hide on close</source>
        <translation>Nascondi alla chiusura</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="43"/>
        <source>Start hidden</source>
        <translation>Avvia nascosto</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="210"/>
        <source>Visualization Colors</source>
        <translation>Colori della visualizzazione</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="216"/>
        <source>Color #1:</source>
        <translation>Colore #1:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="283"/>
        <source>Color #2:</source>
        <translation>Colore #2:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="347"/>
        <source>Color #3:</source>
        <translation>Colore #3:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="436"/>
        <source>Reset colors</source>
        <translation>Ripristina colori</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="703"/>
        <source>Override group colors</source>
        <translation>Sovrascrivi colori di gruppo</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="806"/>
        <source>Override current track colors</source>
        <translation>Sovrascrivi colori della traccia corrente</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="816"/>
        <source>Current track text:</source>
        <translation>Testo della traccia attuale:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="915"/>
        <source>Waveform Seekbar Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="921"/>
        <source>Progress bar:</source>
        <translation>Barra di avanzamento</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1010"/>
        <source>RMS:</source>
        <extracomment>Root mean square</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1055"/>
        <source>Waveform:</source>
        <translation>Forma d&apos;onda:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1096"/>
        <source>Fonts</source>
        <translation>Caratteri</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1102"/>
        <source>Use system fonts</source>
        <translation>Usa caratteri di sistema</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1176"/>
        <source>Playlist:</source>
        <translation>Scaletta</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="62"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="30"/>
        <source>Main Window</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="52"/>
        <source>Window title format:</source>
        <translation>Formato del titolo della finestra:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="74"/>
        <source>Song Display</source>
        <translation>Visualizzazione brani</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="80"/>
        <source>Show protocol</source>
        <translation>Mostra protocollo</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="148"/>
        <source>Show song numbers</source>
        <translation>Mostra il numero dei brani</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="155"/>
        <source>Show song lengths</source>
        <translation>Mostra la lunghezza dei brani</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="165"/>
        <source>Align song numbers</source>
        <translation>Allinea il numero dei brani</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="87"/>
        <source>Show anchor</source>
        <translation>Mostra ancora</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="101"/>
        <source>Show popup information</source>
        <translation>Mostra informazioni a comparsa</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="113"/>
        <source>Edit template</source>
        <translation>Modifica modello</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1243"/>
        <source>Reset fonts</source>
        <translation>Ripristina caratteri</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1192"/>
        <source>Column headers:</source>
        <translation>Intestazioni delle colonne:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1258"/>
        <source>Tab names:</source>
        <translation>Nomi delle schede:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1339"/>
        <source>Miscellaneous</source>
        <translation>Varie</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="94"/>
        <source>Show splitters</source>
        <translation>Mostra separatori</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="142"/>
        <source>Single Column Mode</source>
        <translation>Modalità a colonna singola</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="189"/>
        <source>Colors</source>
        <translation>Colori</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="251"/>
        <source>Peaks:</source>
        <translation>Picchi:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="315"/>
        <location filename="../forms/qsuisettings.ui" line="1003"/>
        <source>Background:</source>
        <translation>Sfondo:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="448"/>
        <source>Playlist Colors</source>
        <translation>Colori della scaletta</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="470"/>
        <source>Background #1:</source>
        <translation>Sfondo n° 1:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="601"/>
        <source>Normal text:</source>
        <translation>Testo normale:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="569"/>
        <source>Background #2:</source>
        <translation>Sfondo n° 2:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="633"/>
        <source>Highlighted background:</source>
        <translation>Sfondo evidenziato:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="665"/>
        <source>Highlighted text:</source>
        <translation>Testo evidenziato:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="867"/>
        <source>Current track background:</source>
        <translation>Sfondo della traccia corrente:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1268"/>
        <source>Groups:</source>
        <translation>Gruppi</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1275"/>
        <source>Extra group row:</source>
        <translation>Riga aggiuntiva di gruppo</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1372"/>
        <source>Tab position:</source>
        <translation>Posizione scheda:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1409"/>
        <source>Toolbars</source>
        <translation>Barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1417"/>
        <source>Icon size:</source>
        <translation>Dimensione icona:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1442"/>
        <source>Customize...</source>
        <translation>Personalizza...</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="454"/>
        <source>Use system colors</source>
        <translation>Usa colori di sistema</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="761"/>
        <source>Group background:</source>
        <translation>Sfondo di gruppo:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="713"/>
        <source>Group text:</source>
        <translation>Testo di gruppo:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="515"/>
        <source>Splitter:</source>
        <translation>Separatore:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1345"/>
        <source>Tabs</source>
        <translation>Schede</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1351"/>
        <source>Show close buttons</source>
        <translation>Mostra pulsanti di chiusura</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1358"/>
        <source>Show tab list menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1365"/>
        <source>Show &apos;New Playlist&apos; button</source>
        <translation>Mostra pulsante «Nuova scaletta»</translation>
    </message>
</context>
<context>
    <name>QSUIVisualization</name>
    <message>
        <location filename="../qsuivisualization.cpp" line="125"/>
        <source>Cover</source>
        <translation>Copertina</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="128"/>
        <source>Visualization Mode</source>
        <translation>Modo visualizzazione</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="131"/>
        <source>Analyzer</source>
        <translation>Analizzatore</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="132"/>
        <source>Scope</source>
        <translation>Oscilloscopio</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="139"/>
        <source>Analyzer Mode</source>
        <translation>Modo analizzatore</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="141"/>
        <source>Cells</source>
        <translation>Celle</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="142"/>
        <source>Lines</source>
        <translation>Linee</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="150"/>
        <source>Peaks</source>
        <translation>Picchi</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="153"/>
        <source>Refresh Rate</source>
        <translation>Velocità di aggiornamento</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="156"/>
        <source>50 fps</source>
        <translation>50 fps</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="157"/>
        <source>25 fps</source>
        <translation>25 fps</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="158"/>
        <source>10 fps</source>
        <translation>10 fps</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="159"/>
        <source>5 fps</source>
        <translation>5 fps</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="166"/>
        <source>Analyzer Falloff</source>
        <translation>Ricaduta analizzatore</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="169"/>
        <location filename="../qsuivisualization.cpp" line="183"/>
        <source>Slowest</source>
        <translation>Molto lenta</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="170"/>
        <location filename="../qsuivisualization.cpp" line="184"/>
        <source>Slow</source>
        <translation>Lenta</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="171"/>
        <location filename="../qsuivisualization.cpp" line="185"/>
        <source>Medium</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="172"/>
        <location filename="../qsuivisualization.cpp" line="186"/>
        <source>Fast</source>
        <translation>Rapida</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="173"/>
        <location filename="../qsuivisualization.cpp" line="187"/>
        <source>Fastest</source>
        <translation>Molto veloce</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="180"/>
        <source>Peaks Falloff</source>
        <translation>Ricaduta picchi</translation>
    </message>
</context>
<context>
    <name>QSUiActionManager</name>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="43"/>
        <source>&amp;Play</source>
        <translation>Ri&amp;produci</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="43"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="44"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pausa</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="44"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="45"/>
        <source>&amp;Stop</source>
        <translation>&amp;Ferma</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="45"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="46"/>
        <source>&amp;Previous</source>
        <translation>&amp;Precedente</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="46"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="47"/>
        <source>&amp;Next</source>
        <translation>&amp;Successivo</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="47"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="48"/>
        <source>&amp;Play/Pause</source>
        <translation>&amp;Riproduci/pausa</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="48"/>
        <source>Space</source>
        <translation>Spazio</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="49"/>
        <source>&amp;Jump to Track</source>
        <translation>&amp;Passa alla traccia</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="49"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="50"/>
        <source>&amp;Play Files</source>
        <translation>Ri&amp;produci file</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="50"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="51"/>
        <source>&amp;Record</source>
        <translation>&amp;Registra</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="52"/>
        <source>&amp;Repeat Playlist</source>
        <translation>&amp;Ripeti la scaletta</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="52"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="53"/>
        <source>&amp;Repeat Track</source>
        <translation>&amp;Ripeti la traccia</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="53"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="54"/>
        <source>&amp;Shuffle</source>
        <translation>Ordine ca&amp;suale</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="54"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="55"/>
        <source>&amp;No Playlist Advance</source>
        <translation>&amp;Non avanzare nella scaletta</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="55"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="56"/>
        <source>&amp;Transit between playlists</source>
        <translation>Passaggio &amp;tra scalette</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="57"/>
        <source>&amp;Stop After Selected</source>
        <translation>&amp;Ferma dopo l&apos;elemento selezionato</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="57"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="58"/>
        <source>&amp;Clear Queue</source>
        <translation>Pulis&amp;ci la coda</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="58"/>
        <source>Alt+Q</source>
        <translation>Alt+Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="60"/>
        <source>Always on Top</source>
        <translation>Sempre in primo piano</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="61"/>
        <source>Put on All Workspaces</source>
        <translation>Su tutti gli spazi di lavoro</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="67"/>
        <source>Show Tabs</source>
        <translation>Mostra le schede</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="68"/>
        <source>Block Floating Panels</source>
        <translation>Blocca i pannelli fluttuanti</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="69"/>
        <source>Block Toolbars</source>
        <translation>Blocca le barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="71"/>
        <source>Volume &amp;+</source>
        <translation>Volume &amp;+</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="71"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="72"/>
        <source>Volume &amp;-</source>
        <translation>Volume &amp;-</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="72"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="73"/>
        <source>&amp;Mute</source>
        <translation>&amp;Silenzia</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="73"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="75"/>
        <source>&amp;Add File</source>
        <translation>&amp;Aggiungi file</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="75"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="76"/>
        <source>&amp;Add Directory</source>
        <translation>&amp;Aggiungi cartella</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="76"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="77"/>
        <source>&amp;Add Url</source>
        <translation>&amp;Aggiungi URL</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="77"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="78"/>
        <source>&amp;Remove Selected</source>
        <translation>&amp;Rimuovi selezionati</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="78"/>
        <source>Del</source>
        <translation>Canc</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="79"/>
        <source>&amp;Remove All</source>
        <translation>&amp;Rimuovi tutti</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="80"/>
        <source>&amp;Remove Unselected</source>
        <translation>&amp;Rimuovi non selezionati</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="81"/>
        <source>Remove unavailable files</source>
        <translation>Rimuovi file non disponibili</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="82"/>
        <source>Remove duplicates</source>
        <translation>Rimuovi duplicati</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="83"/>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="84"/>
        <source>&amp;Queue Toggle</source>
        <translation>Attiva co&amp;da</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="84"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="85"/>
        <source>Invert Selection</source>
        <translation>Inverti la selezione</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="86"/>
        <source>&amp;Select None</source>
        <translation>De&amp;seleziona tutti</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="87"/>
        <source>&amp;Select All</source>
        <translation>&amp;Seleziona tutti</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="87"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="88"/>
        <source>&amp;View Track Details</source>
        <translation>&amp;Vedi i dettagli della traccia</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="88"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="89"/>
        <source>&amp;New List</source>
        <translation>&amp;Nuova lista</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="89"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="90"/>
        <source>&amp;Delete List</source>
        <translation>&amp;Elimina lista</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="90"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="91"/>
        <source>&amp;Load List</source>
        <translation>Carica &amp;lista</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="91"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="92"/>
        <source>&amp;Save List</source>
        <translation>&amp;Salva lista</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="92"/>
        <source>Shift+S</source>
        <translation>Shift+S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="93"/>
        <source>&amp;Rename List</source>
        <translation>&amp;Rinomina lista</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="93"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="94"/>
        <source>&amp;Select Next Playlist</source>
        <translation>&amp;Seleziona la scaletta successiva</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="94"/>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+Pag↓</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="95"/>
        <source>&amp;Select Previous Playlist</source>
        <translation>&amp;Seleziona la scaletta precedente</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="95"/>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+Pag↑</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="96"/>
        <source>&amp;Group Tracks</source>
        <translation>Ra&amp;ggruppa tracce</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="96"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="97"/>
        <source>&amp;Show Column Headers</source>
        <translation>Mo&amp;stra intestazioni di colonne</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="97"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="99"/>
        <source>&amp;Equalizer</source>
        <translation>&amp;Equalizzatore</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="99"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="100"/>
        <source>&amp;Settings</source>
        <translation>Impo&amp;stazioni</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="100"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="101"/>
        <source>Application Menu</source>
        <translation>Menu applicazione</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="102"/>
        <source>&amp;About Ui</source>
        <translation>&amp;Informazioni sull&apos;interfaccia</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="103"/>
        <source>&amp;About</source>
        <translation>&amp;Informazioni</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="104"/>
        <source>&amp;About Qt</source>
        <translation>&amp;Informazioni su Qt</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="105"/>
        <source>&amp;Exit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="105"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="310"/>
        <source>Toolbar</source>
        <translation>Barra degli strumenti</translation>
    </message>
</context>
<context>
    <name>QSUiCoverWidget</name>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="32"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Salva come...</translation>
    </message>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="65"/>
        <source>Save Cover As</source>
        <translation>Salva copertina come</translation>
    </message>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="67"/>
        <source>Images</source>
        <translation>Immagini</translation>
    </message>
</context>
<context>
    <name>QSUiEqualizer</name>
    <message>
        <location filename="../qsuiequalizer.cpp" line="39"/>
        <source>Equalizer</source>
        <translation>Equalizzatore</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="48"/>
        <source>Enable equalizer</source>
        <translation>Abilita equalizzatore</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="54"/>
        <source>Preset:</source>
        <translation>Preimpostazione:</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="62"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="66"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="70"/>
        <source>Reset</source>
        <translation>Azzera impostazione</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="83"/>
        <source>Preamp</source>
        <translation>Preamplificazione</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="100"/>
        <location filename="../qsuiequalizer.cpp" line="195"/>
        <source>%1dB</source>
        <translation>%1dB</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="102"/>
        <location filename="../qsuiequalizer.cpp" line="193"/>
        <source>+%1dB</source>
        <translation>+%1dB</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="148"/>
        <source>preset</source>
        <translation>preimpostazione</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="218"/>
        <source>Overwrite Request</source>
        <translation>Richiesta di sovrascrittura</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="219"/>
        <source>Preset &apos;%1&apos; already exists. Overwrite?</source>
        <translation>La preimpostazione «%1» esiste già. Sovrascriverla?</translation>
    </message>
</context>
<context>
    <name>QSUiFactory</name>
    <message>
        <location filename="../qsuifactory.cpp" line="32"/>
        <source>Simple User Interface</source>
        <translation>Interfaccia utente semplice</translation>
    </message>
</context>
<context>
    <name>QSUiHotkeyEditor</name>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="33"/>
        <source>Change shortcut...</source>
        <translation>Modifica scorciatoia...</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="40"/>
        <source>Reset</source>
        <translation>Azzera impostazione</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="54"/>
        <source>Action</source>
        <translation>Azione</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="59"/>
        <source>Shortcut</source>
        <translation>Scorciatoia</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="56"/>
        <source>Reset Shortcuts</source>
        <translation>Ripristina scorciatoie</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="57"/>
        <source>Do you want to restore default shortcuts?</source>
        <translation>Vuoi ripristinare le scorciatoie predefinite?</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="69"/>
        <source>Playback</source>
        <translation>Riproduzione</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="75"/>
        <source>View</source>
        <translation>Visualizza</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="81"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="87"/>
        <source>Playlist</source>
        <translation>Scaletta</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="93"/>
        <source>Misc</source>
        <translation>Varie</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="101"/>
        <source>Tools</source>
        <translation>Strumenti</translation>
    </message>
</context>
<context>
    <name>QSUiMainWindow</name>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="14"/>
        <location filename="../qsuimainwindow.cpp" line="917"/>
        <source>Qmmp</source>
        <translation>Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="35"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="40"/>
        <source>&amp;Tools</source>
        <translation>S&amp;trumenti</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="45"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="50"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modifica</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="55"/>
        <source>&amp;Playback</source>
        <translation>Ri&amp;produci</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="60"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="84"/>
        <location filename="../forms/qsuimainwindow.ui" line="249"/>
        <source>Visualization</source>
        <translation>Visualizzazione</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="99"/>
        <source>Files</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="114"/>
        <source>Cover</source>
        <translation>Copertina</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="123"/>
        <source>Playlists</source>
        <translation>Scalette</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="135"/>
        <source>Waveform Seek Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="149"/>
        <source>Previous</source>
        <translation>Precedente</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="159"/>
        <source>Play</source>
        <translation>Riproduci</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="169"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="179"/>
        <source>Next</source>
        <translation>Successivo</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="189"/>
        <source>Stop</source>
        <translation>Ferma</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="194"/>
        <source>&amp;Add File</source>
        <translation>&amp;Aggiungi file</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="199"/>
        <source>&amp;Remove All</source>
        <translation>&amp;Rimuovi tutti</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="204"/>
        <source>New Playlist</source>
        <translation>Nuova scaletta</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="209"/>
        <source>Remove Playlist</source>
        <translation>Rimuovi scaletta</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="214"/>
        <source>&amp;Add Directory</source>
        <translation>&amp;Aggiungi cartella</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="219"/>
        <source>&amp;Exit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="224"/>
        <source>About</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="229"/>
        <source>About Qt</source>
        <translation>Informazioni su Qt</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="234"/>
        <source>&amp;Select All</source>
        <translation>&amp;Seleziona tutti</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="239"/>
        <source>&amp;Remove Selected</source>
        <translation>&amp;Rimuovi selezionati</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="244"/>
        <source>&amp;Remove Unselected</source>
        <translation>&amp;Rimuovi non selezionati</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="254"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="259"/>
        <location filename="../qsuimainwindow.cpp" line="295"/>
        <source>Rename Playlist</source>
        <translation>Rinomina scaletta</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="86"/>
        <source>&amp;Copy Selection To</source>
        <translation>&amp;Copia selezione in</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="295"/>
        <source>Playlist name:</source>
        <translation>Nome della scaletta</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="335"/>
        <source>Appearance</source>
        <translation>Aspetto</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="336"/>
        <source>Shortcuts</source>
        <translation>Scorciatoie</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="415"/>
        <source>Menu Bar</source>
        <translation>Barra del menu</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="434"/>
        <source>Add new playlist</source>
        <translation>Aggiungi nuova scaletta</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="440"/>
        <source>Show all tabs</source>
        <translation>Mostra tutte le schede</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="467"/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="473"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="478"/>
        <source>Position</source>
        <translation>Posizione</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="480"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="482"/>
        <source>Balance</source>
        <translation>Bilanciamento</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="485"/>
        <source>Quick Search</source>
        <translation>Ricerca rapida</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="543"/>
        <source>Edit Toolbars</source>
        <translation>Modifica barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="545"/>
        <source>Sort List</source>
        <translation>Riordina la lista</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="547"/>
        <location filename="../qsuimainwindow.cpp" line="563"/>
        <source>By Title</source>
        <translation>Per titolo</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="548"/>
        <location filename="../qsuimainwindow.cpp" line="564"/>
        <source>By Album</source>
        <translation>Per album</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="549"/>
        <location filename="../qsuimainwindow.cpp" line="565"/>
        <source>By Artist</source>
        <translation>Per artista</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="550"/>
        <location filename="../qsuimainwindow.cpp" line="566"/>
        <source>By Album Artist</source>
        <translation>Per artista dell&apos;album</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="551"/>
        <location filename="../qsuimainwindow.cpp" line="567"/>
        <source>By Filename</source>
        <translation>Per nome del file</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="552"/>
        <location filename="../qsuimainwindow.cpp" line="568"/>
        <source>By Path + Filename</source>
        <translation>Per percorso più nome del file</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="553"/>
        <location filename="../qsuimainwindow.cpp" line="569"/>
        <source>By Date</source>
        <translation>Per data</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="554"/>
        <location filename="../qsuimainwindow.cpp" line="570"/>
        <source>By Track Number</source>
        <translation>Per numero di traccia</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="555"/>
        <location filename="../qsuimainwindow.cpp" line="571"/>
        <source>By Disc Number</source>
        <translation>Per numero di disco</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="556"/>
        <location filename="../qsuimainwindow.cpp" line="572"/>
        <source>By File Creation Date</source>
        <translation>Per data di creazione dei file</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="557"/>
        <location filename="../qsuimainwindow.cpp" line="573"/>
        <source>By File Modification Date</source>
        <translation>Per data di modifica dei file</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="558"/>
        <source>By Group</source>
        <translation>Per gruppo</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="561"/>
        <source>Sort Selection</source>
        <translation>Riordina la selezione</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="577"/>
        <source>Randomize List</source>
        <translation>Mescola il contenuto della lista</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="579"/>
        <source>Reverse List</source>
        <translation>Inverti la lista</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="620"/>
        <source>Actions</source>
        <translation>Azioni</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="938"/>
        <source>&amp;New PlayList</source>
        <translation>&amp;Nuova scaletta</translation>
    </message>
</context>
<context>
    <name>QSUiPlayListBrowser</name>
    <message>
        <location filename="../qsuiplaylistbrowser.cpp" line="62"/>
        <source>Quick Search</source>
        <translation>Ricerca rapida</translation>
    </message>
</context>
<context>
    <name>QSUiPlayListHeader</name>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="54"/>
        <source>Add Column</source>
        <translation>Aggiungi colonna</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="55"/>
        <source>Edit Column</source>
        <translation>Modifica colonna</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="56"/>
        <source>Show Queue/Protocol</source>
        <translation>Mostra coda/protocollo</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="58"/>
        <source>Auto-resize</source>
        <translation>Ridimensionamento automatico</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="61"/>
        <source>Alignment</source>
        <translation>Allineamento</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="62"/>
        <source>Left</source>
        <comment>alignment</comment>
        <translation>A sinistra</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="63"/>
        <source>Right</source>
        <comment>alignment</comment>
        <translation>A destra</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="64"/>
        <source>Center</source>
        <comment>alignment</comment>
        <translation>Al centro</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="74"/>
        <source>Remove Column</source>
        <translation>Rimuovi colonna</translation>
    </message>
</context>
<context>
    <name>QSUiPopupSettings</name>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="14"/>
        <source>Popup Information Settings</source>
        <translation>Impostazioni informazioni a comparsa</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="29"/>
        <source>Template</source>
        <translation>Modello</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="58"/>
        <source>Reset</source>
        <translation>Azzera impostazione</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="65"/>
        <source>Insert</source>
        <translation>Inserisci</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="75"/>
        <source>Show cover</source>
        <translation>Mostra copertina</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="89"/>
        <source>Cover size:</source>
        <translation>Dimensione copertina:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="115"/>
        <source>Transparency:</source>
        <translation>Trasparenza:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="145"/>
        <source>Delay:</source>
        <translation>Ritardo:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="165"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
</context>
<context>
    <name>QSUiSettings</name>
    <message>
        <location filename="../qsuisettings.cpp" line="41"/>
        <source>Default</source>
        <translation>Predefinito</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="42"/>
        <source>16x16</source>
        <translation>16x16</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="43"/>
        <source>22x22</source>
        <translation>22x22</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="44"/>
        <source>32x32</source>
        <translation>32x32</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="45"/>
        <source>48x48</source>
        <translation>48x48</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="46"/>
        <source>64x64</source>
        <translation>64x64</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="48"/>
        <source>Top</source>
        <translation>In alto</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="49"/>
        <source>Bottom</source>
        <translation>In basso</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="50"/>
        <source>Left</source>
        <translation>A sinistra</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="51"/>
        <source>Right</source>
        <translation>A destra</translation>
    </message>
</context>
<context>
    <name>QSUiStatusBar</name>
    <message>
        <location filename="../qsuistatusbar.cpp" line="68"/>
        <source>tracks: %1</source>
        <translation>tracce: %1</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="69"/>
        <source>total time: %1</source>
        <translation>tempo totale: %1</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="87"/>
        <source>Playing</source>
        <translation>In riproduzione</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="87"/>
        <source>Paused</source>
        <translation>In pausa</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="102"/>
        <source>Buffering</source>
        <translation>Riempimento buffer</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="127"/>
        <source>Stopped</source>
        <translation>Fermato</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="139"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="147"/>
        <source>Buffering: %1%</source>
        <translation>Riempimento buffer: %1%</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="152"/>
        <source>%1 bits</source>
        <translation>%1 bit</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="154"/>
        <source>mono</source>
        <translation>mono</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="156"/>
        <source>stereo</source>
        <translation>stereo</translation>
    </message>
    <message numerus="yes">
        <location filename="../qsuistatusbar.cpp" line="158"/>
        <source>%n channels</source>
        <translation>
            <numerusform>1 canale</numerusform>
            <numerusform>%n canali</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="159"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="164"/>
        <source>%1 kbps</source>
        <translation>%1 kbps</translation>
    </message>
</context>
<context>
    <name>QSUiWaveformSeekBar</name>
    <message>
        <location filename="../qsuiwaveformseekbar.cpp" line="335"/>
        <source>2 Channels</source>
        <translation>2 canali</translation>
    </message>
    <message>
        <location filename="../qsuiwaveformseekbar.cpp" line="338"/>
        <source>RMS</source>
        <extracomment>Root mean square</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBarEditor</name>
    <message>
        <location filename="../forms/toolbareditor.ui" line="14"/>
        <source>ToolBar Editor</source>
        <translation>Editor delle barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="62"/>
        <source>Reset</source>
        <translation>Azzera</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="199"/>
        <source>Toolbar:</source>
        <translation>Barra degli strumenti:</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="222"/>
        <source>&amp;Create</source>
        <translation>&amp;Crea</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="238"/>
        <source>Re&amp;name</source>
        <translation>Ri&amp;nomina</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="254"/>
        <source>&amp;Remove</source>
        <translation>&amp;Rimuovi</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="100"/>
        <location filename="../toolbareditor.cpp" line="198"/>
        <source>Separator</source>
        <translation>Separatore</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="248"/>
        <source>Toolbar</source>
        <translation>Barra degli strumenti</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="250"/>
        <source>Toolbar %1</source>
        <translation>Barra degli strumenti %1</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="264"/>
        <source>Rename Toolbar</source>
        <translation>Rinomina barra degli strumenti</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="264"/>
        <source>Toolbar name:</source>
        <translation>Nome della barra degli strumenti:</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../volumeslider.cpp" line="110"/>
        <source>%1: %2%</source>
        <translation>%1: %2%</translation>
    </message>
</context>
</TS>
