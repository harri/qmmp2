<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>AboutQSUIDialog</name>
    <message>
        <location filename="../forms/aboutqsuidialog.ui" line="14"/>
        <source>About QSUI</source>
        <translation>O programie QSUI</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="42"/>
        <source>Qmmp Simple User Interface (QSUI)</source>
        <translation>Prosty interfejs użytkownika Qmmp (QSUI)</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="43"/>
        <source>Qmmp version: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Wersja Qmmp: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="45"/>
        <source>Developers:</source>
        <translation>Programiści:</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="46"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="48"/>
        <source>Translators:</source>
        <translation>Tłumacze:</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="44"/>
        <source>Simple user interface based on standard widgets set.</source>
        <translation>Prosty interfejs użytkownika oparty na standardowych widżetach.</translation>
    </message>
</context>
<context>
    <name>FileSystemBrowser</name>
    <message>
        <location filename="../filesystembrowser.cpp" line="100"/>
        <source>Add to Playlist</source>
        <translation>Dodaj do listy odtwarzania</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="101"/>
        <source>Replace Playlist</source>
        <translation>Zamień listę odtwarzania</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="102"/>
        <source>Change Directory</source>
        <translation>Zmień katalog</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="105"/>
        <source>Tree View Mode</source>
        <translation>Tryb widoku drzewa</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="107"/>
        <source>Quick Search</source>
        <translation>Szybkie wyszukiwanie</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="116"/>
        <source>Sort</source>
        <translation>Sortuj</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="110"/>
        <source>By Name</source>
        <translation>Według nazwy</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="111"/>
        <source>By Size</source>
        <translation>Według rozmiaru</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="112"/>
        <source>By Type</source>
        <translation>Według typu</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="113"/>
        <source>By Date</source>
        <translation>Według daty</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="219"/>
        <source>Select Directory</source>
        <translation>Wybierz katalog</translation>
    </message>
</context>
<context>
    <name>QSUISettings</name>
    <message>
        <location filename="../forms/qsuisettings.ui" line="24"/>
        <source>View</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="36"/>
        <source>Hide on close</source>
        <translation>Zminimalizuj przy zamykaniu</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="43"/>
        <source>Start hidden</source>
        <translation>Uruchom zminimalizowany</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="210"/>
        <source>Visualization Colors</source>
        <translation>Kolory wizualizacji</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="216"/>
        <source>Color #1:</source>
        <translation>Kolor #1:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="283"/>
        <source>Color #2:</source>
        <translation>Kolor #2:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="347"/>
        <source>Color #3:</source>
        <translation>Kolor #3:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="436"/>
        <source>Reset colors</source>
        <translation>Resetuj kolory</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="703"/>
        <source>Override group colors</source>
        <translation>Zastąp kolory grupy</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="806"/>
        <source>Override current track colors</source>
        <translation>Zastąp kolory bieżącej ścieżki</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="816"/>
        <source>Current track text:</source>
        <translation>Tekst bieżącego utworu:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="915"/>
        <source>Waveform Seekbar Colors</source>
        <translation>Kolory paska wyszukiwania przebiegu fali</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="921"/>
        <source>Progress bar:</source>
        <translation>Pasek postępu:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1010"/>
        <source>RMS:</source>
        <extracomment>Root mean square</extracomment>
        <translation>Średnia kwadratowa:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1055"/>
        <source>Waveform:</source>
        <translation>Przebieg fali:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1096"/>
        <source>Fonts</source>
        <translation>Czcionki</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1102"/>
        <source>Use system fonts</source>
        <translation>Użyj systemowych czcionek</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1176"/>
        <source>Playlist:</source>
        <translation>Lista odtwarzania:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="62"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="30"/>
        <source>Main Window</source>
        <translation>Okno główne </translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="52"/>
        <source>Window title format:</source>
        <translation>Format tytułu okna:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="74"/>
        <source>Song Display</source>
        <translation>Wyświetlanie utworu</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="80"/>
        <source>Show protocol</source>
        <translation>Pokaż protokół</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="148"/>
        <source>Show song numbers</source>
        <translation>Wyświetl numery utworów</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="155"/>
        <source>Show song lengths</source>
        <translation>Pokaż długości utworów</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="165"/>
        <source>Align song numbers</source>
        <translation>Wyrównaj w pionie numery utworów</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="87"/>
        <source>Show anchor</source>
        <translation>Pokaż kotwicę</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="101"/>
        <source>Show popup information</source>
        <translation>Pokaż informację popup</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="113"/>
        <source>Edit template</source>
        <translation>Edytuj szablon</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1243"/>
        <source>Reset fonts</source>
        <translation>Resetuj czcionki</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1192"/>
        <source>Column headers:</source>
        <translation>Nagłówki kolumn:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1258"/>
        <source>Tab names:</source>
        <translation>Nazwy kart:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1339"/>
        <source>Miscellaneous</source>
        <translation>Inne</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="94"/>
        <source>Show splitters</source>
        <translation>Pokaż rozdzielacze</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="142"/>
        <source>Single Column Mode</source>
        <translation>Tryb pojedynczej kolumny</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="189"/>
        <source>Colors</source>
        <translation>Kolory</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="251"/>
        <source>Peaks:</source>
        <translation>Szczyty:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="315"/>
        <location filename="../forms/qsuisettings.ui" line="1003"/>
        <source>Background:</source>
        <translation>Tło:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="448"/>
        <source>Playlist Colors</source>
        <translation>Kolory list odtwarzania</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="470"/>
        <source>Background #1:</source>
        <translation>Tło #1:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="601"/>
        <source>Normal text:</source>
        <translation>Normalny tekst:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="569"/>
        <source>Background #2:</source>
        <translation>Tło #2:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="633"/>
        <source>Highlighted background:</source>
        <translation>Podświetlone tło:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="665"/>
        <source>Highlighted text:</source>
        <translation>Podświetlony tekst:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="867"/>
        <source>Current track background:</source>
        <translation>Tło aktualnego utworu:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1268"/>
        <source>Groups:</source>
        <translation>Grupy:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1275"/>
        <source>Extra group row:</source>
        <translation>Wiersz dodatkowej grupy:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1372"/>
        <source>Tab position:</source>
        <translation>Pozycja karty:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1409"/>
        <source>Toolbars</source>
        <translation>Paski narzędziowe</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1417"/>
        <source>Icon size:</source>
        <translation>Rozmiar ikony:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1442"/>
        <source>Customize...</source>
        <translation>Dostosuj...</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="454"/>
        <source>Use system colors</source>
        <translation>Użyj systemowych kolorów</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="761"/>
        <source>Group background:</source>
        <translation>Tło grupy:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="713"/>
        <source>Group text:</source>
        <translation>Tekst grupy:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="515"/>
        <source>Splitter:</source>
        <translation>Rozdzielacz:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1345"/>
        <source>Tabs</source>
        <translation>Karty</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1351"/>
        <source>Show close buttons</source>
        <translation>Pokaż przyciski zamykania</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1358"/>
        <source>Show tab list menu</source>
        <translation>Pokaż menu listy kart</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1365"/>
        <source>Show &apos;New Playlist&apos; button</source>
        <translation>Pokaż przycisk &apos;Nowa lista odtwarzania&apos;</translation>
    </message>
</context>
<context>
    <name>QSUIVisualization</name>
    <message>
        <location filename="../qsuivisualization.cpp" line="125"/>
        <source>Cover</source>
        <translation>Okładka</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="128"/>
        <source>Visualization Mode</source>
        <translation>Tryb wizualizacji</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="131"/>
        <source>Analyzer</source>
        <translation>Analizator</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="132"/>
        <source>Scope</source>
        <translation>Zakres</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="139"/>
        <source>Analyzer Mode</source>
        <translation>Tryb Analizatora</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="141"/>
        <source>Cells</source>
        <translation>Komórki</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="142"/>
        <source>Lines</source>
        <translation>Linie</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="150"/>
        <source>Peaks</source>
        <translation>Szczyty</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="153"/>
        <source>Refresh Rate</source>
        <translation>Odświeżanie</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="156"/>
        <source>50 fps</source>
        <translation>50 kl./s</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="157"/>
        <source>25 fps</source>
        <translation>25 kl./s</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="158"/>
        <source>10 fps</source>
        <translation>10 kl./s</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="159"/>
        <source>5 fps</source>
        <translation>5 kl./s</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="166"/>
        <source>Analyzer Falloff</source>
        <translation>Opadanie Analizatora</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="169"/>
        <location filename="../qsuivisualization.cpp" line="183"/>
        <source>Slowest</source>
        <translation>Najwolniej</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="170"/>
        <location filename="../qsuivisualization.cpp" line="184"/>
        <source>Slow</source>
        <translation>Wolno</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="171"/>
        <location filename="../qsuivisualization.cpp" line="185"/>
        <source>Medium</source>
        <translation>Średnia</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="172"/>
        <location filename="../qsuivisualization.cpp" line="186"/>
        <source>Fast</source>
        <translation>Szybko</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="173"/>
        <location filename="../qsuivisualization.cpp" line="187"/>
        <source>Fastest</source>
        <translation>Najszybciej</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="180"/>
        <source>Peaks Falloff</source>
        <translation>Opadanie szczytów</translation>
    </message>
</context>
<context>
    <name>QSUiActionManager</name>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="43"/>
        <source>&amp;Play</source>
        <translation>&amp;Odtwarzaj</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="43"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="44"/>
        <source>&amp;Pause</source>
        <translation>&amp;Wstrzymaj</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="44"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="45"/>
        <source>&amp;Stop</source>
        <translation>&amp;Zatrzymaj</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="45"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="46"/>
        <source>&amp;Previous</source>
        <translation>&amp;Poprzedni</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="46"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="47"/>
        <source>&amp;Next</source>
        <translation>&amp;Następny</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="47"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="48"/>
        <source>&amp;Play/Pause</source>
        <translation>&amp;Odtwarzaj/Wstrzymaj</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="48"/>
        <source>Space</source>
        <translation>Spacja</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="49"/>
        <source>&amp;Jump to Track</source>
        <translation>&amp;Skocz do utworu</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="49"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="50"/>
        <source>&amp;Play Files</source>
        <translation>Odtwarzaj &amp;pliki</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="50"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="51"/>
        <source>&amp;Record</source>
        <translation>Na&amp;graj</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="52"/>
        <source>&amp;Repeat Playlist</source>
        <translation>Powtó&amp;rz listę odtwarzania</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="52"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="53"/>
        <source>&amp;Repeat Track</source>
        <translation>&amp;Powtórz utwór</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="53"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="54"/>
        <source>&amp;Shuffle</source>
        <translation>&amp;Losowo</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="54"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="55"/>
        <source>&amp;No Playlist Advance</source>
        <translation>&amp;Brak listy odtwarzania</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="55"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="56"/>
        <source>&amp;Transit between playlists</source>
        <translation>Przejście między listami &amp;odtwarzania</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="57"/>
        <source>&amp;Stop After Selected</source>
        <translation>Za&amp;trzymaj po aktualnie odtwarzanym utworze</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="57"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="58"/>
        <source>&amp;Clear Queue</source>
        <translation>Wy&amp;czyść kolejkę</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="58"/>
        <source>Alt+Q</source>
        <translation>Alt+Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="60"/>
        <source>Always on Top</source>
        <translation>Zawsze na wierzchu</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="61"/>
        <source>Put on All Workspaces</source>
        <translation>Na wszystkie pulpity</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="67"/>
        <source>Show Tabs</source>
        <translation>Pokaż karty</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="68"/>
        <source>Block Floating Panels</source>
        <translation>Zablokuj pływające panele</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="69"/>
        <source>Block Toolbars</source>
        <translation>Zablokuj paski narzędziowe</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="71"/>
        <source>Volume &amp;+</source>
        <translation>Głośność &amp;+</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="71"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="72"/>
        <source>Volume &amp;-</source>
        <translation>Głośność &amp;-</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="72"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="73"/>
        <source>&amp;Mute</source>
        <translation>Wycisz</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="73"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="75"/>
        <source>&amp;Add File</source>
        <translation>&amp;Dodaj plik</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="75"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="76"/>
        <source>&amp;Add Directory</source>
        <translation>Dodaj &amp;katalog</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="76"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="77"/>
        <source>&amp;Add Url</source>
        <translation>Dod&amp;aj URL</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="77"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="78"/>
        <source>&amp;Remove Selected</source>
        <translation>&amp;Usuń zaznaczone</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="78"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="79"/>
        <source>&amp;Remove All</source>
        <translation>Usuń &amp;wszystkie</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="80"/>
        <source>&amp;Remove Unselected</source>
        <translation>Usuń &amp;niezaznaczone</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="81"/>
        <source>Remove unavailable files</source>
        <translation>Usuń niedostępne pliki</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="82"/>
        <source>Remove duplicates</source>
        <translation>Usuń duplikaty</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="83"/>
        <source>Refresh</source>
        <translation>Odśwież</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="84"/>
        <source>&amp;Queue Toggle</source>
        <translation>Doda&amp;j do/Usuń z kolejki</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="84"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="85"/>
        <source>Invert Selection</source>
        <translation>Odwróć zaznaczenie</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="86"/>
        <source>&amp;Select None</source>
        <translation>&amp;Odznacz wszystkie</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="87"/>
        <source>&amp;Select All</source>
        <translation>&amp;Zaznacz wszystkie</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="87"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="88"/>
        <source>&amp;View Track Details</source>
        <translation>&amp;Pokaż informacje o pliku</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="88"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="89"/>
        <source>&amp;New List</source>
        <translation>&amp;Nowa lista</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="89"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="90"/>
        <source>&amp;Delete List</source>
        <translation>Usuń listę o&amp;dtwarzania</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="90"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="91"/>
        <source>&amp;Load List</source>
        <translation>&amp;Ładuj listę</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="91"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="92"/>
        <source>&amp;Save List</source>
        <translation>&amp;Zapisz listę</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="92"/>
        <source>Shift+S</source>
        <translation>Shift+S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="93"/>
        <source>&amp;Rename List</source>
        <translation>&amp;Zmień nazwę listy</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="93"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="94"/>
        <source>&amp;Select Next Playlist</source>
        <translation>Wybierz na&amp;stępną listę</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="94"/>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+PgDown</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="95"/>
        <source>&amp;Select Previous Playlist</source>
        <translation>Wybierz poprzednią li&amp;stę</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="95"/>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+PgUp</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="96"/>
        <source>&amp;Group Tracks</source>
        <translation>&amp;Grupuj utwory</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="96"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="97"/>
        <source>&amp;Show Column Headers</source>
        <translation>&amp;Pokaż nagłówki kolumn</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="97"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="99"/>
        <source>&amp;Equalizer</source>
        <translation>&amp;Korektor</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="99"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="100"/>
        <source>&amp;Settings</source>
        <translation>&amp;Ustawienia</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="100"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="101"/>
        <source>Application Menu</source>
        <translation>Menu aplikacji</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="102"/>
        <source>&amp;About Ui</source>
        <translation>&amp;O UI</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="103"/>
        <source>&amp;About</source>
        <translation>&amp;O programie</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="104"/>
        <source>&amp;About Qt</source>
        <translation>&amp;O Qt</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="105"/>
        <source>&amp;Exit</source>
        <translation>&amp;Wyjście</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="105"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="310"/>
        <source>Toolbar</source>
        <translation>Pasek narzędziowy</translation>
    </message>
</context>
<context>
    <name>QSUiCoverWidget</name>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="32"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Zapisz jako...</translation>
    </message>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="65"/>
        <source>Save Cover As</source>
        <translation>Zapisz okładkę jako</translation>
    </message>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="67"/>
        <source>Images</source>
        <translation>Obrazy</translation>
    </message>
</context>
<context>
    <name>QSUiEqualizer</name>
    <message>
        <location filename="../qsuiequalizer.cpp" line="39"/>
        <source>Equalizer</source>
        <translation>Korektor</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="48"/>
        <source>Enable equalizer</source>
        <translation>Włącz korektor</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="54"/>
        <source>Preset:</source>
        <translation>Profil:</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="62"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="66"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="70"/>
        <source>Reset</source>
        <translation>Resetuj</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="83"/>
        <source>Preamp</source>
        <translation>Wzmocnienie sygnału</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="100"/>
        <location filename="../qsuiequalizer.cpp" line="195"/>
        <source>%1dB</source>
        <translation>%1dB</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="102"/>
        <location filename="../qsuiequalizer.cpp" line="193"/>
        <source>+%1dB</source>
        <translation>+%1dB</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="148"/>
        <source>preset</source>
        <translation>profil</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="218"/>
        <source>Overwrite Request</source>
        <translation>Nadpisz żądanie</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="219"/>
        <source>Preset &apos;%1&apos; already exists. Overwrite?</source>
        <translation>Profil &apos;%1&apos; już istnieje. Nadpisać?</translation>
    </message>
</context>
<context>
    <name>QSUiFactory</name>
    <message>
        <location filename="../qsuifactory.cpp" line="32"/>
        <source>Simple User Interface</source>
        <translation>Prosty interfejs użytkownika</translation>
    </message>
</context>
<context>
    <name>QSUiHotkeyEditor</name>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="33"/>
        <source>Change shortcut...</source>
        <translation>Zmień skrót...</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="40"/>
        <source>Reset</source>
        <translation>Resetuj</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="54"/>
        <source>Action</source>
        <translation>Akcja</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="59"/>
        <source>Shortcut</source>
        <translation>Skrót klawiszowy</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="56"/>
        <source>Reset Shortcuts</source>
        <translation>Resetuj skróty klawiszowe</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="57"/>
        <source>Do you want to restore default shortcuts?</source>
        <translation>Czy chcesz przywrócić domyślne skróty?</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="69"/>
        <source>Playback</source>
        <translation>Odtwarzanie</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="75"/>
        <source>View</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="81"/>
        <source>Volume</source>
        <translation>Głośność</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="87"/>
        <source>Playlist</source>
        <translation>Lista odtwarzania</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="93"/>
        <source>Misc</source>
        <translation>Różne</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="101"/>
        <source>Tools</source>
        <translation>Narzędzia</translation>
    </message>
</context>
<context>
    <name>QSUiMainWindow</name>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="14"/>
        <location filename="../qsuimainwindow.cpp" line="917"/>
        <source>Qmmp</source>
        <translation>Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="35"/>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="40"/>
        <source>&amp;Tools</source>
        <translation>&amp;Narzędzia</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="45"/>
        <source>&amp;Help</source>
        <translation>&amp;Pomoc</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="50"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edycja</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="55"/>
        <source>&amp;Playback</source>
        <translation>&amp;Odtwarzanie</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="60"/>
        <source>&amp;View</source>
        <translation>&amp;Wygląd</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="84"/>
        <location filename="../forms/qsuimainwindow.ui" line="249"/>
        <source>Visualization</source>
        <translation>Wizualizacja</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="99"/>
        <source>Files</source>
        <translation>Pliki</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="114"/>
        <source>Cover</source>
        <translation>Okładka</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="123"/>
        <source>Playlists</source>
        <translation>Listy odtwarzania</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="135"/>
        <source>Waveform Seek Bar</source>
        <translation>Pasek wyszukiwania przebiegu fali</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="149"/>
        <source>Previous</source>
        <translation>Poprzedni</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="159"/>
        <source>Play</source>
        <translation>Odtwarzaj</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="169"/>
        <source>Pause</source>
        <translation>Pauza</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="179"/>
        <source>Next</source>
        <translation>Następny</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="189"/>
        <source>Stop</source>
        <translation>Zatrzymaj</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="194"/>
        <source>&amp;Add File</source>
        <translation>&amp;Dodaj plik</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="199"/>
        <source>&amp;Remove All</source>
        <translation>Usuń &amp;wszystkie</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="204"/>
        <source>New Playlist</source>
        <translation>Nowa lista odtwarzania</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="209"/>
        <source>Remove Playlist</source>
        <translation>Usuń listę odtwarzania</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="214"/>
        <source>&amp;Add Directory</source>
        <translation>Dodaj &amp;katalog</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="219"/>
        <source>&amp;Exit</source>
        <translation>&amp;Wyjście</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="224"/>
        <source>About</source>
        <translation>O programie</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="229"/>
        <source>About Qt</source>
        <translation>&amp;O Qt</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="234"/>
        <source>&amp;Select All</source>
        <translation>&amp;Zaznacz wszystkie</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="239"/>
        <source>&amp;Remove Selected</source>
        <translation>&amp;Usuń zaznaczone</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="244"/>
        <source>&amp;Remove Unselected</source>
        <translation>Usuń &amp;niezaznaczone</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="254"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="259"/>
        <location filename="../qsuimainwindow.cpp" line="295"/>
        <source>Rename Playlist</source>
        <translation>Zmień nazwę listy</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="86"/>
        <source>&amp;Copy Selection To</source>
        <translation>Kopiuj zazna&amp;czenie do</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="295"/>
        <source>Playlist name:</source>
        <translation>Nazwa listy odtwarzania:</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="335"/>
        <source>Appearance</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="336"/>
        <source>Shortcuts</source>
        <translation>Skróty klawiszowe</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="415"/>
        <source>Menu Bar</source>
        <translation>Pasek menu</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="434"/>
        <source>Add new playlist</source>
        <translation>Dodaj nową listę odtwarzania</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="440"/>
        <source>Show all tabs</source>
        <translation>Pokaż wszystkie karty</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="467"/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="473"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="478"/>
        <source>Position</source>
        <translation>Pozycja</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="480"/>
        <source>Volume</source>
        <translation>Głośność</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="482"/>
        <source>Balance</source>
        <translation>Balans</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="485"/>
        <source>Quick Search</source>
        <translation>Szybkie wyszukiwanie</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="543"/>
        <source>Edit Toolbars</source>
        <translation>Edytuj paski narzędziowe</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="545"/>
        <source>Sort List</source>
        <translation>Sortuj listę</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="547"/>
        <location filename="../qsuimainwindow.cpp" line="563"/>
        <source>By Title</source>
        <translation>Według nazwy</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="548"/>
        <location filename="../qsuimainwindow.cpp" line="564"/>
        <source>By Album</source>
        <translation>Według nazwy albumu</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="549"/>
        <location filename="../qsuimainwindow.cpp" line="565"/>
        <source>By Artist</source>
        <translation>Według artysty</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="550"/>
        <location filename="../qsuimainwindow.cpp" line="566"/>
        <source>By Album Artist</source>
        <translation>Według artysty albumu</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="551"/>
        <location filename="../qsuimainwindow.cpp" line="567"/>
        <source>By Filename</source>
        <translation>Według nazwy pliku</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="552"/>
        <location filename="../qsuimainwindow.cpp" line="568"/>
        <source>By Path + Filename</source>
        <translation>Według ścieżki + nazwy pliku</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="553"/>
        <location filename="../qsuimainwindow.cpp" line="569"/>
        <source>By Date</source>
        <translation>Według daty</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="554"/>
        <location filename="../qsuimainwindow.cpp" line="570"/>
        <source>By Track Number</source>
        <translation>Według numeru utworu</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="555"/>
        <location filename="../qsuimainwindow.cpp" line="571"/>
        <source>By Disc Number</source>
        <translation>Według numeru płyty</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="556"/>
        <location filename="../qsuimainwindow.cpp" line="572"/>
        <source>By File Creation Date</source>
        <translation>Według daty utworzenia pliku</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="557"/>
        <location filename="../qsuimainwindow.cpp" line="573"/>
        <source>By File Modification Date</source>
        <translation>Według daty modyfikacji pliku</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="558"/>
        <source>By Group</source>
        <translation>Według grupy</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="561"/>
        <source>Sort Selection</source>
        <translation>Sortuj zaznaczone</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="577"/>
        <source>Randomize List</source>
        <translation>Tasuj listę</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="579"/>
        <source>Reverse List</source>
        <translation>Odwróć listę</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="620"/>
        <source>Actions</source>
        <translation>Akcje</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="938"/>
        <source>&amp;New PlayList</source>
        <translation>&amp;Nowa lista odtwarzania</translation>
    </message>
</context>
<context>
    <name>QSUiPlayListBrowser</name>
    <message>
        <location filename="../qsuiplaylistbrowser.cpp" line="62"/>
        <source>Quick Search</source>
        <translation>Szybkie wyszukiwanie</translation>
    </message>
</context>
<context>
    <name>QSUiPlayListHeader</name>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="54"/>
        <source>Add Column</source>
        <translation>Dodaj kolumnę</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="55"/>
        <source>Edit Column</source>
        <translation>Edytuj kolumnę</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="56"/>
        <source>Show Queue/Protocol</source>
        <translation>Pokaż kolejkę/protokół</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="58"/>
        <source>Auto-resize</source>
        <translation>Automatyczna zmiana rozmiaru</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="61"/>
        <source>Alignment</source>
        <translation>Wyrównanie</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="62"/>
        <source>Left</source>
        <comment>alignment</comment>
        <translation>Lewo</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="63"/>
        <source>Right</source>
        <comment>alignment</comment>
        <translation>Prawo</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="64"/>
        <source>Center</source>
        <comment>alignment</comment>
        <translation>Środek</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="74"/>
        <source>Remove Column</source>
        <translation>Usuń kolumnę</translation>
    </message>
</context>
<context>
    <name>QSUiPopupSettings</name>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="14"/>
        <source>Popup Information Settings</source>
        <translation>Ustawienia informacji popup</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="29"/>
        <source>Template</source>
        <translation>Szablon</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="58"/>
        <source>Reset</source>
        <translation>Resetuj</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="65"/>
        <source>Insert</source>
        <translation>Wstaw</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="75"/>
        <source>Show cover</source>
        <translation>Pokaż okładkę</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="89"/>
        <source>Cover size:</source>
        <translation>Rozmiar okładki:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="115"/>
        <source>Transparency:</source>
        <translation>Przezroczystość:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="145"/>
        <source>Delay:</source>
        <translation>Opóźnienie:</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="165"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
</context>
<context>
    <name>QSUiSettings</name>
    <message>
        <location filename="../qsuisettings.cpp" line="41"/>
        <source>Default</source>
        <translation>Domyślne</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="42"/>
        <source>16x16</source>
        <translation>16x16</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="43"/>
        <source>22x22</source>
        <translation>22x22</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="44"/>
        <source>32x32</source>
        <translation>32x32</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="45"/>
        <source>48x48</source>
        <translation>48x48</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="46"/>
        <source>64x64</source>
        <translation>64x64</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="48"/>
        <source>Top</source>
        <translation>Góra</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="49"/>
        <source>Bottom</source>
        <translation>Dół</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="50"/>
        <source>Left</source>
        <translation>Lewo</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="51"/>
        <source>Right</source>
        <translation>Prawo</translation>
    </message>
</context>
<context>
    <name>QSUiStatusBar</name>
    <message>
        <location filename="../qsuistatusbar.cpp" line="68"/>
        <source>tracks: %1</source>
        <translation>utwory: %1</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="69"/>
        <source>total time: %1</source>
        <translation>czas całkowity: %1</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="87"/>
        <source>Playing</source>
        <translation>Odtwarzanie</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="87"/>
        <source>Paused</source>
        <translation>Wstrzymane</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="102"/>
        <source>Buffering</source>
        <translation>Buforowanie</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="127"/>
        <source>Stopped</source>
        <translation>Zatrzymane</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="139"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="147"/>
        <source>Buffering: %1%</source>
        <translation>Buforowanie: %1%</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="152"/>
        <source>%1 bits</source>
        <translation>%1 bity</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="154"/>
        <source>mono</source>
        <translation>mono</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="156"/>
        <source>stereo</source>
        <translation>stereo</translation>
    </message>
    <message numerus="yes">
        <location filename="../qsuistatusbar.cpp" line="158"/>
        <source>%n channels</source>
        <translation>
            <numerusform>%n kanał</numerusform>
            <numerusform>%n kanały</numerusform>
            <numerusform>%n kanałów</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="159"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="164"/>
        <source>%1 kbps</source>
        <translation>%1 kbps</translation>
    </message>
</context>
<context>
    <name>QSUiWaveformSeekBar</name>
    <message>
        <location filename="../qsuiwaveformseekbar.cpp" line="335"/>
        <source>2 Channels</source>
        <translation>2 kanały</translation>
    </message>
    <message>
        <location filename="../qsuiwaveformseekbar.cpp" line="338"/>
        <source>RMS</source>
        <extracomment>Root mean square</extracomment>
        <translation>Średnia kwadratowa</translation>
    </message>
</context>
<context>
    <name>ToolBarEditor</name>
    <message>
        <location filename="../forms/toolbareditor.ui" line="14"/>
        <source>ToolBar Editor</source>
        <translation>Edytor paska narzędziowego</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="62"/>
        <source>Reset</source>
        <translation>Resetuj</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="199"/>
        <source>Toolbar:</source>
        <translation>Pasek narzędziowy:</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="222"/>
        <source>&amp;Create</source>
        <translation>&amp;Utwórz</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="238"/>
        <source>Re&amp;name</source>
        <translation>Z&amp;mień nazwę</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="254"/>
        <source>&amp;Remove</source>
        <translation>&amp;Usuń</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="100"/>
        <location filename="../toolbareditor.cpp" line="198"/>
        <source>Separator</source>
        <translation>Separator</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="248"/>
        <source>Toolbar</source>
        <translation>Pasek narzędziowy</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="250"/>
        <source>Toolbar %1</source>
        <translation>Pasek narzędziowy %1</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="264"/>
        <source>Rename Toolbar</source>
        <translation>Zmień nazwę paska narzędziowego</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="264"/>
        <source>Toolbar name:</source>
        <translation>Nazwa paska narzędziowego:</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../volumeslider.cpp" line="110"/>
        <source>%1: %2%</source>
        <translation>%1: %2%</translation>
    </message>
</context>
</TS>
