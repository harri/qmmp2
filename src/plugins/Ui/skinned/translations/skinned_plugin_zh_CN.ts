<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>SkinnedActionManager</name>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="39"/>
        <source>&amp;Play</source>
        <translation>播放(&amp;P)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="39"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="40"/>
        <source>&amp;Pause</source>
        <translation>暂停(&amp;P)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="40"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="41"/>
        <source>&amp;Stop</source>
        <translation>停止(&amp;S)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="41"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="42"/>
        <source>&amp;Previous</source>
        <translation>上一首(&amp;P)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="42"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="43"/>
        <source>&amp;Next</source>
        <translation>下一曲(&amp;N)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="43"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="44"/>
        <source>&amp;Play/Pause</source>
        <translation>播放/暂停(&amp;P)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="44"/>
        <source>Space</source>
        <translation>空格键</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="45"/>
        <source>&amp;Jump to Track</source>
        <translation>跳至单曲(&amp;J)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="45"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="46"/>
        <source>&amp;Repeat Playlist</source>
        <translation>重复播放列表(&amp;R)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="46"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="47"/>
        <source>&amp;Repeat Track</source>
        <translation>重复单曲(&amp;R)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="47"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="48"/>
        <source>&amp;Shuffle</source>
        <translation>乱序(&amp;S)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="48"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="49"/>
        <source>&amp;No Playlist Advance</source>
        <translation>播放列表中播放的曲目不自动前进(&amp;N)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="49"/>
        <source>Ctrl+N</source>
        <translation>Ctrl + N</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="50"/>
        <source>&amp;Stop After Selected</source>
        <translation>&amp;播完选定的曲目后停止播放</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="50"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="51"/>
        <source>&amp;Transit between playlists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="52"/>
        <source>&amp;Clear Queue</source>
        <translation>&amp;清除正在排队中的所有曲目</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="52"/>
        <source>Alt+Q</source>
        <translation>Alt+Q</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="54"/>
        <source>Show Playlist</source>
        <translation>显示播放列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="54"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="55"/>
        <source>Show Equalizer</source>
        <translation>显示均衡器</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="55"/>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="56"/>
        <source>Always on Top</source>
        <translation>总是在顶部显示</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="57"/>
        <source>Put on All Workspaces</source>
        <translation>在所有工作空间中显示</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="58"/>
        <source>Double Size</source>
        <translation>两倍尺寸</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="58"/>
        <source>Meta+D</source>
        <translation>Meta+D</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="59"/>
        <source>Anti-aliasing</source>
        <translation>抗锯齿图形保真</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="61"/>
        <source>Volume &amp;+</source>
        <translation>音量 &amp;+</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="61"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="62"/>
        <source>Volume &amp;-</source>
        <translation>音量 &amp;-</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="62"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="63"/>
        <source>&amp;Mute</source>
        <translation>&amp;静音</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="63"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="65"/>
        <source>&amp;Add File</source>
        <translation>&amp;添加文件</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="65"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="66"/>
        <source>&amp;Add Directory</source>
        <translation>&amp;添加文件夹</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="66"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="67"/>
        <source>&amp;Add Url</source>
        <translation>&amp;添加Url</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="67"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="68"/>
        <source>&amp;Remove Selected</source>
        <translation>&amp;移除所选项</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="68"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="69"/>
        <source>&amp;Remove All</source>
        <translation>&amp;移除所有文件</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="70"/>
        <source>&amp;Remove Unselected</source>
        <translation>&amp;移除未被选中项</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="71"/>
        <source>Remove unavailable files</source>
        <translation>移除已不存在的文件</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="72"/>
        <source>Remove duplicates</source>
        <translation>移除多余副本</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="73"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="74"/>
        <source>&amp;Queue Toggle</source>
        <translation>&amp;排队状态切换</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="74"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="75"/>
        <source>Invert Selection</source>
        <translation>反选</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="76"/>
        <source>&amp;Select None</source>
        <translation>&amp;什么也不选</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="77"/>
        <source>&amp;Select All</source>
        <translation>&amp;选择所有文件</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="77"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="78"/>
        <source>&amp;View Track Details</source>
        <translation>&amp;查看曲目详细内容</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="78"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="79"/>
        <source>&amp;New List</source>
        <translation>&amp;新的列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="79"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="80"/>
        <source>&amp;Delete List</source>
        <translation>&amp;删除列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="80"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="81"/>
        <source>&amp;Load List</source>
        <translation>&amp;载入列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="81"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="82"/>
        <source>&amp;Save List</source>
        <translation>&amp;保存列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="82"/>
        <source>Shift+S</source>
        <translation>Shift+S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="83"/>
        <source>&amp;Rename List</source>
        <translation>&amp;重命名列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="83"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="84"/>
        <source>&amp;Select Next Playlist</source>
        <translation>&amp;选择下一个播放列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="84"/>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+PgDown</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="85"/>
        <source>&amp;Select Previous Playlist</source>
        <translation>&amp;选择上一个播放列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="85"/>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+PgUp</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="86"/>
        <source>&amp;Show Playlists</source>
        <translation>&amp;显示播放列表</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="86"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="87"/>
        <source>&amp;Group Tracks</source>
        <translation>&amp;对曲目分组</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="87"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="88"/>
        <source>&amp;Show Column Headers</source>
        <translation>&amp;显示n列题眉</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="88"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="89"/>
        <source>Show &amp;Tab Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="89"/>
        <source>Alt+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="91"/>
        <source>&amp;Settings</source>
        <translation>设置(&amp;S)</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="91"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="92"/>
        <source>&amp;About</source>
        <translation>&amp;关于</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="93"/>
        <source>&amp;About Qt</source>
        <translation>&amp;关于Qt</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="94"/>
        <source>&amp;Exit</source>
        <translation>&amp;退出</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="94"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
</context>
<context>
    <name>SkinnedDisplay</name>
    <message>
        <location filename="../skinneddisplay.cpp" line="59"/>
        <source>Previous</source>
        <translation>上一个</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="63"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="66"/>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="69"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="72"/>
        <source>Next</source>
        <translation>下一首</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="75"/>
        <source>Play files</source>
        <translation>播放文件</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="80"/>
        <source>Equalizer</source>
        <translation>均衡器</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="82"/>
        <source>Playlist</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="85"/>
        <source>Repeat playlist</source>
        <translation>重复播放列表</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="87"/>
        <source>Shuffle</source>
        <translation>乱序</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="97"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="103"/>
        <source>Balance</source>
        <translation>平衡</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="290"/>
        <source>Volume: %1%</source>
        <translation>音量：%1%</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="294"/>
        <source>Balance: %1% right</source>
        <translation>平衡：%1% 右</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="296"/>
        <source>Balance: %1% left</source>
        <translation>平衡：%1% 左</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="298"/>
        <source>Balance: center</source>
        <translation>平衡：居中</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="304"/>
        <source>Seek to: %1</source>
        <translation>搜寻到：%1</translation>
    </message>
</context>
<context>
    <name>SkinnedEqWidget</name>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="47"/>
        <source>Equalizer</source>
        <translation>均衡器</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="160"/>
        <location filename="../skinnedeqwidget.cpp" line="177"/>
        <source>preset</source>
        <translation>预设</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="261"/>
        <source>&amp;Load/Delete</source>
        <translation>载入/删除(&amp;L)</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="263"/>
        <source>&amp;Save Preset</source>
        <translation>保存预设(&amp;S)</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="264"/>
        <source>&amp;Save Auto-load Preset</source>
        <translation>保存自动载入预设(&amp;S)</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="265"/>
        <source>&amp;Import</source>
        <translation>导入(&amp;I)</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="267"/>
        <source>&amp;Clear</source>
        <translation>清除(&amp;C)</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="296"/>
        <source>Saving Preset</source>
        <translation>保存预设</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="297"/>
        <source>Preset name:</source>
        <translation>预设名称：</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="298"/>
        <source>preset #</source>
        <translation>预设 #</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="394"/>
        <source>Import Preset</source>
        <translation>导入预设</translation>
    </message>
</context>
<context>
    <name>SkinnedFactory</name>
    <message>
        <location filename="../skinnedfactory.cpp" line="35"/>
        <source>Skinned User Interface</source>
        <translation>皮肤化用户界面</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="61"/>
        <source>About Qmmp Skinned User Interface</source>
        <translation>关于Qmmp皮肤化用户界面</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="62"/>
        <source>Qmmp Skinned User Interface</source>
        <translation>Qmmp皮肤化用户界面</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="63"/>
        <source>Simple user interface with Winamp-2.x/XMMS skins support</source>
        <translation>支持Winamp-2.x/XMMS皮肤的简单用户界面</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="64"/>
        <source>Written by:</source>
        <translation>作者：</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="65"/>
        <source>Vladimir Kuznetsov &lt;vovanec@gmail.com&gt;</source>
        <translation>Vladimir Kuznetsov &lt;vovanec@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="66"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="67"/>
        <source>Artwork:</source>
        <translation>艺术制作</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="68"/>
        <source>Andrey Adreev &lt;andreev00@gmail.com&gt;</source>
        <translation>Andrey Adreev &lt;andreev00@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="69"/>
        <source>sixsixfive &lt;http://sixsixfive.deviantart.com/&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SkinnedHotkeyEditor</name>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="33"/>
        <source>Change shortcut...</source>
        <translation>更改快捷键</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="44"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="58"/>
        <source>Action</source>
        <translation>行动</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="63"/>
        <source>Shortcut</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="57"/>
        <source>Playback</source>
        <translation>回放</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="63"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="69"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="75"/>
        <source>Playlist</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="81"/>
        <source>Misc</source>
        <translation>其他杂项</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="93"/>
        <source>Reset Shortcuts</source>
        <translation>重置快捷键</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="94"/>
        <source>Do you want to restore default shortcuts?</source>
        <translation>您希望将快捷键恢复到默认值吗？</translation>
    </message>
</context>
<context>
    <name>SkinnedMainWindow</name>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="358"/>
        <source>Appearance</source>
        <translation>外观</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="359"/>
        <source>Shortcuts</source>
        <translation>热键</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="433"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="442"/>
        <source>Playlist</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="461"/>
        <source>Audio</source>
        <translation>音频</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="469"/>
        <source>Tools</source>
        <translation>工具</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="541"/>
        <source>Qmmp</source>
        <translation>Qmmp</translation>
    </message>
</context>
<context>
    <name>SkinnedPlayList</name>
    <message>
        <location filename="../skinnedplaylist.cpp" line="56"/>
        <source>Playlist</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="193"/>
        <source>&amp;Copy Selection To</source>
        <translation>&amp;复制选项到</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="218"/>
        <source>Sort List</source>
        <translation>清单排序</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="221"/>
        <location filename="../skinnedplaylist.cpp" line="261"/>
        <source>By Title</source>
        <translation>按标题</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="224"/>
        <location filename="../skinnedplaylist.cpp" line="264"/>
        <source>By Album</source>
        <translation>按专辑</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="227"/>
        <location filename="../skinnedplaylist.cpp" line="267"/>
        <source>By Artist</source>
        <translation>按艺术家</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="230"/>
        <location filename="../skinnedplaylist.cpp" line="270"/>
        <source>By Album Artist</source>
        <translation>按专辑 艺术家</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="233"/>
        <location filename="../skinnedplaylist.cpp" line="273"/>
        <source>By Filename</source>
        <translation>按文件名称</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="236"/>
        <location filename="../skinnedplaylist.cpp" line="276"/>
        <source>By Path + Filename</source>
        <translation>按路径+文件名称</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="239"/>
        <location filename="../skinnedplaylist.cpp" line="279"/>
        <source>By Date</source>
        <translation>按日期</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="242"/>
        <location filename="../skinnedplaylist.cpp" line="282"/>
        <source>By Track Number</source>
        <translation>按单曲号</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="245"/>
        <location filename="../skinnedplaylist.cpp" line="285"/>
        <source>By Disc Number</source>
        <translation>按光盘号</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="248"/>
        <location filename="../skinnedplaylist.cpp" line="288"/>
        <source>By File Creation Date</source>
        <translation>按文件的创建日期</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="251"/>
        <location filename="../skinnedplaylist.cpp" line="291"/>
        <source>By File Modification Date</source>
        <translation>按文件的修改日期</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="254"/>
        <source>By Group</source>
        <translation>按组</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="259"/>
        <source>Sort Selection</source>
        <translation>排序选择</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="296"/>
        <source>Randomize List</source>
        <translation>随机列表</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="298"/>
        <source>Reverse List</source>
        <translation>反序列表</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="306"/>
        <source>Actions</source>
        <translation>行动</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="576"/>
        <source>Rename Playlist</source>
        <translation>重命名播放列表</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="576"/>
        <source>Playlist name:</source>
        <translation>播放列表名称：</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="597"/>
        <source>&amp;New PlayList</source>
        <translation>&amp;新播放列表</translation>
    </message>
</context>
<context>
    <name>SkinnedPlayListBrowser</name>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="14"/>
        <source>Playlist Browser</source>
        <translation>播放列表浏览器</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="31"/>
        <source>Filter:</source>
        <translation>筛选器：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="47"/>
        <source>New</source>
        <translation>新的</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="54"/>
        <location filename="../skinnedplaylistbrowser.cpp" line="45"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="61"/>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="71"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistbrowser.cpp" line="44"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
</context>
<context>
    <name>SkinnedPlayListHeader</name>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="77"/>
        <source>Add Column</source>
        <translation>增加n列</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="78"/>
        <source>Edit Column</source>
        <translation>编辑n列</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="79"/>
        <source>Show Queue/Protocol</source>
        <translation>显示排队/协议</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="81"/>
        <source>Auto-resize</source>
        <translation>自动调整大小</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="84"/>
        <source>Alignment</source>
        <translation>对齐</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="85"/>
        <source>Left</source>
        <comment>alignment</comment>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="86"/>
        <source>Right</source>
        <comment>alignment</comment>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="87"/>
        <source>Center</source>
        <comment>alignment</comment>
        <translation>居中</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="97"/>
        <source>Remove Column</source>
        <translation>移除n列</translation>
    </message>
</context>
<context>
    <name>SkinnedPopupSettings</name>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="14"/>
        <source>Popup Information Settings</source>
        <translation>弹出信息设置</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="29"/>
        <source>Template</source>
        <translation>模板</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="58"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="65"/>
        <source>Insert</source>
        <translation>插入</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="75"/>
        <source>Show cover</source>
        <translation>显示封面</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="89"/>
        <source>Cover size:</source>
        <translation>封面尺寸：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="115"/>
        <source>Transparency:</source>
        <translation>透明：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="145"/>
        <source>Delay:</source>
        <translation>延迟：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="178"/>
        <source>ms</source>
        <translation>毫秒</translation>
    </message>
</context>
<context>
    <name>SkinnedPresetEditor</name>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="14"/>
        <source>Preset Editor</source>
        <translation>预设编辑器</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="36"/>
        <source>Preset</source>
        <translation>预设</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="61"/>
        <source>Auto-preset</source>
        <translation>自动预设</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="95"/>
        <source>Load</source>
        <translation>载入</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="102"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>SkinnedSettings</name>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="24"/>
        <source>Skins</source>
        <translation>皮肤</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="55"/>
        <source>Add...</source>
        <translation>增加......</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="72"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="103"/>
        <source>Main Window</source>
        <translation>主窗口</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="109"/>
        <source>Hide on close</source>
        <translation>点击关闭后隐藏</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="116"/>
        <source>Start hidden</source>
        <translation>启动后隐藏</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="123"/>
        <source>Use skin cursors</source>
        <translation>使用皮肤光标</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="261"/>
        <source>Single Column Mode</source>
        <translation>单列模式</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="308"/>
        <source>Show splitters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="315"/>
        <source>Alternate splitter color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="415"/>
        <source>Colors</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="421"/>
        <source>Playlist Colors</source>
        <translation>播放列表颜色</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="427"/>
        <source>Use skin colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="545"/>
        <source>Background #2:</source>
        <translation>背景#2：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="609"/>
        <source>Highlighted background:</source>
        <translation>高亮显示的背景：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="500"/>
        <source>Normal text:</source>
        <translation>正常文本：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="763"/>
        <source>Splitter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="577"/>
        <source>Current text:</source>
        <translation>当前文本：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="641"/>
        <source>Highlighted text:</source>
        <translation>高亮显示的文本：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="813"/>
        <source>Current track background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="871"/>
        <source>Override current track background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="778"/>
        <source>Group background:</source>
        <translation>分组背景：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="878"/>
        <source>Override group background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="700"/>
        <source>Group text:</source>
        <translation>分组文本：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="455"/>
        <source>Background #1:</source>
        <translation>背景#1：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="691"/>
        <source>Load skin colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="904"/>
        <source>Fonts</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="958"/>
        <source>Playlist:</source>
        <translation>播放列表：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="994"/>
        <source>Groups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1021"/>
        <source>Extra group row:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1048"/>
        <source>Column headers:</source>
        <translation>n列题眉：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="916"/>
        <source>Player:</source>
        <translation>播放器：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="938"/>
        <location filename="../forms/skinnedsettings.ui" line="980"/>
        <location filename="../forms/skinnedsettings.ui" line="1061"/>
        <source>???</source>
        <translation>？？？</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="142"/>
        <location filename="../forms/skinnedsettings.ui" line="945"/>
        <location filename="../forms/skinnedsettings.ui" line="987"/>
        <location filename="../forms/skinnedsettings.ui" line="1068"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1103"/>
        <source>Reset fonts</source>
        <translation>重置字体</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1112"/>
        <source>Use bitmap font if available</source>
        <translation>如有则使用点阵字体</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="132"/>
        <source>Window title format:</source>
        <translation>窗口题目格式：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="97"/>
        <source>General</source>
        <translation>总的</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="154"/>
        <source>Transparency</source>
        <translation>透明度</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="160"/>
        <source>Main window</source>
        <translation>主窗口</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="183"/>
        <location filename="../forms/skinnedsettings.ui" line="207"/>
        <location filename="../forms/skinnedsettings.ui" line="231"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="190"/>
        <source>Equalizer</source>
        <translation>均衡器</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="214"/>
        <source>Playlist</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="255"/>
        <source>Song Display</source>
        <translation>歌曲显示</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="294"/>
        <source>Show protocol</source>
        <translation>显示协议</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="274"/>
        <source>Show song lengths</source>
        <translation>显示歌曲长度</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="267"/>
        <source>Show song numbers</source>
        <translation>显示歌曲号</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="284"/>
        <source>Align song numbers</source>
        <translation>排列歌曲号</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="301"/>
        <source>Show anchor</source>
        <translation>显示定位锚</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="363"/>
        <source>Show popup information</source>
        <translation>显示弹出信息</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="375"/>
        <source>Edit template</source>
        <translation>编辑模板</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="331"/>
        <source>Playlist separator:</source>
        <translation>播放列表分隔符：</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="322"/>
        <source>Show &apos;New Playlist&apos; button</source>
        <translation>显示’新播放列表中‘按钮</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="81"/>
        <source>Select Skin Files</source>
        <translation>选择皮肤文件</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="82"/>
        <source>Skin files</source>
        <translation>皮肤文件</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="165"/>
        <source>Default skin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="174"/>
        <source>Unarchived skin %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="174"/>
        <source>Archived skin %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SkinnedTextScroller</name>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="54"/>
        <source>Autoscroll Songname</source>
        <translation>自动滚动曲目名</translation>
    </message>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="55"/>
        <source>Transparent Background</source>
        <translation>透明的背景</translation>
    </message>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="122"/>
        <source>Buffering: %1%</source>
        <translation>缓冲中：%1%</translation>
    </message>
</context>
<context>
    <name>SkinnedVisualization</name>
    <message>
        <location filename="../skinnedvisualization.cpp" line="211"/>
        <source>Visualization Mode</source>
        <translation>可视化模式</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="214"/>
        <source>Analyzer</source>
        <translation>分析器</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="215"/>
        <source>Scope</source>
        <translation>示波器</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="216"/>
        <source>Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="223"/>
        <source>Analyzer Mode</source>
        <translation>分析模式</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="226"/>
        <source>Normal</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="227"/>
        <source>Fire</source>
        <translation>火花</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="228"/>
        <source>Vertical Lines</source>
        <translation>垂直线</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="229"/>
        <source>Lines</source>
        <translation>线形</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="230"/>
        <source>Bars</source>
        <translation>条形</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="243"/>
        <source>Peaks</source>
        <translation>峰</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="247"/>
        <source>Refresh Rate</source>
        <translation>刷新率</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="250"/>
        <source>50 fps</source>
        <translation>50 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="251"/>
        <source>25 fps</source>
        <translation>25 帧每秒</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="252"/>
        <source>10 fps</source>
        <translation>10 帧每秒</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="253"/>
        <source>5 fps</source>
        <translation>5 帧每秒</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="260"/>
        <source>Analyzer Falloff</source>
        <translation>分析器坠落</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="263"/>
        <location filename="../skinnedvisualization.cpp" line="277"/>
        <source>Slowest</source>
        <translation>最慢</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="264"/>
        <location filename="../skinnedvisualization.cpp" line="278"/>
        <source>Slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="265"/>
        <location filename="../skinnedvisualization.cpp" line="279"/>
        <source>Medium</source>
        <translation>中等</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="266"/>
        <location filename="../skinnedvisualization.cpp" line="280"/>
        <source>Fast</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="267"/>
        <location filename="../skinnedvisualization.cpp" line="281"/>
        <source>Fastest</source>
        <translation>最快</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="274"/>
        <source>Peaks Falloff</source>
        <translation>顶峰坠落</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="287"/>
        <source>Background</source>
        <translation>背景</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="288"/>
        <source>Transparent</source>
        <translation>透明</translation>
    </message>
</context>
</TS>
