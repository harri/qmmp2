<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>SkinnedActionManager</name>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="39"/>
        <source>&amp;Play</source>
        <translation>&amp;Reproducir</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="39"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="40"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pausar</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="40"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="41"/>
        <source>&amp;Stop</source>
        <translation>&amp;Detener</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="41"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="42"/>
        <source>&amp;Previous</source>
        <translation>&amp;Previo</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="42"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="43"/>
        <source>&amp;Next</source>
        <translation>&amp;Siguiente</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="43"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="44"/>
        <source>&amp;Play/Pause</source>
        <translation>&amp;Reproducir/Pausar</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="44"/>
        <source>Space</source>
        <translation>Espacio</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="45"/>
        <source>&amp;Jump to Track</source>
        <translation>&amp;Ir a la Pista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="45"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="46"/>
        <source>&amp;Repeat Playlist</source>
        <translation>&amp;Repetir la lista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="46"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="47"/>
        <source>&amp;Repeat Track</source>
        <translation>&amp;Repetir pista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="47"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="48"/>
        <source>&amp;Shuffle</source>
        <translation>&amp;Revolver</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="48"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="49"/>
        <source>&amp;No Playlist Advance</source>
        <translation>&amp;No avanzar en la lista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="49"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="50"/>
        <source>&amp;Stop After Selected</source>
        <translation>&amp;Parar tras los seleccionados</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="50"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="51"/>
        <source>&amp;Transit between playlists</source>
        <translation>&amp;Tránsito entre listas de reproducción</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="52"/>
        <source>&amp;Clear Queue</source>
        <translation>&amp;Limpiar la Cola</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="52"/>
        <source>Alt+Q</source>
        <translation>Alt+Q</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="54"/>
        <source>Show Playlist</source>
        <translation>Mostrar la lista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="54"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="55"/>
        <source>Show Equalizer</source>
        <translation>Mostrar el ecualizador</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="55"/>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="56"/>
        <source>Always on Top</source>
        <translation>Siempre encima</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="57"/>
        <source>Put on All Workspaces</source>
        <translation>Ver en todos los escritorios</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="58"/>
        <source>Double Size</source>
        <translation>Tamaño doble</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="58"/>
        <source>Meta+D</source>
        <translation>Meta+D</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="59"/>
        <source>Anti-aliasing</source>
        <translation>Anti-aliasing</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="61"/>
        <source>Volume &amp;+</source>
        <translation>Volumen &amp;+</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="61"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="62"/>
        <source>Volume &amp;-</source>
        <translation>Volumen &amp;-</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="62"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="63"/>
        <source>&amp;Mute</source>
        <translation>&amp;Silenciar</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="63"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="65"/>
        <source>&amp;Add File</source>
        <translation>&amp;Añadir archivo</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="65"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="66"/>
        <source>&amp;Add Directory</source>
        <translation>&amp;Añadir directorio</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="66"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="67"/>
        <source>&amp;Add Url</source>
        <translation>&amp;Añadir URL</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="67"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="68"/>
        <source>&amp;Remove Selected</source>
        <translation>&amp;Eliminar los seleccionados</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="68"/>
        <source>Del</source>
        <translation>Supr</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="69"/>
        <source>&amp;Remove All</source>
        <translation>&amp;Eliminar todo</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="70"/>
        <source>&amp;Remove Unselected</source>
        <translation>&amp;Eliminar los no seleccionados</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="71"/>
        <source>Remove unavailable files</source>
        <translation>Eliminar los archivos no disponibles</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="72"/>
        <source>Remove duplicates</source>
        <translation>Eliminar los duplicados</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="73"/>
        <source>Refresh</source>
        <translation>Refrescar</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="74"/>
        <source>&amp;Queue Toggle</source>
        <translation>&amp;Formar/desformar</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="74"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="75"/>
        <source>Invert Selection</source>
        <translation>Invertir la selección</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="76"/>
        <source>&amp;Select None</source>
        <translation>&amp;No seleccionar nada</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="77"/>
        <source>&amp;Select All</source>
        <translation>&amp;Seleccionar todo</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="77"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="78"/>
        <source>&amp;View Track Details</source>
        <translation>&amp;Ver detalles de la pista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="78"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="79"/>
        <source>&amp;New List</source>
        <translation>&amp;Lista nueva</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="79"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="80"/>
        <source>&amp;Delete List</source>
        <translation>&amp;Borrar la lista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="80"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="81"/>
        <source>&amp;Load List</source>
        <translation>&amp;Cargar una lista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="81"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="82"/>
        <source>&amp;Save List</source>
        <translation>&amp;Guardar la lista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="82"/>
        <source>Shift+S</source>
        <translation>Shift+S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="83"/>
        <source>&amp;Rename List</source>
        <translation>&amp;Renombrar Lista</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="83"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="84"/>
        <source>&amp;Select Next Playlist</source>
        <translation>&amp;Seleccionar la lista siguiente</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="84"/>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+AvPág</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="85"/>
        <source>&amp;Select Previous Playlist</source>
        <translation>&amp;Seleccionar la lista anterior</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="85"/>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+RePág</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="86"/>
        <source>&amp;Show Playlists</source>
        <translation>&amp;Mostrar las listas</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="86"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="87"/>
        <source>&amp;Group Tracks</source>
        <translation>A&amp;grupar pistas</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="87"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="88"/>
        <source>&amp;Show Column Headers</source>
        <translation>Mostrar Cabecera&amp;s de Columnas</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="88"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="89"/>
        <source>Show &amp;Tab Bar</source>
        <translation>Mostrar Barra de &amp;Pestañas</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="89"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="91"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="91"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="92"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="93"/>
        <source>&amp;About Qt</source>
        <translation>&amp;Acerca de Qt</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="94"/>
        <source>&amp;Exit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="94"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
</context>
<context>
    <name>SkinnedDisplay</name>
    <message>
        <location filename="../skinneddisplay.cpp" line="59"/>
        <source>Previous</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="63"/>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="66"/>
        <source>Pause</source>
        <translation>Pausar</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="69"/>
        <source>Stop</source>
        <translation>Detener</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="72"/>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="75"/>
        <source>Play files</source>
        <translation>Reproducir archivos</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="80"/>
        <source>Equalizer</source>
        <translation>Ecualizador</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="82"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="85"/>
        <source>Repeat playlist</source>
        <translation>Repetir la lista de reproducción</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="87"/>
        <source>Shuffle</source>
        <translation>Revolver</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="97"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="103"/>
        <source>Balance</source>
        <translation>Balance</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="290"/>
        <source>Volume: %1%</source>
        <translation>Volumen: %1%</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="294"/>
        <source>Balance: %1% right</source>
        <translation>Balance: %1% derecha</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="296"/>
        <source>Balance: %1% left</source>
        <translation>Balance: %1% izquierda</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="298"/>
        <source>Balance: center</source>
        <translation>Balance: centro</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="304"/>
        <source>Seek to: %1</source>
        <translation>Ir hasta: %1</translation>
    </message>
</context>
<context>
    <name>SkinnedEqWidget</name>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="47"/>
        <source>Equalizer</source>
        <translation>Ecualizador</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="160"/>
        <location filename="../skinnedeqwidget.cpp" line="177"/>
        <source>preset</source>
        <translation>preprogramado</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="261"/>
        <source>&amp;Load/Delete</source>
        <translation>&amp;Cargar/Eliminar</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="263"/>
        <source>&amp;Save Preset</source>
        <translation>&amp;Guardar preprogramado</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="264"/>
        <source>&amp;Save Auto-load Preset</source>
        <translation>&amp;Guardar preprogramado autocargable</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="265"/>
        <source>&amp;Import</source>
        <translation>&amp;Importar</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="267"/>
        <source>&amp;Clear</source>
        <translation>&amp;Limpiar</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="296"/>
        <source>Saving Preset</source>
        <translation>Guardando preprogramado</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="297"/>
        <source>Preset name:</source>
        <translation>Nombre del preprogramado:</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="298"/>
        <source>preset #</source>
        <translation>Preprogramado nº</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="394"/>
        <source>Import Preset</source>
        <translation>Importar preprogramado</translation>
    </message>
</context>
<context>
    <name>SkinnedFactory</name>
    <message>
        <location filename="../skinnedfactory.cpp" line="35"/>
        <source>Skinned User Interface</source>
        <translation>Interfaz de Usuario con Piel</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="61"/>
        <source>About Qmmp Skinned User Interface</source>
        <translation>Acerca de la Interfaz de Usuario con Piel</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="62"/>
        <source>Qmmp Skinned User Interface</source>
        <translation>Acerca de la Interfaz de Usuario con Piel de Qmmp</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="63"/>
        <source>Simple user interface with Winamp-2.x/XMMS skins support</source>
        <translation>Interfaz de usuario simplificada con soporte para pieles para Winamp-2.x/XMMS</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="64"/>
        <source>Written by:</source>
        <translation>Escrito por:</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="65"/>
        <source>Vladimir Kuznetsov &lt;vovanec@gmail.com&gt;</source>
        <translation>Vladimir Kuznetsov &lt;vovanec@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="66"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="67"/>
        <source>Artwork:</source>
        <translation>Arte:</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="68"/>
        <source>Andrey Adreev &lt;andreev00@gmail.com&gt;</source>
        <translation>Andrey Adreev &lt;andreev00@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="69"/>
        <source>sixsixfive &lt;http://sixsixfive.deviantart.com/&gt;</source>
        <translation>sixsixfive &lt;http://sixsixfive.deviantart.com/&gt;</translation>
    </message>
</context>
<context>
    <name>SkinnedHotkeyEditor</name>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="33"/>
        <source>Change shortcut...</source>
        <translation>Cambiar atajo...</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="44"/>
        <source>Reset</source>
        <translation>Restaurar</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="58"/>
        <source>Action</source>
        <translation>Acción</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="63"/>
        <source>Shortcut</source>
        <translation>Atajo</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="57"/>
        <source>Playback</source>
        <translation>Reproducción</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="63"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="69"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="75"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="81"/>
        <source>Misc</source>
        <translation>Varios</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="93"/>
        <source>Reset Shortcuts</source>
        <translation>Restaurar Atajos</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="94"/>
        <source>Do you want to restore default shortcuts?</source>
        <translation>¿Desea restaurar los atajos predeterminados?</translation>
    </message>
</context>
<context>
    <name>SkinnedMainWindow</name>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="358"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="359"/>
        <source>Shortcuts</source>
        <translation>Atajos</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="433"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="442"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="461"/>
        <source>Audio</source>
        <translation>Sonido</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="469"/>
        <source>Tools</source>
        <translation>Herramientas</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="541"/>
        <source>Qmmp</source>
        <translation>Qmmp</translation>
    </message>
</context>
<context>
    <name>SkinnedPlayList</name>
    <message>
        <location filename="../skinnedplaylist.cpp" line="56"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="193"/>
        <source>&amp;Copy Selection To</source>
        <translation>&amp;Copiar la selección a</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="218"/>
        <source>Sort List</source>
        <translation>Ordenar la lista</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="221"/>
        <location filename="../skinnedplaylist.cpp" line="261"/>
        <source>By Title</source>
        <translation>Por título</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="224"/>
        <location filename="../skinnedplaylist.cpp" line="264"/>
        <source>By Album</source>
        <translation>Por álbum</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="227"/>
        <location filename="../skinnedplaylist.cpp" line="267"/>
        <source>By Artist</source>
        <translation>Por intérprete</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="230"/>
        <location filename="../skinnedplaylist.cpp" line="270"/>
        <source>By Album Artist</source>
        <translation>Por Artista del Álbum</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="233"/>
        <location filename="../skinnedplaylist.cpp" line="273"/>
        <source>By Filename</source>
        <translation>Por nombre de archivo</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="236"/>
        <location filename="../skinnedplaylist.cpp" line="276"/>
        <source>By Path + Filename</source>
        <translation>Por ruta + nombre</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="239"/>
        <location filename="../skinnedplaylist.cpp" line="279"/>
        <source>By Date</source>
        <translation>Por fecha</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="242"/>
        <location filename="../skinnedplaylist.cpp" line="282"/>
        <source>By Track Number</source>
        <translation>Por número de pista</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="245"/>
        <location filename="../skinnedplaylist.cpp" line="285"/>
        <source>By Disc Number</source>
        <translation>Por Número de Disco</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="248"/>
        <location filename="../skinnedplaylist.cpp" line="288"/>
        <source>By File Creation Date</source>
        <translation>Por Fecha de Creación de Archivo</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="251"/>
        <location filename="../skinnedplaylist.cpp" line="291"/>
        <source>By File Modification Date</source>
        <translation>Por Fecha de Modificación de Archivo</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="254"/>
        <source>By Group</source>
        <translation>Por Grupo</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="259"/>
        <source>Sort Selection</source>
        <translation>Ordenar la selección</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="296"/>
        <source>Randomize List</source>
        <translation>Lista aleatoria</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="298"/>
        <source>Reverse List</source>
        <translation>Invertir la lista</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="306"/>
        <source>Actions</source>
        <translation>Acciones</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="576"/>
        <source>Rename Playlist</source>
        <translation>Renombrar lista</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="576"/>
        <source>Playlist name:</source>
        <translation>Nombre de la lista:</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="597"/>
        <source>&amp;New PlayList</source>
        <translation>&amp;Lista nueva</translation>
    </message>
</context>
<context>
    <name>SkinnedPlayListBrowser</name>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="14"/>
        <source>Playlist Browser</source>
        <translation>Navegador de listas de reproducción</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="31"/>
        <source>Filter:</source>
        <translation>Filtro:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="47"/>
        <source>New</source>
        <translation>Nueva</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="54"/>
        <location filename="../skinnedplaylistbrowser.cpp" line="45"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="61"/>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="71"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistbrowser.cpp" line="44"/>
        <source>Rename</source>
        <translation>Renombrar</translation>
    </message>
</context>
<context>
    <name>SkinnedPlayListHeader</name>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="77"/>
        <source>Add Column</source>
        <translation>Agregar columna</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="78"/>
        <source>Edit Column</source>
        <translation>Editar Columna</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="79"/>
        <source>Show Queue/Protocol</source>
        <translation>Mostrar Cola/Protocolo</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="81"/>
        <source>Auto-resize</source>
        <translation>Auto-redimensionar</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="84"/>
        <source>Alignment</source>
        <translation>Alineación</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="85"/>
        <source>Left</source>
        <comment>alignment</comment>
        <translation>Izquierda</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="86"/>
        <source>Right</source>
        <comment>alignment</comment>
        <translation>Derecha</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="87"/>
        <source>Center</source>
        <comment>alignment</comment>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="97"/>
        <source>Remove Column</source>
        <translation>Quitar Columna</translation>
    </message>
</context>
<context>
    <name>SkinnedPopupSettings</name>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="14"/>
        <source>Popup Information Settings</source>
        <translation>Configuración de información emergente</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="29"/>
        <source>Template</source>
        <translation>Plantilla</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="58"/>
        <source>Reset</source>
        <translation>Restaurar</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="65"/>
        <source>Insert</source>
        <translation>Insertar</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="75"/>
        <source>Show cover</source>
        <translation>Mostrar carátula</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="89"/>
        <source>Cover size:</source>
        <translation>Tamaño de carátula:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="115"/>
        <source>Transparency:</source>
        <translation>Transparencia:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="145"/>
        <source>Delay:</source>
        <translation>Retardo:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="178"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
</context>
<context>
    <name>SkinnedPresetEditor</name>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="14"/>
        <source>Preset Editor</source>
        <translation>Editor de preprogamados</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="36"/>
        <source>Preset</source>
        <translation>Preprogramado</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="61"/>
        <source>Auto-preset</source>
        <translation>Automático</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="95"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="102"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
</context>
<context>
    <name>SkinnedSettings</name>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="24"/>
        <source>Skins</source>
        <translation>Pieles</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="55"/>
        <source>Add...</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="72"/>
        <source>Refresh</source>
        <translation>Refrescar</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="103"/>
        <source>Main Window</source>
        <translation>Ventana Principal</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="109"/>
        <source>Hide on close</source>
        <translation>Esconder al cerrar</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="116"/>
        <source>Start hidden</source>
        <translation>Iniciar escondido</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="123"/>
        <source>Use skin cursors</source>
        <translation>Usar cursores de piel</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="261"/>
        <source>Single Column Mode</source>
        <translation>Modalidad de Columna Única</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="308"/>
        <source>Show splitters</source>
        <translation>Mostrar divisores</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="315"/>
        <source>Alternate splitter color</source>
        <translation>Color alternativo de divisor</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="415"/>
        <source>Colors</source>
        <translation>Colores</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="421"/>
        <source>Playlist Colors</source>
        <translation>Colores de Listas de Reproducción</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="427"/>
        <source>Use skin colors</source>
        <translation>Utilizar colores de piel</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="545"/>
        <source>Background #2:</source>
        <translation>Fondo #2:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="609"/>
        <source>Highlighted background:</source>
        <translation>Fondo resaltado:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="500"/>
        <source>Normal text:</source>
        <translation>Texto normal:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="763"/>
        <source>Splitter:</source>
        <translation>Divisor:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="577"/>
        <source>Current text:</source>
        <translation>Texto actual:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="641"/>
        <source>Highlighted text:</source>
        <translation>Texto resaltado:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="813"/>
        <source>Current track background:</source>
        <translation>Fondo de la pista actual</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="871"/>
        <source>Override current track background</source>
        <translation>Anular el fondo de la pista actual</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="778"/>
        <source>Group background:</source>
        <translation>Fondo del Grupo:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="878"/>
        <source>Override group background</source>
        <translation>Anular fondo de grupo</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="700"/>
        <source>Group text:</source>
        <translation>Texto del Grupo:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="455"/>
        <source>Background #1:</source>
        <translation>Fondo #1:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="691"/>
        <source>Load skin colors</source>
        <translation>Cargar colores de piel</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="904"/>
        <source>Fonts</source>
        <translation>Tipografías</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="958"/>
        <source>Playlist:</source>
        <translation>Lista de Reproducción:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="994"/>
        <source>Groups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1021"/>
        <source>Extra group row:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1048"/>
        <source>Column headers:</source>
        <translation>Cabeceras de columna:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="916"/>
        <source>Player:</source>
        <translation>Reproductor</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="938"/>
        <location filename="../forms/skinnedsettings.ui" line="980"/>
        <location filename="../forms/skinnedsettings.ui" line="1061"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="142"/>
        <location filename="../forms/skinnedsettings.ui" line="945"/>
        <location filename="../forms/skinnedsettings.ui" line="987"/>
        <location filename="../forms/skinnedsettings.ui" line="1068"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1103"/>
        <source>Reset fonts</source>
        <translation>Restaurar tipografías</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1112"/>
        <source>Use bitmap font if available</source>
        <translation>Usar tipografías de mapa de bits si es posible</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="132"/>
        <source>Window title format:</source>
        <translation>Formato del título de la ventana:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="97"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="154"/>
        <source>Transparency</source>
        <translation>Transparencia</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="160"/>
        <source>Main window</source>
        <translation>Ventana principal</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="183"/>
        <location filename="../forms/skinnedsettings.ui" line="207"/>
        <location filename="../forms/skinnedsettings.ui" line="231"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="190"/>
        <source>Equalizer</source>
        <translation>Ecualizador</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="214"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="255"/>
        <source>Song Display</source>
        <translation>Mostrar Canción</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="294"/>
        <source>Show protocol</source>
        <translation>Mostrar protocolo</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="274"/>
        <source>Show song lengths</source>
        <translation>Mostrar tamaños de canciones</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="267"/>
        <source>Show song numbers</source>
        <translation>Mostrar números de canción</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="284"/>
        <source>Align song numbers</source>
        <translation>Alinear número de canciones</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="301"/>
        <source>Show anchor</source>
        <translation>Mostrar anclaje</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="363"/>
        <source>Show popup information</source>
        <translation>Mostrar información en ventana emergente</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="375"/>
        <source>Edit template</source>
        <translation>Editar la plantilla</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="331"/>
        <source>Playlist separator:</source>
        <translation>Separador de listas de reproducción</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="322"/>
        <source>Show &apos;New Playlist&apos; button</source>
        <translation>Mostrar botón &apos;Nueva Lista de Reproducción&apos;</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="81"/>
        <source>Select Skin Files</source>
        <translation>Seleccionar Archivos de Pieles</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="82"/>
        <source>Skin files</source>
        <translation>Archivos de Pieles</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="165"/>
        <source>Default skin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="174"/>
        <source>Unarchived skin %1</source>
        <translation>Piel sin archivar %1</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="174"/>
        <source>Archived skin %1</source>
        <translation>Piel archivada %1</translation>
    </message>
</context>
<context>
    <name>SkinnedTextScroller</name>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="54"/>
        <source>Autoscroll Songname</source>
        <translation>Autodesplazar el nombre de la canción</translation>
    </message>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="55"/>
        <source>Transparent Background</source>
        <translation>Fondo Transparente</translation>
    </message>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="122"/>
        <source>Buffering: %1%</source>
        <translation>Cargando: %1%</translation>
    </message>
</context>
<context>
    <name>SkinnedVisualization</name>
    <message>
        <location filename="../skinnedvisualization.cpp" line="211"/>
        <source>Visualization Mode</source>
        <translation>Modo de visualización</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="214"/>
        <source>Analyzer</source>
        <translation>Analizador</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="215"/>
        <source>Scope</source>
        <translation>Osciloscopio</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="216"/>
        <source>Off</source>
        <translation>Apagado</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="223"/>
        <source>Analyzer Mode</source>
        <translation>Modo del analizador</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="226"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="227"/>
        <source>Fire</source>
        <translation>Fuego</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="228"/>
        <source>Vertical Lines</source>
        <translation>Líneas verticales</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="229"/>
        <source>Lines</source>
        <translation>Líneas</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="230"/>
        <source>Bars</source>
        <translation>Barras</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="243"/>
        <source>Peaks</source>
        <translation>Picos</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="247"/>
        <source>Refresh Rate</source>
        <translation>Velocidad de actualización</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="250"/>
        <source>50 fps</source>
        <translation>50 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="251"/>
        <source>25 fps</source>
        <translation>25 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="252"/>
        <source>10 fps</source>
        <translation>10 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="253"/>
        <source>5 fps</source>
        <translation>5 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="260"/>
        <source>Analyzer Falloff</source>
        <translation>Caída del analizador</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="263"/>
        <location filename="../skinnedvisualization.cpp" line="277"/>
        <source>Slowest</source>
        <translation>Muy lenta</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="264"/>
        <location filename="../skinnedvisualization.cpp" line="278"/>
        <source>Slow</source>
        <translation>Lenta</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="265"/>
        <location filename="../skinnedvisualization.cpp" line="279"/>
        <source>Medium</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="266"/>
        <location filename="../skinnedvisualization.cpp" line="280"/>
        <source>Fast</source>
        <translation>Rápida</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="267"/>
        <location filename="../skinnedvisualization.cpp" line="281"/>
        <source>Fastest</source>
        <translation>Muy rápida</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="274"/>
        <source>Peaks Falloff</source>
        <translation>Caída de picos</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="287"/>
        <source>Background</source>
        <translation>Fondo</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="288"/>
        <source>Transparent</source>
        <translation>Transparente</translation>
    </message>
</context>
</TS>
