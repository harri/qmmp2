<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>Analyzer</name>
    <message>
        <location filename="../analyzer.cpp" line="36"/>
        <source>Qmmp Analyzer</source>
        <translation>Analyzer Qmmp</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="320"/>
        <source>Peaks</source>
        <translation>Puncak</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="323"/>
        <source>Refresh Rate</source>
        <translation>Segarkan Laju</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="326"/>
        <source>50 fps</source>
        <translation>50 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="327"/>
        <source>25 fps</source>
        <translation>25 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="328"/>
        <source>10 fps</source>
        <translation>10 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="329"/>
        <source>5 fps</source>
        <translation>5 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="336"/>
        <source>Analyzer Falloff</source>
        <translation>Analyzer Merosot</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="339"/>
        <location filename="../analyzer.cpp" line="353"/>
        <source>Slowest</source>
        <translation>Agak Lambat</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="340"/>
        <location filename="../analyzer.cpp" line="354"/>
        <source>Slow</source>
        <translation>Lambat</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="341"/>
        <location filename="../analyzer.cpp" line="355"/>
        <source>Medium</source>
        <translation>Sedang</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="342"/>
        <location filename="../analyzer.cpp" line="356"/>
        <source>Fast</source>
        <translation>Cepat</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="343"/>
        <location filename="../analyzer.cpp" line="357"/>
        <source>Fastest</source>
        <translation>Agak Cepat</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="350"/>
        <source>Peaks Falloff</source>
        <translation>Puncak Merosot</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Layar Penuh</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>AnalyzerColorWidget</name>
    <message>
        <location filename="../analyzercolorwidget.cpp" line="37"/>
        <source>Select Color</source>
        <translation type="unfinished">Pilih Warna</translation>
    </message>
</context>
<context>
    <name>AnalyzerSettingsDialog</name>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="14"/>
        <source>Analyzer Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin Analyzer</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="35"/>
        <source>General</source>
        <translation type="unfinished">Umum</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="41"/>
        <source>Cells size:</source>
        <translation type="unfinished">Ukuran sel:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="101"/>
        <source>Colors</source>
        <translation type="unfinished">Warna</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="107"/>
        <source>Peaks:</source>
        <translation type="unfinished">Puncak:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="139"/>
        <source>Analyzer #1:</source>
        <translation type="unfinished">Analyzer #1:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="171"/>
        <source>Background:</source>
        <translation type="unfinished">Background:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="203"/>
        <source>Analyzer #2:</source>
        <translation type="unfinished">Analyzer #2:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="248"/>
        <source>Analyzer #3:</source>
        <translation type="unfinished">Analyzer #3:</translation>
    </message>
</context>
<context>
    <name>VisualAnalyzerFactory</name>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="30"/>
        <source>Analyzer Plugin</source>
        <translation>Plugin Analyzer</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="49"/>
        <source>About Analyzer Visual Plugin</source>
        <translation>Tentang Plugin Visual Analyzer</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="50"/>
        <source>Qmmp Analyzer Visual Plugin</source>
        <translation>Plugin Visual Analyzer Qmmp</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
