<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>Analyzer</name>
    <message>
        <location filename="../analyzer.cpp" line="36"/>
        <source>Qmmp Analyzer</source>
        <translation>QMMP アナライザー</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="320"/>
        <source>Peaks</source>
        <translation>ピーク図</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="323"/>
        <source>Refresh Rate</source>
        <translation>再描画の頻度</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="326"/>
        <source>50 fps</source>
        <translation>50 フレーム毎秒</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="327"/>
        <source>25 fps</source>
        <translation>25 フレーム毎秒</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="328"/>
        <source>10 fps</source>
        <translation>10 フレーム毎秒</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="329"/>
        <source>5 fps</source>
        <translation>5 フレーム毎秒</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="336"/>
        <source>Analyzer Falloff</source>
        <translation>アナライザー減衰速度</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="339"/>
        <location filename="../analyzer.cpp" line="353"/>
        <source>Slowest</source>
        <translation>さらに遅く</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="340"/>
        <location filename="../analyzer.cpp" line="354"/>
        <source>Slow</source>
        <translation>遅く</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="341"/>
        <location filename="../analyzer.cpp" line="355"/>
        <source>Medium</source>
        <translation>適度</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="342"/>
        <location filename="../analyzer.cpp" line="356"/>
        <source>Fast</source>
        <translation>速く</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="343"/>
        <location filename="../analyzer.cpp" line="357"/>
        <source>Fastest</source>
        <translation>さらに速く</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="350"/>
        <source>Peaks Falloff</source>
        <translation>ピーク減衰速度</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>&amp;Full Screen</source>
        <translation>フルスクリーン(&amp;F)</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>AnalyzerColorWidget</name>
    <message>
        <location filename="../analyzercolorwidget.cpp" line="37"/>
        <source>Select Color</source>
        <translation>色を選択</translation>
    </message>
</context>
<context>
    <name>AnalyzerSettingsDialog</name>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="14"/>
        <source>Analyzer Plugin Settings</source>
        <translation>アナライザープラグイン設定</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="35"/>
        <source>General</source>
        <translation>総合</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="41"/>
        <source>Cells size:</source>
        <translation>セルサイズ:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="101"/>
        <source>Colors</source>
        <translation>色</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="107"/>
        <source>Peaks:</source>
        <translation>峰:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="139"/>
        <source>Analyzer #1:</source>
        <translation>第一アナライザー:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="171"/>
        <source>Background:</source>
        <translation>背景:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="203"/>
        <source>Analyzer #2:</source>
        <translation>第二アナライザー:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="248"/>
        <source>Analyzer #3:</source>
        <translation>第三アナライザー:</translation>
    </message>
</context>
<context>
    <name>VisualAnalyzerFactory</name>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="30"/>
        <source>Analyzer Plugin</source>
        <translation>アナライザープラグイン</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="49"/>
        <source>About Analyzer Visual Plugin</source>
        <translation>アナライザー視覚効果プラグインについて</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="50"/>
        <source>Qmmp Analyzer Visual Plugin</source>
        <translation>QMMP アナライザー視覚効果プラグイン</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
