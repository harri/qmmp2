<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ProjectM4Widget</name>
    <message>
        <location filename="../projectm4widget.cpp" line="163"/>
        <source>&amp;Show Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="163"/>
        <source>M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="165"/>
        <source>&amp;Next Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="165"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="166"/>
        <source>&amp;Previous Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="166"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="167"/>
        <source>&amp;Shuffle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="167"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="168"/>
        <source>&amp;Lock Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="168"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="170"/>
        <source>&amp;Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectm4widget.cpp" line="170"/>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectMPlugin</name>
    <message>
        <location filename="../projectmplugin.cpp" line="42"/>
        <source>ProjectM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectMWidget</name>
    <message>
        <location filename="../projectmwidget.cpp" line="144"/>
        <location filename="../projectmwidget.cpp" line="156"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="144"/>
        <location filename="../projectmwidget.cpp" line="156"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="145"/>
        <location filename="../projectmwidget.cpp" line="157"/>
        <source>&amp;Show Song Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="145"/>
        <location filename="../projectmwidget.cpp" line="157"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="146"/>
        <location filename="../projectmwidget.cpp" line="158"/>
        <source>&amp;Show Preset Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="146"/>
        <location filename="../projectmwidget.cpp" line="158"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="147"/>
        <location filename="../projectmwidget.cpp" line="159"/>
        <source>&amp;Show Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="147"/>
        <location filename="../projectmwidget.cpp" line="159"/>
        <source>M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="149"/>
        <location filename="../projectmwidget.cpp" line="161"/>
        <source>&amp;Next Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="149"/>
        <location filename="../projectmwidget.cpp" line="161"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="150"/>
        <location filename="../projectmwidget.cpp" line="162"/>
        <source>&amp;Previous Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="150"/>
        <location filename="../projectmwidget.cpp" line="162"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="151"/>
        <location filename="../projectmwidget.cpp" line="163"/>
        <source>&amp;Random Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="151"/>
        <location filename="../projectmwidget.cpp" line="163"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="152"/>
        <location filename="../projectmwidget.cpp" line="164"/>
        <source>&amp;Lock Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="152"/>
        <location filename="../projectmwidget.cpp" line="164"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="154"/>
        <location filename="../projectmwidget.cpp" line="166"/>
        <source>&amp;Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="154"/>
        <location filename="../projectmwidget.cpp" line="166"/>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualProjectMFactory</name>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="30"/>
        <source>ProjectM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="50"/>
        <source>About ProjectM Visual Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="51"/>
        <source>Qmmp ProjectM Visual Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="52"/>
        <source>This plugin adds projectM visualization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="54"/>
        <source>Based on libprojectM-qt library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
