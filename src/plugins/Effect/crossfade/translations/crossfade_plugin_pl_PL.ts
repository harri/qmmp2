<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>CrossfadeSettingsDialog</name>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="14"/>
        <source>Crossfade Plugin Settings</source>
        <translation>Ustawienia wtyczki Crossfade</translation>
    </message>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="29"/>
        <source>Overlap:</source>
        <translation>Nakładanie:</translation>
    </message>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="42"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
</context>
<context>
    <name>EffectCrossfadeFactory</name>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="30"/>
        <source>Crossfade Plugin</source>
        <translation>Wtyczka Crossfade</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="50"/>
        <source>About Crossfade Plugin</source>
        <translation>O wtyczce Crossfade</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="51"/>
        <source>Qmmp Crossfade Plugin</source>
        <translation>Wtyczka Crossfade dla Qmmp</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Napisana przez: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
