<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_RS">
<context>
    <name>CrossfadeSettingsDialog</name>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="14"/>
        <source>Crossfade Plugin Settings</source>
        <translation type="unfinished">Поставке претапања</translation>
    </message>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="29"/>
        <source>Overlap:</source>
        <translation type="unfinished">Преклапање:</translation>
    </message>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="42"/>
        <source>ms</source>
        <translation type="unfinished">ms</translation>
    </message>
</context>
<context>
    <name>EffectCrossfadeFactory</name>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="30"/>
        <source>Crossfade Plugin</source>
        <translation>Претапање</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="50"/>
        <source>About Crossfade Plugin</source>
        <translation>О прикључку за претапање</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="51"/>
        <source>Qmmp Crossfade Plugin</source>
        <translation>Кумп прикључак за претапање</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
