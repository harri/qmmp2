<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>Bs2bSettingsDialog</name>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="14"/>
        <source>BS2B Plugin Settings</source>
        <translation>Налаштування втулка BS2B</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="32"/>
        <source>Crossfeed level</source>
        <translation>Рівень взаємозмішування</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="45"/>
        <location filename="../bs2bsettingsdialog.ui" line="59"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="71"/>
        <source>Default</source>
        <translation>Усталено</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="78"/>
        <source>C.Moy</source>
        <translation>C.Moy</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="85"/>
        <source>J. Meier</source>
        <translation>Дж. Мейєр</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.cpp" line="61"/>
        <source>%1 Hz, %2 us</source>
        <translation>%1 Гц, %2 мкс</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.cpp" line="68"/>
        <source>%1 dB</source>
        <translation>%1 дБ</translation>
    </message>
</context>
<context>
    <name>EffectBs2bFactory</name>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="31"/>
        <source>BS2B Plugin</source>
        <translation>Втулок BS2B</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="50"/>
        <source>About BS2B Effect Plugin</source>
        <translation>Про втулок ефектів BS2B</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="52"/>
        <source>This is the Qmmp plugin version of Boris Mikhaylov&apos;s headphone DSP effect &quot;Bauer stereophonic-to-binaural&quot;, abbreviated bs2b.</source>
        <translation>Цей втулок додає ефект &quot;Bauer stereophonic-to-binaural&quot; (скорочено bs2b) Бориса Михайлова.</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="55"/>
        <source>Visit %1 for more details</source>
        <translation>Для отримання інформації відвідайте %1</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="57"/>
        <source>Compiled against libbs2b-%1</source>
        <translation>Зібрано з libbs2b-%1</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="59"/>
        <source>Developers:</source>
        <translation>Розробники:</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="60"/>
        <source>Ilya Kotov &amp;lt;forkotov02@ya.ru&amp;gt;</source>
        <translation>Ілля Котов &amp;lt;forkotov02@ya.ru&amp;gt;</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="61"/>
        <source>Sebastian Pipping &amp;lt;sebastian@pipping.org&amp;gt;</source>
        <translation>Себастьян Піппінґ &amp;lt;sebastian@pipping.org&amp;gt;</translation>
    </message>
</context>
</TS>
