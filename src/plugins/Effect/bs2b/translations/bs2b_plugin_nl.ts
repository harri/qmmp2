<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>Bs2bSettingsDialog</name>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="14"/>
        <source>BS2B Plugin Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="32"/>
        <source>Crossfeed level</source>
        <translation>Overgangsniveau</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="45"/>
        <location filename="../bs2bsettingsdialog.ui" line="59"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="71"/>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="78"/>
        <source>C.Moy</source>
        <translation>C.Moy</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="85"/>
        <source>J. Meier</source>
        <translation>J. Meier</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.cpp" line="61"/>
        <source>%1 Hz, %2 us</source>
        <translation>%1 Hz, %2 us</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.cpp" line="68"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
</context>
<context>
    <name>EffectBs2bFactory</name>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="31"/>
        <source>BS2B Plugin</source>
        <translation>BS2B-plug-in</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="50"/>
        <source>About BS2B Effect Plugin</source>
        <translation>Over de BS2B-effectenplug-in</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="52"/>
        <source>This is the Qmmp plugin version of Boris Mikhaylov&apos;s headphone DSP effect &quot;Bauer stereophonic-to-binaural&quot;, abbreviated bs2b.</source>
        <translation>Dit is de Qmmp-plug-in-uitvoering van Boris Mikhaylov&apos;s headset dsp-effect &apos;Bauer stereofonisch-naar-&apos;binaural&apos;, afgekort bs2b.</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="55"/>
        <source>Visit %1 for more details</source>
        <translation>Ga voor meer informatie naar %1</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="57"/>
        <source>Compiled against libbs2b-%1</source>
        <translation>Gebouwd met libbs2b-%1</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="59"/>
        <source>Developers:</source>
        <translation>Ontwikkelaars:</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="60"/>
        <source>Ilya Kotov &amp;lt;forkotov02@ya.ru&amp;gt;</source>
        <translation>Ilya Kotov &amp;lt;forkotov02@ya.ru&amp;gt;</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="61"/>
        <source>Sebastian Pipping &amp;lt;sebastian@pipping.org&amp;gt;</source>
        <translation>Sebastian Pipping &amp;lt;sebastian@pipping.org&amp;gt;</translation>
    </message>
</context>
</TS>
