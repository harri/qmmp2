<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk">
<context>
    <name>Bs2bSettingsDialog</name>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="14"/>
        <source>BS2B Plugin Settings</source>
        <translation type="unfinished">Nastavenia BS2B Plugin</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="32"/>
        <source>Crossfeed level</source>
        <translation type="unfinished">Crossfeed level</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="45"/>
        <location filename="../bs2bsettingsdialog.ui" line="59"/>
        <source>-</source>
        <translation type="unfinished">-</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="71"/>
        <source>Default</source>
        <translation type="unfinished">Predvolené</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="78"/>
        <source>C.Moy</source>
        <translation type="unfinished">C.Moy</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.ui" line="85"/>
        <source>J. Meier</source>
        <translation type="unfinished">J. Meier</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.cpp" line="61"/>
        <source>%1 Hz, %2 us</source>
        <translation type="unfinished">%1 Hz, %2 us</translation>
    </message>
    <message>
        <location filename="../bs2bsettingsdialog.cpp" line="68"/>
        <source>%1 dB</source>
        <translation type="unfinished">%1 dB</translation>
    </message>
</context>
<context>
    <name>EffectBs2bFactory</name>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="31"/>
        <source>BS2B Plugin</source>
        <translation>BS2B Plugin</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="50"/>
        <source>About BS2B Effect Plugin</source>
        <translation>O BS2B efektovom plugine</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="52"/>
        <source>This is the Qmmp plugin version of Boris Mikhaylov&apos;s headphone DSP effect &quot;Bauer stereophonic-to-binaural&quot;, abbreviated bs2b.</source>
        <translation>Toto je Qmmp verzia pluginu DSP efektu &quot;Bauer steroephonic-to-binaural&quot; alebo bs2b pre slúchadlá  od Borisa Mikhaylova </translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="55"/>
        <source>Visit %1 for more details</source>
        <translation>Navštívte %1 pre viac informácií</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="57"/>
        <source>Compiled against libbs2b-%1</source>
        <translation>Skompilované oproti libbs2b-%1</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="59"/>
        <source>Developers:</source>
        <translation>Vývojári:</translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="60"/>
        <source>Ilya Kotov &amp;lt;forkotov02@ya.ru&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectbs2bfactory.cpp" line="61"/>
        <source>Sebastian Pipping &amp;lt;sebastian@pipping.org&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
