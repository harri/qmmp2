<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>EffectMonoToStereoFactory</name>
    <message>
        <location filename="../effectmonotostereofactory.cpp" line="29"/>
        <source>Mono to Stereo Converter Plugin</source>
        <translation>Втулок перетвор. моно на стерео</translation>
    </message>
    <message>
        <location filename="../effectmonotostereofactory.cpp" line="49"/>
        <source>About Mono to Stereo Converter Plugin</source>
        <translation>Про втулок перетворення з моно на стерео</translation>
    </message>
    <message>
        <location filename="../effectmonotostereofactory.cpp" line="50"/>
        <source>Qmmp Mono to Stereo Converter Plugin</source>
        <translation>Втулок перетворення з моно на стерео для Qmmp</translation>
    </message>
    <message>
        <location filename="../effectmonotostereofactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов  &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
