<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>EffectMonoToStereoFactory</name>
    <message>
        <location filename="../effectmonotostereofactory.cpp" line="29"/>
        <source>Mono to Stereo Converter Plugin</source>
        <translation>Tekili Çoğula Dönüştürücü Eklenti</translation>
    </message>
    <message>
        <location filename="../effectmonotostereofactory.cpp" line="49"/>
        <source>About Mono to Stereo Converter Plugin</source>
        <translation>Tekkanallıyı Çokkanallıya Dönüştürücü Eklenti Hakkında</translation>
    </message>
    <message>
        <location filename="../effectmonotostereofactory.cpp" line="50"/>
        <source>Qmmp Mono to Stereo Converter Plugin</source>
        <translation>Qmmp Tekkanallıyı Çiftkanallıya Dönüştürücü Eklentisi</translation>
    </message>
    <message>
        <location filename="../effectmonotostereofactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
