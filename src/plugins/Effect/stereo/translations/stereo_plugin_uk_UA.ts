<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>EffectStereoFactory</name>
    <message>
        <location filename="../effectstereofactory.cpp" line="30"/>
        <source>Extra Stereo Plugin</source>
        <translation>Втулок розширення стереобази</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="49"/>
        <source>About Extra Stereo Plugin</source>
        <translation>Про втулок розширення стереобази</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="50"/>
        <source>Qmmp Extra Stereo Plugin</source>
        <translation>Втулок розширення стереобази для Qmmp</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="52"/>
        <source>Based on the Extra Stereo Plugin for Xmms by Johan Levin</source>
        <translation>Базується на втулці розширення стерео для Xmms від Johan Levin</translation>
    </message>
</context>
<context>
    <name>StereoSettingsDialog</name>
    <message>
        <location filename="../stereosettingsdialog.ui" line="14"/>
        <source>Extra Stereo Plugin Settings</source>
        <translation>Налаштування втулка розширення стереобази</translation>
    </message>
    <message>
        <location filename="../stereosettingsdialog.ui" line="31"/>
        <source>Effect intensity:</source>
        <translation>Глибина ефекту:</translation>
    </message>
    <message>
        <location filename="../stereosettingsdialog.ui" line="54"/>
        <source>-</source>
        <translation>-</translation>
    </message>
</context>
</TS>
