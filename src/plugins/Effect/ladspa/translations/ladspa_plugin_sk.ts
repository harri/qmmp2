<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk">
<context>
    <name>EffectLADSPAFactory</name>
    <message>
        <location filename="../effectladspafactory.cpp" line="30"/>
        <source>LADSPA Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="49"/>
        <source>About LADSPA Host for Qmmp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="50"/>
        <source>LADSPA Host for Qmmp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="52"/>
        <source>Based on the LADSPA Host for BMP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="53"/>
        <source>BMP-ladspa developers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="54"/>
        <source>Nick Lamb &lt;njl195@zepler.org.uk&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="55"/>
        <source>Giacomo Lozito &lt;city_hunter@users.sf.net&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LADSPASettingsDialog</name>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="14"/>
        <source>LADSPA Plugin Catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="52"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="59"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="94"/>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="46"/>
        <source>UID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="47"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="131"/>
        <source>This LADSPA plugin has no user controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
