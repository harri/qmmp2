<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>EffectLADSPAFactory</name>
    <message>
        <location filename="../effectladspafactory.cpp" line="30"/>
        <source>LADSPA Plugin</source>
        <translation>LADSPA 플러그인</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="49"/>
        <source>About LADSPA Host for Qmmp</source>
        <translation>Qmmp용 LADSPA 호스트 정보</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="50"/>
        <source>LADSPA Host for Qmmp</source>
        <translation>Qmmp용 LADSPA 호스트</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>작성자: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="52"/>
        <source>Based on the LADSPA Host for BMP</source>
        <translation>BMP용 LADSPA 호스트에 기반함</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="53"/>
        <source>BMP-ladspa developers:</source>
        <translation>BMP-ladspa 개발자:</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="54"/>
        <source>Nick Lamb &lt;njl195@zepler.org.uk&gt;</source>
        <translation>Nick Lamb &lt;njl195@zepler.org.uk&gt;</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="55"/>
        <source>Giacomo Lozito &lt;city_hunter@users.sf.net&gt;</source>
        <translation>Giacomo Lozito &lt;city_hunter@users.sf.net&gt;</translation>
    </message>
</context>
<context>
    <name>LADSPASettingsDialog</name>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="14"/>
        <source>LADSPA Plugin Catalog</source>
        <translation>LADSPA 플러그인 카탈로그</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="52"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="59"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="94"/>
        <source>Configure</source>
        <translation>구성하기</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="46"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="47"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="131"/>
        <source>This LADSPA plugin has no user controls</source>
        <translation>이 LADSPA 플러그인에는 사용자 컨트롤이 없습니다</translation>
    </message>
</context>
</TS>
