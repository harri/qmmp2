<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_BA">
<context>
    <name>EffectLADSPAFactory</name>
    <message>
        <location filename="../effectladspafactory.cpp" line="30"/>
        <source>LADSPA Plugin</source>
        <translation>ЛАДСПА</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="49"/>
        <source>About LADSPA Host for Qmmp</source>
        <translation>О ЛАДСПА хосту за Кумп</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="50"/>
        <source>LADSPA Host for Qmmp</source>
        <translation>ЛАДСПА хост за Кумп</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="52"/>
        <source>Based on the LADSPA Host for BMP</source>
        <translation>Заснован на ЛАДСПА хосту за БМП</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="53"/>
        <source>BMP-ladspa developers:</source>
        <translation>БМП-ладспа програмери:</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="54"/>
        <source>Nick Lamb &lt;njl195@zepler.org.uk&gt;</source>
        <translation>Nick Lamb &lt;njl195@zepler.org.uk&gt;</translation>
    </message>
    <message>
        <location filename="../effectladspafactory.cpp" line="55"/>
        <source>Giacomo Lozito &lt;city_hunter@users.sf.net&gt;</source>
        <translation>Giacomo Lozito &lt;city_hunter@users.sf.net&gt;</translation>
    </message>
</context>
<context>
    <name>LADSPASettingsDialog</name>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="14"/>
        <source>LADSPA Plugin Catalog</source>
        <translation type="unfinished">ЛАДСПА каталог</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="52"/>
        <source>&gt;</source>
        <translation type="unfinished">&gt;</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="59"/>
        <source>&lt;</source>
        <translation type="unfinished">&lt;</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.ui" line="94"/>
        <source>Configure</source>
        <translation type="unfinished">Подеси</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="46"/>
        <source>UID</source>
        <translation type="unfinished">уид</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="47"/>
        <source>Name</source>
        <translation type="unfinished">име</translation>
    </message>
    <message>
        <location filename="../ladspasettingsdialog.cpp" line="131"/>
        <source>This LADSPA plugin has no user controls</source>
        <translation type="unfinished">Овај ЛАДСПА прикључак нема корисничких контрола</translation>
    </message>
</context>
</TS>
