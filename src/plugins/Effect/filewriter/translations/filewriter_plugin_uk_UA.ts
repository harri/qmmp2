<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>EffectFileWriterFactory</name>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="30"/>
        <source>File Writer Plugin</source>
        <translation>Втулок запису файлів</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="50"/>
        <source>About File Writer Plugin</source>
        <translation>Про втулок запису файлів</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="51"/>
        <source>Qmmp File Writer Plugin</source>
        <translation>Втулок запису файлів для Qmmp</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>FileWriterSettingsDialog</name>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="14"/>
        <source>File Writer Plugin Settings</source>
        <translation>Налаштування втулка запису файлів</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="29"/>
        <source>Output directory:</source>
        <translation>Вихідна тека:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="36"/>
        <source>Quality:</source>
        <translation>Якість:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="95"/>
        <source>Output file name:</source>
        <translation>Ім&apos;я вихідного файлу:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="109"/>
        <source>Write to single file if possible.</source>
        <translation>Записувати за можливістю в один файл.</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.cpp" line="72"/>
        <source>Choose a directory</source>
        <translation>Оберіть теку</translation>
    </message>
</context>
</TS>
