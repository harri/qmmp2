<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>EffectFileWriterFactory</name>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="30"/>
        <source>File Writer Plugin</source>
        <translation>Plugin File Writer</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="50"/>
        <source>About File Writer Plugin</source>
        <translation>Tentang Plugin File Writer</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="51"/>
        <source>Qmmp File Writer Plugin</source>
        <translation>Plugin File Writer Qmmp</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>FileWriterSettingsDialog</name>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="14"/>
        <source>File Writer Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin File Writer</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="29"/>
        <source>Output directory:</source>
        <translation type="unfinished">Direktori keluaran:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="36"/>
        <source>Quality:</source>
        <translation type="unfinished">Kualitas:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="95"/>
        <source>Output file name:</source>
        <translation type="unfinished">Nama file keluaran:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="109"/>
        <source>Write to single file if possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.cpp" line="72"/>
        <source>Choose a directory</source>
        <translation type="unfinished">Memilih sebuah direktori</translation>
    </message>
</context>
</TS>
