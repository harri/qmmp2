<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>MplayerEngineFactory</name>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="33"/>
        <source>Mplayer Plugin</source>
        <translation>Втулок Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="36"/>
        <source>Video Files</source>
        <translation>Відеофайли</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="73"/>
        <source>About MPlayer Plugin</source>
        <translation>Про втулок Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="74"/>
        <source>Qmmp MPlayer Plugin</source>
        <translation>Втулок Mplayer для Qmmp</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="75"/>
        <source>This plugin uses MPlayer as backend</source>
        <translation>Цей втулок використовує MPlayer як бекенд</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="76"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>MplayerMetaDataModel</name>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>KiB</source>
        <translation>КіБ</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="58"/>
        <source>Demuxer</source>
        <translation>Демультиплексор</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="60"/>
        <source>Video format</source>
        <translation>Формат відео</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="61"/>
        <source>FPS</source>
        <translation>Кадрів на секунду</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="62"/>
        <source>Video codec</source>
        <translation>Відеокодек</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="63"/>
        <source>Aspect ratio</source>
        <translation>Співвідношення</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <source>Video bitrate</source>
        <translation>Відеобітрейт</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>kbps</source>
        <translation>кб/с</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="66"/>
        <source>Audio codec</source>
        <translation>Авдіокодек</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Sample rate</source>
        <translation>Дискретизація</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Hz</source>
        <translation>Гц</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>Audio bitrate</source>
        <translation>Авдіобітрейт</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="69"/>
        <source>Channels</source>
        <translation>Канали</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="65"/>
        <source>Resolution</source>
        <translation>Роздільність</translation>
    </message>
</context>
<context>
    <name>MplayerSettingsDialog</name>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="14"/>
        <source>MPlayer Settings</source>
        <translation>Налаштування MPlayer</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="34"/>
        <source>Video:</source>
        <translation>Відео:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="48"/>
        <source>Audio:</source>
        <translation>Авдіо:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="62"/>
        <source>Audio/video auto synchronization</source>
        <translation>Автоматична синхронізація авдіо/відео</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="72"/>
        <source>Synchronization factor:</source>
        <translation>Чинник синхронізації:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="89"/>
        <source>Extra options:</source>
        <translation>Додаткові параметри:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="96"/>
        <source>Extra command line options</source>
        <translation>Додаткові команди командного рядка</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.cpp" line="29"/>
        <location filename="../mplayersettingsdialog.cpp" line="33"/>
        <location filename="../mplayersettingsdialog.cpp" line="37"/>
        <location filename="../mplayersettingsdialog.cpp" line="38"/>
        <location filename="../mplayersettingsdialog.cpp" line="54"/>
        <location filename="../mplayersettingsdialog.cpp" line="55"/>
        <source>default</source>
        <translation>усталено</translation>
    </message>
</context>
</TS>
