<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>MplayerEngineFactory</name>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="33"/>
        <source>Mplayer Plugin</source>
        <translation>Wtyczka Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="36"/>
        <source>Video Files</source>
        <translation>Pliki wideo</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="73"/>
        <source>About MPlayer Plugin</source>
        <translation>O wtyczce Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="74"/>
        <source>Qmmp MPlayer Plugin</source>
        <translation>Wtyczka Mplayer dla Qmmp</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="75"/>
        <source>This plugin uses MPlayer as backend</source>
        <translation>Ta wtyczka używa Mplayer do odtwarzania</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="76"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Napisana przez: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>MplayerMetaDataModel</name>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="58"/>
        <source>Demuxer</source>
        <translation>Demuxer</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="60"/>
        <source>Video format</source>
        <translation>Format wideo</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="61"/>
        <source>FPS</source>
        <translation>Kl./s</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="62"/>
        <source>Video codec</source>
        <translation>Kodek wideo</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="63"/>
        <source>Aspect ratio</source>
        <translation>Format obrazu</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <source>Video bitrate</source>
        <translation>Bitrate wideo</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>kbps</source>
        <translation>kbps</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="66"/>
        <source>Audio codec</source>
        <translation>Kodek dźwięku</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Sample rate</source>
        <translation>Próbkowanie</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>Audio bitrate</source>
        <translation>Bitrate dźwięku</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="69"/>
        <source>Channels</source>
        <translation>Kanały</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="65"/>
        <source>Resolution</source>
        <translation>Rozdzielczość</translation>
    </message>
</context>
<context>
    <name>MplayerSettingsDialog</name>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="14"/>
        <source>MPlayer Settings</source>
        <translation>Ustawienia MPlayer</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="34"/>
        <source>Video:</source>
        <translation>Obraz:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="48"/>
        <source>Audio:</source>
        <translation>Dźwięk:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="62"/>
        <source>Audio/video auto synchronization</source>
        <translation>Automatyczna synchronizacja audio/wideo</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="72"/>
        <source>Synchronization factor:</source>
        <translation>Współczynnik synchronizacji:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="89"/>
        <source>Extra options:</source>
        <translation>Dodatkowe opcje:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="96"/>
        <source>Extra command line options</source>
        <translation>Dodatkowe opcje z linii poleceń</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.cpp" line="29"/>
        <location filename="../mplayersettingsdialog.cpp" line="33"/>
        <location filename="../mplayersettingsdialog.cpp" line="37"/>
        <location filename="../mplayersettingsdialog.cpp" line="38"/>
        <location filename="../mplayersettingsdialog.cpp" line="54"/>
        <location filename="../mplayersettingsdialog.cpp" line="55"/>
        <source>default</source>
        <translation>domyślne</translation>
    </message>
</context>
</TS>
