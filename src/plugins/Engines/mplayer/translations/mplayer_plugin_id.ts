<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>MplayerEngineFactory</name>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="33"/>
        <source>Mplayer Plugin</source>
        <translation>Plugin Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="36"/>
        <source>Video Files</source>
        <translation>File Video</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="73"/>
        <source>About MPlayer Plugin</source>
        <translation>Tentang Plugin Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="74"/>
        <source>Qmmp MPlayer Plugin</source>
        <translation>Plugin Mplayer Qmmp</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="75"/>
        <source>This plugin uses MPlayer as backend</source>
        <translation>Plugin ini digunakan MPlayer sebagai backend</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="76"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>MplayerMetaDataModel</name>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>Size</source>
        <translation>Ukuran</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="58"/>
        <source>Demuxer</source>
        <translation>Demuxer</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="60"/>
        <source>Video format</source>
        <translation>Format video</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="61"/>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="62"/>
        <source>Video codec</source>
        <translation>Video codec</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="63"/>
        <source>Aspect ratio</source>
        <translation>Aspect ratio</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <source>Video bitrate</source>
        <translation>Video bitrate</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>kbps</source>
        <translation>kbps</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="66"/>
        <source>Audio codec</source>
        <translation>Audio codec</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Sample rate</source>
        <translation>Sample rate</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>Audio bitrate</source>
        <translation>Audio bitrate</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="69"/>
        <source>Channels</source>
        <translation>Saluran</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="65"/>
        <source>Resolution</source>
        <translation>Resolusi</translation>
    </message>
</context>
<context>
    <name>MplayerSettingsDialog</name>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="14"/>
        <source>MPlayer Settings</source>
        <translation type="unfinished">Setelan MPlayer</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="34"/>
        <source>Video:</source>
        <translation type="unfinished">Video:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="48"/>
        <source>Audio:</source>
        <translation type="unfinished">Audio:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="62"/>
        <source>Audio/video auto synchronization</source>
        <translation type="unfinished">Sinkronisasi otomatis audio/video</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="72"/>
        <source>Synchronization factor:</source>
        <translation type="unfinished">Faktor sinkronisasi:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="89"/>
        <source>Extra options:</source>
        <translation type="unfinished">Pilihan eksta:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="96"/>
        <source>Extra command line options</source>
        <translation type="unfinished">Pilihan baris perintah ekstra</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.cpp" line="29"/>
        <location filename="../mplayersettingsdialog.cpp" line="33"/>
        <location filename="../mplayersettingsdialog.cpp" line="37"/>
        <location filename="../mplayersettingsdialog.cpp" line="38"/>
        <location filename="../mplayersettingsdialog.cpp" line="54"/>
        <location filename="../mplayersettingsdialog.cpp" line="55"/>
        <source>default</source>
        <translation type="unfinished">Baku</translation>
    </message>
</context>
</TS>
