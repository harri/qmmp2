<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>MplayerEngineFactory</name>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="33"/>
        <source>Mplayer Plugin</source>
        <translation>Modul MPlayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="36"/>
        <source>Video Files</source>
        <translation>Videosoubory</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="73"/>
        <source>About MPlayer Plugin</source>
        <translation>O modulu MPlayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="74"/>
        <source>Qmmp MPlayer Plugin</source>
        <translation>Modul Qmmp MPlayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="75"/>
        <source>This plugin uses MPlayer as backend</source>
        <translation>Tento modul používá jako backend MPlayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="76"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MplayerMetaDataModel</name>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="58"/>
        <source>Demuxer</source>
        <translation>Demultiplexor</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="60"/>
        <source>Video format</source>
        <translation>Formát videa</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="61"/>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="62"/>
        <source>Video codec</source>
        <translation>Videokodek</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="63"/>
        <source>Aspect ratio</source>
        <translation>Poměr stran</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <source>Video bitrate</source>
        <translation>Datový tok videa</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>kbps</source>
        <translation>kbps</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="66"/>
        <source>Audio codec</source>
        <translation>Zvukový kodek</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Sample rate</source>
        <translation>Vzorkovací frekvence</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>Audio bitrate</source>
        <translation>Datový tok audia</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="69"/>
        <source>Channels</source>
        <translation>Počet kanálů</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="65"/>
        <source>Resolution</source>
        <translation>Rozlišení</translation>
    </message>
</context>
<context>
    <name>MplayerSettingsDialog</name>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="14"/>
        <source>MPlayer Settings</source>
        <translation type="unfinished">Nastavení MPlayer</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="34"/>
        <source>Video:</source>
        <translation type="unfinished">Video:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="48"/>
        <source>Audio:</source>
        <translation type="unfinished">Audio:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="62"/>
        <source>Audio/video auto synchronization</source>
        <translation type="unfinished">Automatická A/V synchronizace</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="72"/>
        <source>Synchronization factor:</source>
        <translation type="unfinished">Synchronizační faktor:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="89"/>
        <source>Extra options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="96"/>
        <source>Extra command line options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.cpp" line="29"/>
        <location filename="../mplayersettingsdialog.cpp" line="33"/>
        <location filename="../mplayersettingsdialog.cpp" line="37"/>
        <location filename="../mplayersettingsdialog.cpp" line="38"/>
        <location filename="../mplayersettingsdialog.cpp" line="54"/>
        <location filename="../mplayersettingsdialog.cpp" line="55"/>
        <source>default</source>
        <translation type="unfinished">výchozí</translation>
    </message>
</context>
</TS>
