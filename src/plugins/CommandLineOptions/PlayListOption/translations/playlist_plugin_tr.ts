<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>PlayListOption</name>
    <message>
        <location filename="../playlistoption.cpp" line="33"/>
        <source>Show playlist manipulation commands</source>
        <translation>Çalma listesi düzenleme komutlarını göster</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="34"/>
        <source>List all available playlists</source>
        <translation>Mevcut tüm çalma listelerini listele</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="35"/>
        <source>Show playlist content</source>
        <translation>Çalma listesi içeriğini göster</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="36"/>
        <source>Play track &lt;track&gt; in playlist &lt;id&gt;</source>
        <translation>Parçayı çal &lt;track&gt; çalma listesinden &lt;id&gt;</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="37"/>
        <source>Clear playlist</source>
        <translation>Çalma listesini temizle</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="38"/>
        <source>Activate next playlist</source>
        <translation>Sonraki çalma listesini etkinleştir</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="39"/>
        <source>Activate previous playlist</source>
        <translation>Önceki çalma listesini etkinleştir</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="40"/>
        <source>Toggle playlist repeat</source>
        <translation>Çalma listesi tekrarında geçiş sağla</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="41"/>
        <source>Toggle playlist shuffle</source>
        <translation>Çalma listesi karıştırmada geçiş sağla</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="42"/>
        <source>Show playlist options</source>
        <translation>Çalma listesi seçeneklerini göster</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="112"/>
        <location filename="../playlistoption.cpp" line="132"/>
        <location filename="../playlistoption.cpp" line="171"/>
        <source>Invalid playlist ID</source>
        <translation>Geçersiz çalma listesi kimliği</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="126"/>
        <source>Invalid number of arguments</source>
        <translation>Bağımsız değişkenlerin sayısı geçersiz</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="136"/>
        <source>Invalid track ID</source>
        <translation>Geçersiz parça kimliği</translation>
    </message>
</context>
</TS>
