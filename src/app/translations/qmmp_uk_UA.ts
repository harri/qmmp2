<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>BuiltinCommandLineOption</name>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="46"/>
        <source>Don&apos;t clear the playlist</source>
        <translation>Не очищувати грайлист</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="47"/>
        <source>Start playing current song</source>
        <translation>Грати поточну пісню</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="48"/>
        <source>Pause current song</source>
        <translation>Призупинити поточну пісню</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="49"/>
        <source>Pause if playing, play otherwise</source>
        <translation>Призупинити/відтворити</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="50"/>
        <source>Stop current song</source>
        <translation>Зупитини поточну пісню</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="51"/>
        <source>Display Jump to Track dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="52"/>
        <source>Quit application</source>
        <translation>Закрити програму</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="53"/>
        <source>Set playback volume (example: qmmp --volume 20)</source>
        <translation>Встановити гучність (приклад: qmmp --volume 20)</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="54"/>
        <source>Print volume level</source>
        <translation>Вивести рівень гучности</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="55"/>
        <source>Mute/Restore volume</source>
        <translation>Вимкнути/Відновити гучність</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="56"/>
        <source>Print mute status</source>
        <translation>Вивести статус приглушення</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="57"/>
        <source>Skip forward in playlist</source>
        <translation>Перейти до наступного фрагменту</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="58"/>
        <source>Skip backwards in playlist</source>
        <translation>Перейти до попереднього фрагменту</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="59"/>
        <source>Show/hide application</source>
        <translation>Показати/сховати програму</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="60"/>
        <source>Show main window</source>
        <translation>Показати головне вікно</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="61"/>
        <source>Display Add File dialog</source>
        <translation>Показати діалог додавання файлів</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="62"/>
        <source>Display Add Directory dialog</source>
        <translation>Показати діалог додавання тек</translation>
    </message>
</context>
<context>
    <name>QMMPStarter</name>
    <message>
        <location filename="../qmmpstarter.cpp" line="149"/>
        <source>Unknown command</source>
        <translation>Невідома команда</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="508"/>
        <source>Usage: qmmp [options] [files]</source>
        <translation>Використання: qmmp [options] [files]</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="509"/>
        <source>Options:</source>
        <translation>Параметри:</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="515"/>
        <source>Start qmmp with the specified user interface</source>
        <translation>Запустити qmmp з вказаним інтерфейсом користувача</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="516"/>
        <source>List all available user interfaces</source>
        <translation>Перелік доступних інтерфейсів користувача</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="517"/>
        <source>Don&apos;t start the application</source>
        <translation>Не запускати програму</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="518"/>
        <source>Print debugging messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="519"/>
        <source>Display this text and exit</source>
        <translation>Показати цей текст і вийти</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="520"/>
        <source>Print version number and exit</source>
        <translation>Показати версію та вийти</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="522"/>
        <source>Home page: %1</source>
        <translation>Домашня сторінка: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="523"/>
        <source>Development page: %1</source>
        <translation>Сторінка розробки: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="524"/>
        <source>Bug tracker: %1</source>
        <translation>Трекер помилок: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="171"/>
        <location filename="../qmmpstarter.cpp" line="529"/>
        <source>Command Line Help</source>
        <translation>Довідка по командному рядку</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="542"/>
        <source>QMMP version: %1</source>
        <translation>Версія QMMP: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="543"/>
        <source>Compiled with Qt version: %1</source>
        <translation>Зібрано з Qt версії: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="544"/>
        <source>Using Qt version: %1</source>
        <translation>Використовується Qt версії: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="547"/>
        <source>Qmmp Version</source>
        <translation>Версія Qmmp</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="564"/>
        <source>User Interfaces</source>
        <translation>Інтерфейси користувача</translation>
    </message>
</context>
</TS>
