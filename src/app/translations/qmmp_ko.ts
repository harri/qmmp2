<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>BuiltinCommandLineOption</name>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="46"/>
        <source>Don&apos;t clear the playlist</source>
        <translation>재생목록 지우기 안함</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="47"/>
        <source>Start playing current song</source>
        <translation>현재 곡 재생 시작</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="48"/>
        <source>Pause current song</source>
        <translation>현재 곡 일시중지</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="49"/>
        <source>Pause if playing, play otherwise</source>
        <translation>재생 / 일시중지</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="50"/>
        <source>Stop current song</source>
        <translation>현재 곡 중지</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="51"/>
        <source>Display Jump to Track dialog</source>
        <translation>트랙 대화상자로 이동 화면표시</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="52"/>
        <source>Quit application</source>
        <translation>응용프로그램 종료</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="53"/>
        <source>Set playback volume (example: qmmp --volume 20)</source>
        <translation>재생 볼륨 지정 (예: qmmp --volume 20)</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="54"/>
        <source>Print volume level</source>
        <translation>볼륨 레벨 출력</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="55"/>
        <source>Mute/Restore volume</source>
        <translation>볼륨 음소거/복원</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="56"/>
        <source>Print mute status</source>
        <translation>음소거 상태 출력</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="57"/>
        <source>Skip forward in playlist</source>
        <translation>재생목록에서 앞으로 건너뛰기</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="58"/>
        <source>Skip backwards in playlist</source>
        <translation>재생목록에서 뒤로 건너뛰기</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="59"/>
        <source>Show/hide application</source>
        <translation>응용프로그램 표시/숨김</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="60"/>
        <source>Show main window</source>
        <translation>기본 창 표시</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="61"/>
        <source>Display Add File dialog</source>
        <translation>파일 대화상자 추가 화면표시</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="62"/>
        <source>Display Add Directory dialog</source>
        <translation>디렉토리 대화상자 추가 화면표시</translation>
    </message>
</context>
<context>
    <name>QMMPStarter</name>
    <message>
        <location filename="../qmmpstarter.cpp" line="149"/>
        <source>Unknown command</source>
        <translation>알 수 없는 명령</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="508"/>
        <source>Usage: qmmp [options] [files]</source>
        <translation>사용법: qmmp [옵션] [파일]</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="509"/>
        <source>Options:</source>
        <translation>옵션:</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="515"/>
        <source>Start qmmp with the specified user interface</source>
        <translation>지정된 사용자 인터페이스를 사용하여 qmmp 시작</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="516"/>
        <source>List all available user interfaces</source>
        <translation>사용 가능한 모든 사용자 인터페이스 목록</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="517"/>
        <source>Don&apos;t start the application</source>
        <translation>응용프로그램을 시작하지 않음</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="518"/>
        <source>Print debugging messages</source>
        <translation>디버깅 메시지 출력하기</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="519"/>
        <source>Display this text and exit</source>
        <translation>이 텍스트 화면표시 및 종료</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="520"/>
        <source>Print version number and exit</source>
        <translation>버전 번호 출력 후 종료</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="522"/>
        <source>Home page: %1</source>
        <translation>홈 페이지: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="523"/>
        <source>Development page: %1</source>
        <translation>개발 페이지: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="524"/>
        <source>Bug tracker: %1</source>
        <translation>버그 추적기: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="171"/>
        <location filename="../qmmpstarter.cpp" line="529"/>
        <source>Command Line Help</source>
        <translation>명령줄 도움말</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="542"/>
        <source>QMMP version: %1</source>
        <translation>QMMP 버전: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="543"/>
        <source>Compiled with Qt version: %1</source>
        <translation>컴파일된 Qt 버전: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="544"/>
        <source>Using Qt version: %1</source>
        <translation>사용중인 Qt 버전: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="547"/>
        <source>Qmmp Version</source>
        <translation>Qmmp 버전</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="564"/>
        <source>User Interfaces</source>
        <translation>사용자 인터페이스</translation>
    </message>
</context>
</TS>
