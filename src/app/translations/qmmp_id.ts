<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>BuiltinCommandLineOption</name>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="46"/>
        <source>Don&apos;t clear the playlist</source>
        <translation>Jangan bersihkan daftarmain</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="47"/>
        <source>Start playing current song</source>
        <translation>Mulai memainkan lagu saat ini</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="48"/>
        <source>Pause current song</source>
        <translation>Jeda lagu saat ini</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="49"/>
        <source>Pause if playing, play otherwise</source>
        <translation>Jeda jika sedang memainkan, mainkan jika sebaliknya </translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="50"/>
        <source>Stop current song</source>
        <translation>Hentikan lagu saat ini</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="51"/>
        <source>Display Jump to Track dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="52"/>
        <source>Quit application</source>
        <translation>Aplikasi keluar</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="53"/>
        <source>Set playback volume (example: qmmp --volume 20)</source>
        <translation>Setel volume pemutaran (contohnya: qmmp --volume 20)</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="54"/>
        <source>Print volume level</source>
        <translation>Cetak level volume</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="55"/>
        <source>Mute/Restore volume</source>
        <translation>Bungkam/Pulihkan volume</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="56"/>
        <source>Print mute status</source>
        <translation>Cetak status bungkam</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="57"/>
        <source>Skip forward in playlist</source>
        <translation>Lewati maju dalam daftarmain</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="58"/>
        <source>Skip backwards in playlist</source>
        <translation>Lewati mundur dalam daftarmain</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="59"/>
        <source>Show/hide application</source>
        <translation>Tampakkan/sembunyikan aplikasi</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="60"/>
        <source>Show main window</source>
        <translation>Tampakkan jendela utama</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="61"/>
        <source>Display Add File dialog</source>
        <translation>Tayangkan dialog Tambah File</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="62"/>
        <source>Display Add Directory dialog</source>
        <translation>Tayangkan dialog Tambah Direktori</translation>
    </message>
</context>
<context>
    <name>QMMPStarter</name>
    <message>
        <location filename="../qmmpstarter.cpp" line="149"/>
        <source>Unknown command</source>
        <translation>Perintah tak diketahui</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="508"/>
        <source>Usage: qmmp [options] [files]</source>
        <translation>Penggunaan: qmmp [pilihan] [file]</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="509"/>
        <source>Options:</source>
        <translation>Pilihan:</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="515"/>
        <source>Start qmmp with the specified user interface</source>
        <translation>Start qmmp dengan antarmuka pengguna yang ditentukan</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="516"/>
        <source>List all available user interfaces</source>
        <translation>Daftar semua antarmuka pengguna yang tersedia</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="517"/>
        <source>Don&apos;t start the application</source>
        <translation>Jangan mulaikan aplikasi</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="518"/>
        <source>Print debugging messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="519"/>
        <source>Display this text and exit</source>
        <translation>Tayangkan teks ini dan keluar</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="520"/>
        <source>Print version number and exit</source>
        <translation>Cetak nomor versi dan keluar</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="522"/>
        <source>Home page: %1</source>
        <translation>Beranda: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="523"/>
        <source>Development page: %1</source>
        <translation>Halaman pengembang: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="524"/>
        <source>Bug tracker: %1</source>
        <translation>Pelacak bug: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="171"/>
        <location filename="../qmmpstarter.cpp" line="529"/>
        <source>Command Line Help</source>
        <translation>Bantuan Baris Perintah</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="542"/>
        <source>QMMP version: %1</source>
        <translation>Versi QMMP: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="543"/>
        <source>Compiled with Qt version: %1</source>
        <translation>Dikompilasi dengan versi Qt: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="544"/>
        <source>Using Qt version: %1</source>
        <translation>Menggunakan versi Qt: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="547"/>
        <source>Qmmp Version</source>
        <translation>Versi Qmmp</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="564"/>
        <source>User Interfaces</source>
        <translation>Antarmuka Pengguna</translation>
    </message>
</context>
</TS>
